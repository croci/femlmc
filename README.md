FEMLMC - a Finite Element Multilevel (quasi) Monte Carlo methods library for FEniCS
===================================================================================

Synopsis
--------

The FEMLMC library contains the Monte Carlo methods and Matern-Gaussian field sampling algorithms presented in two papers by Croci, Giles, Rognes and Farrell. The library offers the following algorithms and routines:

* A parallel implementation of the standard Monte Carlo (MC), quasi Monte Carlo (QMC), multilevel Monte Carlo (MLMC) and multilevel quasi Monte Carlo methods (MLQMC) for the solution of PDEs with random coefficients and spatial white noise driven SPDEs. All these routine also work on non-nested mesh hierarchies.
* Two optimal (linear) cost complexity Matern-Gaussian field sampling methods based on the [SPDE approach](https://rss.onlinelibrary.wiley.com/doi/full/10.1111/j.1467-9868.2011.00777.x) by Lindgren et al. The former algorithm uses pseudo-random numbers, while the second requires a low-discrepancy sequence generator and is designed to work with (ML)QMC.
* MLMC and MLQMC testing routines inspired by Giles' work (see <http://people.maths.ox.ac.uk/~gilesm/>). 

For a full description of the algorithm, see <https://doi.org/10.1137/18M1175239> (also on arxiv) for (ML)MC, and <https://arxiv.org/abs/1911.12099> for (ML)QMC.

Dependencies
------------

FEMLMC depends on:

* FEniCS (tested on 2018.2.0 and 2019.1.0), compiled with PETSc, petsc4py and HDF5 support (<http://fenicsproject.org>).
* mpi4py (<http://pythonhosted.org/mpi4py/>).
* libsupermesh, configured with -DBUILD\_SHARED\_LIBS=ON (<https://bitbucket.org/libsupermesh/libsupermesh/>).
* Python packages: numpy, scipy, itertools, time, and (recommended) copy.

The easiest way to install FEMLMC is inside the docker images supplied by the FEniCS project (http://fenicsproject.org/download). All dependencies except for libsupermesh are installed there and installing libsupermesh on top of that is a quick process.

We also recommend installing:

* mkl\_sobol, or an alternative randomised low-discrepancy sequence generator. Essential for the (ML)QMC routines and strongly recommended (<https://bitbucket.org/croci/mkl_sobol/>).
* mshr (<https://bitbucket.org/fenics-project/mshr>).

FEMLMC has been tested with mkl\_sobol. Other randomised low-discrepancy sequence generators might not be supported as is. In this case, please drop us an email or open an issue and we will see what we can do to help.

How to use it
-------------

First, you must tell FEMLMC where libsupermesh is installed:

```
export LIBSUPERMESH_DIR=/path/to/libsupermesh/install/prefix/
```

Then, we recommend to look at the examples in femlmc/test/. To run them in parallel, do, e.g.

```
mpiexec -n 4 python3 test.py 40

```

This runs a MLMC test routines with 40 initial samples split across 4 processors.

FEMLMC works both in parallel and in serial. In parallel, FEMLMC splits the total numer of Monte Carlo samples required across the workers. However, due to restrictions in libsupermesh, FEMLMC does not work with the domain decomposition capabilities of FEniCS. Overall, 4 rules must be observed when using FEMLMC:

1. The ```MPI.comm_self``` communicator must be used when using FEMLMC in parallel in all suitable calls to FEniCS routines, especially when loading meshes.

2. A random number generator must be set with a call to setRNG. For Monte Carlo methods it is crucial that each worker generates independent pseudo-random numbers. The easiest way is to run something like
    
    ```    
    setRNG(numpy.random.RandomState(dolfin.MPI.rank(dolfin.MPI.comm_world)))
    ```
    
3. For (ML)QMC applications, a randomised low-discrepancy sequence generator Python class and the number of randomisations must also be set with a call to setQRNG. If mkl\_sobol is being used it is sufficient to run
    
    ```
    from mkl_sobol import MKL_SOBOL_RNG
    ```
    
    ```
    setQRNG(MKL_SOBOL_RNG, M = n_qmc_randomisations)
    ```
    
    Otherwise, the randomised low-discrepancy sequence generator Python class must have the following API:

    * Its constructor must be in the form ```QRNG(seed= , dim= )```, where ```seed``` is the seed of the randomisation and ```dim``` the dimensionality of the sequence to generate.
    * It must generate the next low discrepancy Gaussian distributed point via a call to ```QRNG.randn()```. The returned point must be a flattened numpy array of length ```dim```.
    * (Optional) It must have a skipahead functionality callable with ```QRNG.skipahead(skip)```. This is only needed if the number of workers is strictly larger than the number of randomisations of the low-discrepancy sequence.

4. For sampling Matern-Gaussian fields, the [SPDE approach](https://rss.onlinelibrary.wiley.com/doi/full/10.1111/j.1467-9868.2011.00777.x) requires embedding the computational domain on which the sample is needed into a [larger domain](https://arxiv.org/pdf/1809.07570.pdf). For this reason, a Matern field must be initialised as follows:
    
    ```
    matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, nested_inner_outer = False, nested_hierarchy = False)
    ```
    
    where ```inner_spaces``` is (a list of) the function space(s) on which the sample is needed, ```outer_spaces``` is (a list of) the space(s) on which the field must be sampled and must be constructed over a larger mesh embedding the domain of the inner spaces. If the inner spaces are nested within the outer spaces, then the ```nested_inner_outer``` flag must be set to ```True```. ```parameters``` is a dictionary containing the Matern covariance parameters. ```nested_hierarchy``` must be specified to make computations faster when the multilevel hierarchy is nested. Note that the SPDE approach is only good for sampling fields with small smoothness parameter.

Installation
------------

To build FEMLMC, run ```python3 setup.py install```

or, alternatively, ```python3 setup.py develop```

Testing
-------

To test that FEMLMC has been installed correctly run the tests in ```femlmc/tests/``` (in serial) and the examples in ```femlmc/examples/``` (both serial and parallel).

Note that the QMC-based examples require mkl\_sobol to run and they must be modified if another randomised low-discrepancy sequence generator
is used. In addition, the QMC-based examples might take a long time to run in serial and running them in parallel is to be preferred.
    
Experimental Features
---------------------

The following features are still experimental and might not work:

* hp-refinement.
* non-geometric ML(Q)MC hierarchies.
* p-refinement in the MLQMC case (it works for standard MC and MLMC).
* estimation of the expectation of multiple outputs of interest via MLQMC (it works for standard MC, MLMC and QMC).
* estimation of infinite-dimensional output functionals
* Richardson-Romberg extrapolation for MLMC (not supported for QMC and MLQMC).
* Matern fields with smoothness parameter that is not in the form 2 x K - d/2, where K is an arbitrary positive integer and d is the spatial dimension. This requires solving a fractional diffusion equation.

Contributors
------------

Matteo Croci <matteo.croci@maths.ox.ac.uk>

License
-------

This code is released under the GNU [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). Anyone requiring a more permissive license should contact M. Croci.
