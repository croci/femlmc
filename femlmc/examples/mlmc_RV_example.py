#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

#################################################################################
# EXAMPLE: 
# solution of a diffusion equation with random variable coefficient via MLMC,
# with an example on how to handle two output functionals.
#
# Run with: mpiexec -n np python3 mlmc_example.py N
# where np is the number of processors to use and N the number of samples
# to be taken for convergence tests.
#
#################################################################################

from dolfin import *
from femlmc import *

set_log_level(30)

mpiRank = MPI.rank(MPI.comm_world)

class TestFunctional(OutputFunctional):
    def evaluate(self, V, sample = None, rv_sample = None, level_info = (None,None)):
        # V is the function space on which we want to solve the PDE. sample is a list of
        # the input random field samples needed. rv_sample is a list of the input random
        # variable samples needed. level_info is a tuple (current_level, finecoarse),
        # where finecoarse is 0 if the level is the finer level on MLMC level current_level
        # and 1 if it is the coarser.

        u = TrialFunction(V)
        v = TestFunction(V)

        sol = Function(V)

        lhs = inner(Constant(rv_sample)*grad(u), grad(v))*dx
        rhs = Constant(1.0)*v*dx

        bcs = DirichletBC(u.function_space(), Constant(0.0), 'on_boundary')

        solve(lhs == rhs, sol, bcs)

        E1 = assemble(sol*sol*dx)

        # NOTE: the output of an OutputFunctional must always be a list!!!
        return [E1]

    def get_n_outputs(self):
        return 1

if __name__ == '__main__':

    from numpy.random import RandomState
    import sys

    k   = 1 # the order of the elliptic operator in the SPDE approach will be 2*k
    dim = 2 # spatial dimension
    n_levels  = 10 - [3, 4, 6][dim-1] # number of levels, depending on the dimension

    try: N = int(sys.argv[1])
    except IndexError: 
        raise ValueError("Must specify the number of samples for MLMC convergence tests!")

    # IMPORTANT! Must always set a random number generator or FEMLMC will not run
    RNG = RandomState(mpiRank)
    setRNG(RNG)

    # IMPORTANT!! Each mesh must be loaded with MPI.comm_self
    if dim == 3:
        meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(1, n_levels + 1)]
    elif dim == 2:
        meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(1, n_levels+1)]
    else:
        meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.5, 0.5) for l in range(1, n_levels + 1)]

    # h-refinement
    spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in meshes]

    ## p-refinement example
    #spaces = [FunctionSpace(meshes[2], 'CG', l+1) for l in range(n_levels)]
    
    random_variable = UniformRandomVariable(a=1,b=2)
    mlmc_sampler = MLMCSampler(spaces, random_variable = random_variable, output_functional = TestFunctional())

    # Root mean square error tolerances for MLMC convergence test
    Eps = [5.0e-4/2**i for i in range(7)]

    # the following calls mlmc_test.py
    logfile = open("mlmc_test_RV_diffusion.txt", 'w')
    mlmc_sampler.run_mlmc_test(N, N0=10, Eps=Eps, Lmin = 0, theta = 0.25, logfile=logfile, mc_only = False)
