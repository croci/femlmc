#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

#################################################################################
# EXAMPLE: 
# solution of a diffusion equation with lognormal diffusion coefficient via MLMC,
# with an example on how to handle two output functionals.
#
# Run with: mpiexec -n np python3 mlmc_example.py N
# where np is the number of processors to use and N the number of samples
# to be taken for convergence tests.
#
#################################################################################

from dolfin import *
from femlmc import *

set_log_level(30)

mpiRank = MPI.rank(MPI.comm_world)

class TestFunctional(OutputFunctional):
    def evaluate(self, V, sample = None, rv_sample = None, level_info = (None,None)):
        # V is the function space on which we want to solve the PDE. sample is a list of
        # the input random field samples needed. rv_sample is a list of the input random
        # variable samples needed. level_info is a tuple (current_level, finecoarse),
        # where finecoarse is 0 if the level is the finer level on MLMC level current_level
        # and 1 if it is the coarser.

        u = TrialFunction(V)
        v = TestFunction(V)

        sol = Function(V)

        lhs = inner(exp(sample)*grad(u), grad(v))*dx
        rhs = Constant(1.0)*v*dx

        bcs = DirichletBC(u.function_space(), Constant(0.0), 'on_boundary')

        solve(lhs == rhs, sol, bcs)

        E1 = assemble(sol*sol*dx)
        E2 = assemble(inner(grad(sol),grad(sol))*dx)

        # NOTE: the output of an OutputFunctional must always be a list!!!
        return [E1, E2]

    def get_n_outputs(self):
        return 2

if __name__ == '__main__':

    from numpy.random import RandomState
    import sys

    k   = 1 # the order of the elliptic operator in the SPDE approach will be 2*k
    dim = 2 # spatial dimension
    n_levels  = 10 - [3, 4, 6][dim-1] # number of levels, depending on the dimension

    try: N = int(sys.argv[1])
    except IndexError: 
        raise ValueError("Must specify the number of samples for MLMC convergence tests!")

    # IMPORTANT! Must always set a random number generator or FEMLMC will not run
    RNG = RandomState(mpiRank)
    setRNG(RNG)

    # Matern field parameters: setting lognormal scaling to True since we are using a lognormal
    # coefficient. The exponential of the field will then have the prescribed mean and std. dev.
    matern_parameters = {"lmbda"    : 0.25, # correlation length
                         "avg"      : 1.0, # mean
                         "sigma"    : 0.2, # standard dev.
                         "nu"       : 2*k-dim/2, # smoothness parameter
                         "lognormal_scaling" : True}

    # the Matern field is sampled on the outer meshes, but needed on the inner meshes on which the PDE of interest is solved
    # IMPORTANT!! Each mesh must be loaded with MPI.comm_self
    if dim == 3:
        outer_meshes = [BoxMesh(MPI.comm_self, Point(-1,-1,-1), Point(1,1,1), 2**(l+1), 2**(l+1), 2**(l+1)) for l in range(1, n_levels + 1)]
        inner_meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(1, n_levels + 1)]
    elif dim == 2:
        outer_meshes = [RectangleMesh(MPI.comm_self, Point(-1,-1), Point(1,1), 2**(l+1), 2**(l+1)) for l in range(1, n_levels+1)]
        inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(1, n_levels+1)]
    else:
        outer_meshes = [IntervalMesh(MPI.comm_self, 2**(l+1), -1.0, 1.0) for l in range(1, n_levels + 1)]
        inner_meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.5, 0.5) for l in range(1, n_levels + 1)]

    # h-refinement
    outer_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in outer_meshes]
    inner_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in inner_meshes]

    ## p-refinement example
    #outer_spaces = [FunctionSpace(outer_meshes[2], 'CG', l+1) for l in range(n_levels)]
    #inner_spaces = [FunctionSpace(inner_meshes[2], 'CG', l+1) for l in range(n_levels)]
    
    # NOTE: the hierarchy is nested here, but setting nested_hierarchy to False forces FEMLMC to use a supermesh construction nevertheless (good for testing)
    matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, nested_inner_outer = True, nested_hierarchy = False)
    mlmc_sampler = MLMCSampler(inner_spaces, stochastic_field = matern_field, output_functional = TestFunctional(), richardson_alpha = None)

    # Root mean square error tolerances for MLMC convergence test
    Eps = [5.0e-4/2**i for i in range(7)]

    # the following calls mlmc_test.py
    logfile = open("mlmc_test_lognormal_diffusion.txt", 'w')
    mlmc_sampler.run_mlmc_test(N, N0=10, Eps=Eps, Lmin = 0, theta = 0.25, logfile=logfile, mc_only = False)
