#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

#################################################################################
# EXAMPLE: 
# solution of a diffusion equation with lognormal diffusion coefficient via MLQMC,
# with an example on how to handle multiple inputs.
#
# Run with: mpiexec -n np python3 mlmc_example.py N
# where np is the number of processors to use and N the number of samples
# to be taken for convergence tests.
#
# NOTE: this example requires mkl_sobol (see README.md file)
# NOTE: better to run this on a machine with many processors
#
#################################################################################

from dolfin import *
from femlmc import *
import sys

mpiRank = MPI.rank(MPI.comm_world)
mpiSize = MPI.size(MPI.comm_world)
set_log_level(30)

class TestFunctional(OutputFunctional):
    def evaluate(self, V, sample = None, rv_sample = None, level_info = (None,None)):
        # V is the function space on which we want to solve the PDE. sample is a list of
        # the input random field samples needed. rv_sample is a list of the input random
        # variable samples needed. level_info is a tuple (current_level, finecoarse),
        # where finecoarse is 0 if the level is the finer level on MLMC level current_level
        # and 1 if it is the coarser.

        u = TrialFunction(V)
        v = TestFunction(V)

        sol = Function(V)

        lhs = inner(exp(sample[0]+sample[1])*grad(u), grad(v))*dx
        rhs = Constant(rv_sample[0]/2+rv_sample[1]/2)*v*dx

        bcs = DirichletBC(u.function_space(), Constant(0.0), 'on_boundary')

        solve(lhs == rhs, sol, bcs)

        E = assemble(sol*sol*dx)

        # NOTE: the output of an OutputFunctional must always be a list!!!
        return [E]

    def get_n_outputs(self):
        return 1

if __name__ == '__main__':
    from mkl_sobol import MKL_RNG, MKL_SOBOL_RNG

    try: N = int(sys.argv[1])
    except IndexError:
        raise ValueError("Must specify the number of samples for MLQMC convergence tests!")

    k   = 1 # the order of the elliptic operator in the SPDE approach will be 2*k
    dim = 1 # spatial dimension
    n_levels  = 10 - [1, 5, 5][dim-1]

    # NOTE: in the MLQMC case we can run two tests. One, "mlqmc_test" is the same as in the MLMC case,
    # the other, "qmc_convergence_test", checks the order of convergence with respect to the number of samples.
    # Both tests are needed to produce plots looking like those from the original Giles
    # and Waterhouse MLQMC paper.
    options = ["mlqmc_test", "qmc_convergence_test"]
    selected_option = options[0]

    # need to increase the number of randomisations when doing convergence tests w.r.t. the number of samples
    if selected_option == "qmc_convergence_test":
        n_randomisations = 128
    else:
        n_randomisations = 32

    # IMPORTANT! Must always set a random number generator and a randomised low-discrepancy sequence generator class for MLQMC or FEMLMC will not run
    RNG = MKL_RNG(seed=mpiRank)
    setRNG(RNG)
    setQRNG(MKL_SOBOL_RNG, M = n_randomisations)

    # Matern field parameters: setting lognormal scaling to True since we are using a lognormal
    # coefficient. The exponential of the field will then have the prescribed mean and std. dev.
    matern_parameters = {"lmbda"    : 0.25, # correlation length
                         "avg"      : 1.0, # mean
                         "sigma"    : 0.2, # std. deviation
                         "nu"       : 2*k - dim/2, # smoothness parameter
                         "lognormal_scaling" : True}


    # the Matern field is sampled on the outer meshes, but needed on the inner meshes on which the PDE of interest is solved
    # IMPORTANT!! Each mesh must be loaded with MPI.comm_self
    if dim == 3:
        outer_meshes = [BoxMesh(MPI.comm_self, Point(-1,-1,-1), Point(1,1,1), 2**(l+1), 2**(l+1), 2**(l+1)) for l in range(1, n_levels + 1)]
        inner_meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(1, n_levels + 1)]
    elif dim == 2:
        outer_meshes = [RectangleMesh(MPI.comm_self, Point(-1,-1), Point(1,1), 2**(l+1), 2**(l+1)) for l in range(1, n_levels + 1)]
        inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(1, n_levels + 1)]
    else:
        outer_meshes = [IntervalMesh(MPI.comm_self, 2**(l+1), -1.0, 1.0) for l in range(1, n_levels + 1)]
        inner_meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.5, 0.5) for l in range(1, n_levels + 1)]

    # h-ref
    outer_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in outer_meshes]
    inner_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in inner_meshes]

    # NOTE: the hierarchy is nested here, but setting nested_hierarchy to False forces FEMLMC to use a supermesh construction nevertheless (good for testing)
    # NOTE: setting qmc=True is essential to use QMC/MLQMC. IT MUST BE SET ON ALL INPUTS.
    matern_field1 = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = True, Lh = [5]*n_levels, nested_inner_outer = True, nested_hierarchy = False)
    matern_field2 = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = True, Lh = [5]*n_levels, nested_inner_outer = True, nested_hierarchy = False)

    input_rf = MixedStochasticField([matern_field1, matern_field2])
    input_rv = MixedRandomVariable([NormalRandomVariable(qmc=True), NormalRandomVariable(qmc=True)])
    mlmc_sampler = MLMCSampler(inner_spaces, stochastic_field = input_rf, random_variable = input_rv, output_functional = TestFunctional())

    logfile = open("mlqmc_test_lognormal_diffusion.txt", 'w')

    if selected_option == "mlqmc_test":
        # Root mean square error tolerances for MLMC convergence test
        Eps = [5.0e-4/2**i for i in range(7)]
        # the following calls mlqmc_test.py
        mlmc_sampler.run_mlqmc_test(Eps = Eps, logfile = logfile, gamma = dim, N = N, theta = 0.25, qmc_only = False)

    elif selected_option == "qmc_convergence_test":
        for i in range(n_levels):
            mlmc_sampler.run_mlqmc_convergence_test(N, l = i, p0=0, logfile = logfile)
