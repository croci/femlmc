#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from .stochastic_field import StochasticField
from .random_variable import MixedRandomVariable
from .mlmc_test import mlmc_test
from .mlqmc_test import mlqmc_test
from .mlmc import mlmc
from .mc import mc
from .qmc import qmc, qmc_convergence_test
from .quasi_random_number_generator import GlobalQRNG
from mpi4py.MPI import SUM as MPISUM
from time import time
from sys import stdout, modules
from numpy import isfinite, prod, log2, ceil

class MLMCSampler(StochasticField):
    def setup(self):

        self.qmc_flag = False
        self.random_variable = self.params.get("random_variable", None)
        if self.stochastic_field is not None and self.random_variable is not None:
            self.qmc_flag = any([self.stochastic_field.qmc_flag, self.random_variable.qmc_flag])
        elif self.stochastic_field is not None:
            self.qmc_flag = self.stochastic_field.qmc_flag
        elif self.random_variable is not None:
            self.qmc_flag = self.random_variable.qmc_flag

        if self.qmc_flag:
            self.qmc_setup()

        self.output_functional = self.params.get("output_functional", None)
        if self.output_functional is None:
            raise ValueError("An OutputFunctional must be given to an MLMCSampler")

        self.norm = self.output_functional.norm

        # Richardson extrapolation requires prescribing the MLMC parameter alpha beforehand
        self.richardson_alpha = self.params.get("richardson_alpha", None)
        if self.richardson_alpha is not None:
            assert self.richardson_alpha > 0
            if self.mpiRank == 0 and self.qmc_flag == False:
                print("Richardson Extrapolation ON. alpha = ", self.richardson_alpha)

        self.n_outputs = self.output_functional.get_n_outputs()

        if self.mpiRank == 0:
            if self.refinement_type is None:
                print("Standard Monte Carlo Problem\n")
            elif self.refinement_type == 'p':
                print("MLMC Problem - p-refinement\n")
            elif self.refinement_type == 'h':
                print("MLMC Problem - h-refinement\n")
            elif self.refinement_type == 'hp':
                print("MLMC Problem - hp-refinement\n")
                print("WARNING: hp-refinement is still an experimental functionality\n")

        self.worldComm.barrier() # synchronize workers

    def qmc_setup(self):
        # preparing for QMC: making sure each processor has the shift vectors it needs.
        # also see quasi_random_number_generator.py
        if self._independent == False:
            GQRNG = GlobalQRNG(worldcomm = self.worldComm)
            GQRNG._setUp()
            setattr(modules["femlmc"], '__GLOBAL_QRNG__', GQRNG)
        else:
            GQRNG = getattr(modules["femlmc"], '__GLOBAL_QRNG__')

        self.GQRNG = GQRNG # globar quasi-random number generator
        self.M = self.GQRNG.M # number of randomisations
        self.splitinteams = self.mpiSize > self.M # same shift for multiple processors. Requires an additional MPI communicator.
        self.teamNo = self.GQRNG.teamNo
        self.whoisinmyteam = self.GQRNG.whoisinmyteam
        self.teamSize = self.GQRNG.teamSize
        self.teamRank = self.GQRNG.teamRank
        self.teamComm = self.GQRNG.teamComm
        self.howmanyshifts = self.GQRNG.howmanyshifts
        self.multiple_shifts = self.mpiSize < self.M # multiple shifts per processors

        if self.splitinteams == True:
            bridgeGroup = self.worldComm.group.Incl(list(range(self.M)))
            self.bridgeComm = self.worldComm.Create_group(bridgeGroup)
        else:
            self.bridgeComm = self.worldComm

        if self.stochastic_field is not None:
            self.stochastic_field._initialise_GQRNG()
        if self.random_variable is not None:
            self.random_variable._initialise_GQRNG()

    def change_qrng_shift(self, shift_number):
        if self.multiple_shifts == True:
            self.GQRNG.QRNG = [self.GQRNG.QRNGs[l][shift_number] for l in range(self.GQRNG.n_levels)]

    def sample(self, N, l = 0):
        if not self.qmc_flag:
            return self.mlmc_sample(N, l=l)
        else:
            return self.mlqmc_sample(N, l=l)

    def mlqmc_sample(self, N, l = 0):

        if isinstance(N,int) == False:
            N = int(round(N))

        nprocs  = min(self.teamSize,max(N,1))
        NN      = [int(N/nprocs)]*nprocs 
        NN[0]  += N%nprocs
        NN     += [0 for i in range(self.teamSize - nprocs)]

        xrnge = 2 - (l is 0)

        n_outputs = self.n_outputs

        sums_list = [[[0. for j in range(2)] for k in range(n_outputs)] for i in range(self.howmanyshifts)]

        tic = time()

        if self.mpiRank == 0:
            stdout.write('\r')
            stdout.write("Level %d [%-50s] %d%%" % (l, '', 0))
            stdout.flush()

        for shift_number in range(self.howmanyshifts):

            self.change_qrng_shift(shift_number)

            for iteration in range(NN[self.teamRank]):

                Noneflag = True
                while Noneflag is True:
                    if self.stochastic_field is not None:
                        uu = self.stochastic_field.sample(l)
                    else:
                        uu = [None for i in range(xrnge)]

                    if self.random_variable is not None:
                        rr = self.random_variable.sample(l)
                        if isinstance(self.random_variable, MixedRandomVariable):
                            rtemp = [[] for i in range(xrnge)]
                            for j in range(len(rr)):
                                if not isinstance(rr[j],list):
                                    rr[j] = [rr[j] for i in range(xrnge)]
                                [rtemp[i].append(rr[j][i]) for i in range(xrnge)]
                            rr = rtemp
                        else:
                            if not isinstance(rr,list):
                                rr = [rr for i in range(xrnge)]
                    else:
                        rr = [None for i in range(xrnge)]

                    s = [self.output_functional.evaluate(V, sample = sample, rv_sample = rv_sample, level_info=(l,i)) for i,V,sample,rv_sample in zip(range(xrnge), self.function_levels[l],uu,rr)]

                    Noneflag = not(bool(prod([prod(isfinite(a)) for b in s for a in b])))
                    if Noneflag is True:
                        print("Process %d, WARNING: the output functional returned inf or NaN value" % self.mpiRank)

                if l == 0:
                    s.append([0.]*n_outputs)

                for k in range(n_outputs):
                    sums_list[shift_number][k][0] += (s[0][k] - s[1][k]) 
                    sums_list[shift_number][k][1] +=  s[0][k]

                if self.mpiRank == 0:
                    stdout.write('\r')
                    stdout.write("Level %d [%-50s] %d%%" % (l, '='*int(round(50*(NN[self.teamRank]*shift_number + iteration+1)/(NN[self.teamRank]*self.howmanyshifts))), int(round(100*(NN[self.teamRank]*shift_number + iteration+1)/(NN[self.teamRank]*self.howmanyshifts)))))
                    stdout.flush()

        if self.mpiRank == 0:
            stdout.write('\r%s' % (' '*100))
            stdout.flush()

        stdout.write('\r')

        toc = time()
        costl = toc - tic
        costl = self.worldComm.allreduce(costl, op = MPISUM)

        if self.splitinteams == True:
            for k in range(n_outputs):
                for j in range(2):
                    # only need to do this for sums_list[0] as if we are splitinteams then each team only has a single shift
                    sums_list[0][k][j] = self.teamComm.allreduce(sums_list[0][k][j], op = MPISUM)

        return sums_list, costl

    def mlqmc_finalise_sums(self, sums_list, Nk):
        # given a sums_list returned by mlqmc_sample(...) this routine computes the (ML)QMC estimator variance and expected value
        # by averaging over the M randomisations. To do so, we use MPI to bring the results from all randomisation to the rank 0 worker
        self.worldComm.barrier() # synchronize workers
        sums_out = [[0. for i in range(6)] for k in range(self.n_outputs)]
        for k in range(self.n_outputs):
            val0 = [sums_list[shift_number][k][0]/Nk for shift_number in range(self.howmanyshifts)]
            val1 = [sums_list[shift_number][k][1]/Nk for shift_number in range(self.howmanyshifts)]
            vals0 = [sum([val**i for val in val0]) for i in range(1,5)] 
            vals1 = [sum([val**i for val in val1]) for i in range(1,3)]
            vals = vals0 + vals1
            for j in range(len(vals)):
                val = vals[j]

                if self.mpiRank < self.M:
                    assert self.teamRank == 0 # otherwise bcast won't work
                    val = self.bridgeComm.allreduce(val, op = MPISUM)
                    sums_out[k][j] = val

                if self.splitinteams == True:
                    sums_out[k][j] = self.teamComm.bcast(sums_out[k][j])

        return sums_out

    def mlmc_sample(self, N, l = 0):

        if isinstance(N,int) == False:
            N = int(round(N))

        nprocs  = min(self.mpiSize,max(N,1))
        NN      = [int(N/nprocs)]*nprocs 
        NN[0]  += N%nprocs
        NN     += [0 for i in range(self.mpiSize - nprocs)]

        xrnge = 2 - (l is 0)

        n_outputs = self.n_outputs

        sums = [[0. for i in range(6)] for j in range(n_outputs)]

        tic = time()

        if self.mpiRank == 0:
            stdout.write('\r')
            stdout.write("Level %d [%-50s] %d%%" % (l, '', 0))
            stdout.flush()

        for iteration in range(NN[self.mpiRank]):

            Noneflag = True
            while Noneflag is True:
                if self.stochastic_field is not None:
                    uu = self.stochastic_field.sample(l)
                else:
                    uu = [None for i in range(xrnge)]

                if self.random_variable is not None:
                    rr = self.random_variable.sample(l)
                    if isinstance(self.random_variable, MixedRandomVariable):
                        rtemp = [[] for i in range(xrnge)]
                        for j in range(len(rr)):
                            if not isinstance(rr[j],list):
                                rr[j] = [rr[j] for i in range(xrnge)]
                            [rtemp[i].append(rr[j][i]) for i in range(xrnge)]
                        rr = rtemp
                    else:
                        if not isinstance(rr,list):
                            rr = [rr for i in range(xrnge)]
                else:
                    rr = [None for i in range(xrnge)]

                s = [self.output_functional.evaluate(V, sample = sample, rv_sample = rv_sample, level_info = (l,i)) for i,V,sample,rv_sample in zip(range(xrnge),self.function_levels[l],uu,rr)]

                Noneflag = not(bool(prod([prod(isfinite(a)) for b in s for a in b])))
                if Noneflag is True:
                    print("Process %d, WARNING: the output functional returned inf or NaN value" % self.mpiRank)

            if l == 0:
                s.append([0.]*n_outputs)

            # applying the norm if we are considering infinite-dimensional functionals
            for k in range(n_outputs):
                sums[k][0] +=           (s[0][k] - s[1][k]) 
                sums[k][1] += self.norm((s[0][k] - s[1][k])**2., k)
                sums[k][2] +=           (s[0][k] - s[1][k])**3. # NOTE: no need to take the norm here
                sums[k][3] +=           (s[0][k] - s[1][k])**4. # NOTE: no need to take the norm here
                if self.richardson_alpha is None:
                    sums[k][4] +=        s[0][k]
                    sums[k][5] +=        s[0][k]           **2.
                else:
                    a = self.richardson_alpha
                    sums[k][4] +=  (2**a*s[0][k] - s[1][k])/(2**a - 1)
                    sums[k][5] += ((2**a*s[0][k] - s[1][k])/(2**a - 1))**2

            if self.mpiRank == 0:
                stdout.write('\r')
                stdout.write("Level %d [%-50s] %d%%" % (l, '='*int(round(50*(iteration+1)/NN[self.mpiRank])), int(round(100*(iteration+1)/NN[self.mpiRank]))))
                stdout.flush()

        if self.mpiRank == 0:
            stdout.write('\r%s' % (' '*100))
            stdout.flush()

        stdout.write('\r')

        toc = time()
        costl = toc - tic
        costl = self.worldComm.allreduce(costl, op = MPISUM)

        for k in range(n_outputs):
            for j in range(6):
                sums[k][j] = self.worldComm.allreduce(sums[k][j], op = MPISUM)

        return sums, costl


    def run_qmc_convergence_test(self, N, l = 0, p0 = 0, logfile = None):
        # check convergence w.r.t. number of samples for QMC

        assert l < self.n_levels

        sampler = self.get_independent_field(l)

        sampler.run_mlqmc_convergence_test(N, l=0, p0=p0, isqmc = True, logfile=logfile)

    def run_mlqmc_convergence_test(self, N, l = 0, p0 = 0, isqmc = False, logfile=None):
        # check convergence w.r.t. number of samples for MLQMC

        assert l < self.n_levels

        p0 = 0
        qmc_convergence_test(self, N, l, p0, ismlqmc = (not isqmc), logfile = logfile)

    def run_mlmc_test(self, N, N0 = 10, Eps = None, Lmin = 0, theta = 0.5, logfile = None, mc_only = True):

        if self.n_levels > 1 and Eps is None and mc_only == False:
            print("WARNING! Epsilon not specified: switching to mc_only mode")
            mc_only = True

        if Lmin > self.n_levels:
            raise ValueError("Lmin must be smaller or equal to the total number of levels")

        if self.n_levels == 1:
            mc_only == True

        mlmc_test(self, N, N0, Eps, Lmin = Lmin, logfile = logfile, theta = theta, mc_only = mc_only)

    def run_mlqmc_test(self, Eps=None, Lmin = 0, logfile=None, alpha = None, gamma = None, N = None, theta = 0.5, qmc_only = False):

        if self.n_levels > 1 and Eps is None and qmc_only == False:
            print("WARNING! Epsilon not specified: switching to qmc_only mode")
            qmc_only = True

        if self.n_levels > 1 and (Eps is None or qmc_only == True) and (alpha is None or gamma is None):
            raise ValueError("Provide either Eps or alpha and gamma values!")

        if Lmin > self.n_levels:
            raise ValueError("Lmin must be smaller or equal to the total number of levels")
        if self.n_levels == 1:
            qmc_only == True

        mlqmc_test(self, Eps=Eps, Lmin=Lmin, logfile = logfile, alpha = alpha, gamma = gamma, N = N, theta=theta, qmc_only = qmc_only)

    def run(self, N0, eps = None, alpha = 0, beta = 0, gamma = 0, theta = 0.5, logfile=None):
        # runs either mlmc or mlqmc or mc or qmc according to how the mlmc_sampler is set up.
        if self.n_levels > 1 and eps is None:
            raise ValueError("Please, prescribe a target tolerance for the MLMC run")
        elif self.n_levels > 1:
            if not self.qmc_flag:
                return mlmc(self, N0, eps, alpha = alpha, beta = beta, gamma = gamma, theta = theta, logfile=logfile)
            else:
                return mlqmc(self, eps, alpha = alpha, gamma = gamma, theta=theta, logfile=logfile)
        else:
            if not self.qmc_flag:
                return mc(self.sample, N0, logfile=logfile)
            else:
                return qmc(self, N0, eps = eps, logfile=logfile)
