#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from numpy import array, polyfit, ndarray, arange, log2, ceil, sqrt
import numpy.linalg as linalg
from scipy.special import zeta

def mlmc(mlmc_sampler, N0, eps, alpha = 0, beta = 0 , gamma = 0, theta = 0.5, logfile=None):
    """
    Multi-level Monte Carlo estimation.

    (P, Nl, Cl) = mlmc(...)

    Outputs:
      P:  value
      Nl: number of samples on each level  (numpy array)
      Cl: computational cost on each level (numpy array)
    Inputs:
      N0:   initial number of samples    >  0
      eps:  desired accuracy (rms error) >  0

      alpha: weak error is  O(2^{-alpha*l})
      beta:  variance is    O(2^{-beta*l})
      gamma: sample cost is O(2^{gamma*l})  > 0

      theta: weight between statistical error and bias in (0,1)

      If alpha, beta are not positive or not provided,
      then they will be estimated.

      mlmc_sampler: a FEMLMC MLMCSampler

    """
    
    #Lmin += 1
    #Lmax += 1
    #if Lmin < 3:
    #    Lmin = 3

    Lmin = 1
    if isinstance(N0, (ndarray, list)):
        Lmin = len(N0)

    Lmax = mlmc_sampler.n_levels 

    if eps <= 0 or gamma <= 0 or Lmin > Lmax or theta <= 0 or theta >= 1:
        raise ValueError("Requirements: eps > 0, gamma > 0, Lmin <= n_levels, theta in (0,1)")

    refinement_level = 1.0/array(mlmc_sampler.num_dofs)**(1.0/mlmc_sampler.tdim)
    
    # MLMC helper class
    class MLMCLevel(object):
        def __init__(self,l,Nl):
            self.level = l
            self.Ndone = 0
            self.Ntodo = Nl
            self.Nsamples = Nl
            self.sums = [0.]*6
            self.cost = 0.0
            self.time_cost = 0.0

        def run(self):
            self.Ndone += self.Ntodo
            out, time_cost = mlmc_sampler.sample(self.Ntodo, l = self.level)
            cost = refinement_level[self.level]**(-gamma)*self.Ntodo 
            self.Ntodo = 0
            # FIXME NOTE: out[0] is so that only the first output functional is considered
            self.sums = [a + b for a,b in zip(self.sums,out[0])]
            self.cost += cost
            self.time_cost += time_cost

        def compute_sums(self):
            self.run()
            return self.sums

    if isinstance(N0, (ndarray, list)):
        levels = [MLMCLevel(l,n) for l,n in enumerate(N0)]
    else:
        levels = [MLMCLevel(l,N0) for l in range(Lmin)]

    # FIXME: Again, the zero is so that we only take the first output functional
    norm = lambda x : mlmc_sampler.norm(x, 0)

    # richardson extrapolation
    r_alpha = mlmc_sampler.richardson_alpha
    if r_alpha is not None:
        a = 2**r_alpha/(2**r_alpha - 1)
        b =          1/(2**r_alpha - 1)
	
    L = Lmin

    while sum([level.Ntodo for level in levels]) > 0:

        sums = [level.compute_sums() for level in levels]
        Cl   = [level.cost/level.Ndone for level in levels]

        inducedbyinnerproduct = isinstance(sums[0][0], float)

        ml = [norm(Sum[0])/level.Ndone for Sum,level in zip(sums,levels)]
        if inducedbyinnerproduct:
            Vl = [max(0.,sums[i][1]/levels[i].Ndone - ml[i]**2) for i in range(L)]
        else:
            # in this case can only check the second moment of the error
            Vl = [sums[i][1]/levels[i].Ndone for i in range(L)]

        # richardson extrapolation
        if r_alpha is not None:
            ml = [a*fine - b*coarse for fine, coarse in zip(ml,[0] + ml[:-1])]

        # fix to cope with possible zero values for ml and Vl
        # (can happen in some applications when there are few samples)

        for l in range(Lmin - 1, L):
            ml[l] = max(ml[l], 0.5*ml[l-1]/2**alpha)
            Vl[l] = max(Vl[l], 0.5*Vl[l-1]/2**beta)

        # use linear regression to estimate alpha, beta if not given (copied from Patrick's code)
        if alpha <= 0:
            A = ones((L-1, 2)); A[:, 0] = range(1, L)
            x = linalg.solve(A, array([log2(ml[i]) for i in range(1,L)]))
            alpha = max(0.5, -x[0])

        if beta <= 0:
            A = ones((L-1, 2)); A[:, 0] = range(1, L)
            x = linalg.solve(A, array([log2(Vl[i]) for i in range(1,L)]))
            beta = max(0.5, -x[0])

        # set optimal number of additional samples
        sumCV = sum([sqrt(V*C) for V,C in zip(Vl,Cl)])
        # FIXME: the following is correct only for scalar outputs. According to the norm and output this can be different.
        Ns = [int(ceil(sqrt(V/C)*sumCV/((1. - theta)*eps**2.))) for V,C in zip(Vl,Cl)]
        #Ns = [max(1,2**int(ceil(log2(nn)))) for nn in Ns]
        for level,N in zip(levels,Ns):
            level.Ntodo = max(0,N - level.Nsamples)
            level.Nsamples = max(level.Nsamples,N)
        
        if sum([level.Ntodo > 0.01*level.Nsamples for level in levels]) == 0:
            #err = max([ml[L + i - 1]*2.**(alpha*i) for i in range(-2,1)])/(2.**alpha - 1)
            # NOTE: this is all good even when using Richardson extrapolation, provided that the alpha used in the following
            #       is not r_alpha, but the alpha obtained when using Richardson extrapolation (usually it is alpha = r_alpha + 1)
            err = ml[L - 1]/(2.**alpha - 1.)

            if err > eps*sqrt(theta):
                if L == Lmax:
                    if mlmc_sampler.mpiRank == 0:
                        print("WARNING: WEAK CONVERGENCE FAILURE!!! Increase the maximum refinement level.\n")
                    break;

                V = Vl[-1]/2.**beta
                C = refinement_level[L]**(-gamma)

                sumCV += sqrt(V*C)
                Vl += [V]
                Cl += [C]
                Ns = [int(ceil(sqrt(V/C)*sumCV/((1. - theta)*eps**2))) for V,C in zip(Vl,Cl)]
                #Ns = [max(1,2**int(ceil(log2(nn)))) for nn in Ns]
                levels += [MLMCLevel(L,Ns[-1])]
                L += 1

    outP = [level.sums[0]/level.Ndone for level in levels if level.Ndone > 0]
    if r_alpha is not None:
        # richardson extrapolation
        outP = [a*fine - b*coarse for fine, coarse in zip(outP,[0] + outP[:-1])]

    P = sum(outP)
    Nl = [level.Ndone for level in levels]

    return P, array(Nl), array(Cl) 
