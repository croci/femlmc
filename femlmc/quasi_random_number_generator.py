#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import numpy as np
from dolfin import MPI
from sys import modules

class GlobalQRNG(object):
    # This class handles the randomised low-discrepancy sequence generator and the total QMC input dimensionality.
    # The global QRNG (quasi random number generator) makes sure that the right QMC dimensions are assigned to
    # the corresponding random inputs. The dimensions are ordered so that the first QMC dimensions are assigned
    # to the input random variables (if present) and the remaining ones are spread equally among the remaining
    # random fields in such a way to preserve the QMC ordering. See also pg 13-14 in http://people.maths.ox.ac.uk/~gilesm/mc/mc/lec13.pdf
    #
    # The GlobalQRNG class requires having set a user-provided QRNG base class with a call to setQRNG. This class must have the following properties:
    # 1- The QRNG class base constructor must be in the form QRNG(seed= , dim= ), where seed is the seed
    #    of the randomisation and dim the dimensionality of the sequence to generate.
    # 2- It must generate the next low discrepancy Gaussian distributed point via a call to QRNG.randn().
    #    The returned point must be a flattened numpy array of length dim.
    # 3- (Optional) It must have a skipahead functionality callable with QRNG.skipahead(skip). This is only needed
    #    if the number of workers is strictly larger than the number of randomisations of the low-discrepancy sequence.

    def __init__(self, **kwargs):

        self.RNG = getattr(modules['femlmc'], '__RNG__')
        if self.RNG is None:
            raise RuntimeError("Before using femlmc a random number generator must be set with a call to setRNG")

        worldcomm = kwargs.get("worldcomm", MPI.comm_world).Dup()
        self.mpiRank = MPI.rank(worldcomm)
        self.mpiSize = MPI.size(worldcomm)
        self.worldComm = worldcomm

        try: 
            self.QRNG_CLASS = getattr(modules['femlmc'], '__QRNG_CLASS__')
            self.M = getattr(modules['femlmc'], '__N_QMC_SHIFTS__')
        except AttributeError:
            if self.mpiRank == 0:
                print("WARNING! Quasi-random number generator class is not set! Set it with a call to setQRNG!")
            self.QRNG_CLASS = None
            self.M = 1

        self.setup = False


    def sample(self, l, input_number):
        # assigns each QMC dimension to the correct random inputs so as to preserve the QMC ordering.
        # We store each QMC point sampled in memory until all of it has been used or until the same components of it are needed again.
        # We assign the first dimensions to the random variables and the remaining to the random fields.

        assert self.setup == True

        # NOTE: if input_number in check_sampled, then we have already used the QMC point in store_sample and we must sample another one
        #       even if we have a check later, when playing with independent fields the second check below could be useful
        if self.store_sample[l] is None or self.check_sampled[l][input_number] == True:
            self.check_sampled[l] = [False]*self.n_inputs
            if self.QRNG_CLASS is not None:
                self.store_sample[l] = self.QRNG[l].randn()
            else:
                self.store_sample[l] = self.RNG.randn(self.qmc_dims)

        # if it is the n-th random variable, then take the n-th dimension from the top.
        if input_number in self.rv_indices:
            order = self.rv_indices.index(input_number)
            begin, end = self.rv_entries[order]
            out = self.store_sample[l][begin:end]
            if len(out) == 1: out = out[0] # Check in case it is a length-1 vector
        else:
            order = self.rf_indices.index(input_number)
            out = self.store_sample[l][self.field_entries[order][l]]

        self.check_sampled[l][input_number] = True
        # delete stored sampled if we have fully sampled the level
        if all(self.check_sampled[l]):
            self.store_sample[l] = None

        return out

    def _setUp(self):
        # Routine to set up the global generator.
        # We store information about the number of inputs and the QMC dimensions needed for each of them
        # within global variables. When an input is sampled for the first time, the global QRNG is set up.
        # FIXME: if a new field is created after the GlobalQRNG is already set up, a new _setUp must be forced.

        try:
            self.QMC_dimensions = getattr(modules['femlmc'], '__QMC_DIMENSIONS__')
        except AttributeError:
            if self.QRNG_CLASS is None:
                self.setup = True
                return
            else:
                raise RuntimeError("QMC dimensions (of all stochastic inputs) not found!")

        self.n_inputs = len(self.QMC_dimensions)
        if self.n_inputs == 0:
            print("WARNING! No QMC dimensions found!")

        # counting how many dimensions are needed for each input
        # NOTE: each QMC dimension can be either just a number or a list of numbers. The former case is for random variables, the latter case is for random fields and MLQMC.
        self.n_rv = 0
        self.tot_rv_dims = 0
        self.rv_indices = []
        self.rf_indices = []
        self.n_levels = max([1] + [len(item[1]) for item in self.QMC_dimensions if isinstance(item[1], (list, np.ndarray))])
        self.dims_per_field = [-1]*self.n_levels
        tot_dims = [0]*self.n_levels
        for i in range(self.n_inputs):
            assert i == self.QMC_dimensions[i][0]
            if not isinstance(self.QMC_dimensions[i][1], (list, np.ndarray)):
                rv_dim = self.QMC_dimensions[i][1]
                self.QMC_dimensions[i][1] = [rv_dim]*self.n_levels
                self.n_rv += 1
                self.tot_rv_dims += rv_dim
                self.rv_indices.append(self.QMC_dimensions[i][0])
            else:
                temp = self.QMC_dimensions[i][1]
                if self.dims_per_field[0] == -1:
                    self.dims_per_field = temp
                else:
                    if not all([a == b for a,b in zip(temp, self.dims_per_field)]):
                        raise ValueError("All random fields must have the same number of Haar levels")

                if len(temp) < self.n_levels:
                    raise ValueError("All random fields must have the same number of levels")

                self.rf_indices.append(self.QMC_dimensions[i][0])

            tot_dims = [a+b for a,b in zip(tot_dims,self.QMC_dimensions[i][1])]

        self.n_rf = self.n_inputs - self.n_rv
        self.qmc_dims = tot_dims

        cmsum = 0
        self.rv_entries = []
        for idx in self.rv_indices:
            temp_dim = self.QMC_dimensions[idx][1][0]
            self.rv_entries.append([cmsum, cmsum + temp_dim])
            cmsum += temp_dim

        assert cmsum == self.tot_rv_dims

        self.field_entries = [[None]*self.n_levels]*self.n_rf
        for i in range(self.n_rf):
            for l in range(self.n_levels):
                qmc_dim = self.dims_per_field[l]
                tot_dim = tot_dims[l]
                assert int((tot_dim-self.tot_rv_dims)/qmc_dim) == self.n_rf
                self.field_entries[i][l] = i + self.tot_rv_dims + self.n_rf*np.arange(qmc_dim)

        # Now assigning the QMC randomisations correctly to each worker.
        # in the M == mpiSize case, we assign one randomisation to each worker: easy.
        if self.M == self.mpiSize:
            self.QRNG = [self.QRNG_CLASS(seed=self.mpiRank, dim=qmc_dim) for qmc_dim in self.qmc_dims]
            self.teamNo = self.mpiRank
            self.whoisinmyteam = [self.mpiRank]
            self.teamComm = None
            self.teamSize = 1
            self.teamRank = 0
            self.howmanyshifts = 1

        # in the mpiSize > M case, we split the workers in teams, such that each team shares the same randomisation
        # we create new MPI communicators for this that will be handled by the MLMCSampler and we use the skipahead
        # routine from the user-provided QRNG class to make sure that the same sample is not used multiple times by different workers.
        elif self.mpiSize > self.M:
            if self.mpiSize%self.M != 0 and self.mpiRank == 0:
                print("Warning: for optimal parallel job allocation the number of workers must be a multiple of the number of randomisations!")
            self.teamNo = self.mpiRank%self.M
            self.whoisinmyteam = list(range(self.teamNo, self.mpiSize, self.M))
            teamGroup = self.worldComm.group.Incl(self.whoisinmyteam)
            self.teamComm = self.worldComm.Create_group(teamGroup)
            self.teamSize = self.teamComm.Get_size()
            self.teamRank = self.teamComm.Get_rank()
            self.QRNG = [self.QRNG_CLASS(seed=self.teamNo, dim=qmc_dim) for qmc_dim in self.qmc_dims]
            skip_size = 2**int(32-int(max(np.ceil(np.log2(self.teamSize)), 1)))
            skip = skip_size*self.teamRank
            self.howmanyshifts = 1

            for l in range(len(self.qmc_dims)):
                self.QRNG[l].skipahead(skip)

        # in the mpiSize < M case, we assign multiple randomisations to the same workers. The MLMCSampler will be handling these.
        else:
            if self.M%self.mpiSize != 0 and self.mpiRank == 0:
                print("Warning: for optimal parallel job allocation the number of randomisations must be a multiple of the number of workers!")
            self.teamNo = self.mpiRank
            self.whoisinmyteam = [self.mpiRank]
            self.teamComm = None
            self.teamSize = 1
            self.teamRank = 0
            self.howmanyshifts = int(self.M/self.mpiSize) + int(self.mpiRank < self.M%self.mpiSize)
            self.QRNGs = [[self.QRNG_CLASS(seed=self.mpiRank + self.mpiSize*i, dim=qmc_dim) for i in range(self.howmanyshifts)] for qmc_dim in self.qmc_dims]
            self.QRNG = [self.QRNGs[l][0] for l in range(self.n_levels)]

        self.store_sample = [None]*self.n_levels
        self.check_sampled = [[False]*self.n_inputs]*self.n_levels
        self.setup = True

        
