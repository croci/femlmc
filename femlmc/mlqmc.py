#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from numpy import *
import numpy.linalg as linalg
from scipy.special import zeta

def mlqmc(mlmc_sampler, eps, alpha = 0, gamma = -1, theta=0.5, logfile=None):
    """
    Multi-level Quasi Monte Carlo estimation.

    (P, Nl, Cl) = mlqmc(...)

    Outputs:
      P:  value
      Nl: number of samples on each level  (numpy array)
      Cl: computational cost on each level (numpy array)
    Inputs:
      eps:  desired accuracy (rms error) >  0
      alpha: weak error is  O(2^{-alpha*l})
      gamma: cost increases like O(2^{gamma*l})

      mlmc_sampler: MLMCSampler class set up for MLQMC.
        sums, cost = mlmc_sampler.sample(l, N)
      with inputs
        l = level
        N = number of paths
      and a numpy array of outputs
        sums[0] = sum(Y)
        sums[1] = sum(Y**2)
      where Y are iid samples with expected value
        E[P_0]            on level 0
        E[P_l - P_{l-1}]  on level l > 0
    """

    #Lmin += 1
    #Lmax += 1
    #if Lmin < 3:
    #    Lmin = 3

    Lmin = 1

    Lmax = mlmc_sampler.n_levels 

    M = mlmc_sampler.M
    n_outputs = mlmc_sampler.n_outputs
    howmanyshifts = mlmc_sampler.howmanyshifts
    n_values = 2

    if eps <= 0 or gamma < 0 or alpha <= 0 or Lmin > Lmax - 1 or theta <= 0 or theta >= 1:
        raise ValueError("Requirements: eps > 0, alpha > 0, gamma >= 0, Lmin <= n_levels, theta in (0,1)")

    refinement_level = 1.0/array(mlmc_sampler.num_dofs)**(1.0/mlmc_sampler.tdim)
    
    class MLQMCLevel(object):
        def __init__(self,l):
            self.level = l
            self.p = -1
            self.sums = [0.]*6
            self.sums_list   = [[[0.0 for i in range(n_values)] for k in range(n_outputs)] for shift_number in range(howmanyshifts)]
            self.cost = 0.0
            self.time_cost = 0.0

        def double_n_samples(self):
            out, time_cost = mlmc_sampler.sample(2**max(self.p,0), l = self.level)
            cost = refinement_level[self.level]**(-gamma)*2**max(self.p,0) 
            self.p += 1
            for k in range(n_outputs):
                for i in range(n_values):
                    for shift_number in range(howmanyshifts):
                        self.sums_list[shift_number][k][i] += out[shift_number][k][i]

            est_sums = mlmc_sampler.mlqmc_finalise_sums(self.sums_list, 2**self.p)
            # NOTE: only the first output functional is considered
            for i in range(6):
                self.sums[i] = est_sums[0][i]/M

            self.cost += cost
            self.time_cost += time_cost

        def compute_sums(self):
            self.double_n_samples()
            return self.sums

    levels = [MLQMCLevel(l) for l in range(Lmin)]

    L = Lmin

    sums = [level.compute_sums() for level in levels]
    Cl   = [level.cost/(2**level.p) for level in levels]
    pl   = [level.p for level in levels]

    ml = [abs(Sum[0]) for Sum in sums]
    Vl = [M/max(M-1,1)*max(0.,sums[i][1] - sums[i][0]**2)/M for i in range(L)] # divide by M again as we are summing all the estimators together

    # Horrible while True loop
    while True:
        # Make sure the MSE variance is below target tolerance
        while sum(Vl) > (1 - theta)*eps**2:
            lvl = argmax(array(Vl)/(array(Cl)*2**array(pl)))
            level = levels[lvl]
            sums[lvl] = level.compute_sums()
            Cl[lvl]   = level.cost/(2**level.p)

            ml[lvl] = abs(sums[lvl][0])
            Vl[lvl] = M/max(M-1,1)*max(0.,sums[lvl][1] - sums[lvl][0]**2)/M
            pl[lvl] = level.p

            # fix to cope with possible zero values for ml
            # (can happen in some applications when there are few samples)
            if lvl > 0:
                ml[lvl] = max(ml[lvl], 0.5*ml[lvl-1]/2**alpha)

        #err = max([ml[L + i - 1]*2.**(alpha*i) for i in range(-2,1)])/(2.**alpha - 1)
        err = ml[L - 1]/(2.**alpha - 1.)

        if err > eps*sqrt(theta) or L < 3:
            if L == Lmax:
                if mlmc_sampler.mpiRank == 0:
                    print("WARNING: WEAK CONVERGENCE FAILURE!!! Increase the maximum refinement level.\n")
                break;

            level = MLQMCLevel(L)
            levels += [level]
            sums += [level.compute_sums()]
            Cl   += [level.cost/(2**level.p)]
            pl   += [level.p]

            # fix to cope with possible zero values for ml
            # (can happen in some applications when there are few samples)
            ml += [max(abs(sums[-1][0]), 0.5*ml[-1]/2**alpha)]
            Vl += [M/max(M-1,1)*max(0.,sums[-1][1] - sums[-1][0]**2)/M]

            L += 1

        else:
            # if the bias is below the tolerance, then we already know the variance
            # also below the tolerance so MLQMC has converged!
            break;

    P = sum([Sum[0] for Sum in sums])
    Nl = [2**level.p for level in levels]

    return P, array(Nl), array(Cl) 
