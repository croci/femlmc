#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import numpy
from numpy import sqrt
from dolfin import MPI
import sys

from .mlmc import mlmc

comm = MPI.comm_world
mpiRank = MPI.rank(comm)

# MLMC helper class
class MLMCOutput(object):
    def __init__(self, N, Lmin, norm, richardson_alpha):
        self.N = N
        self.Lmin = Lmin
        self.del1 = [0. for i in range(Lmin)]
        self.del2 = [0. for i in range(Lmin)]
        self.var1 = [0. for i in range(Lmin)]
        self.var2 = [0. for i in range(Lmin)]
        self.kur1 = [0. for i in range(Lmin)]
        self.chk1 = [0. for i in range(Lmin)]
        self.mlmc_parameters = None
        self.norm = norm
        # richardson extrapolation requires knowing in advance the MLMC alpha parameter
        self.r_alpha = richardson_alpha
        if self.r_alpha is not None:
            self.del1true = []

    def test_sums(self, sums, l, logfile=None):

        if self.r_alpha is not None:
            a = 2**self.r_alpha/(2**self.r_alpha - 1)
            b =               1/(2**self.r_alpha - 1)

        for i in range(len(sums)):
            sums[i] = sums[i]/self.N

        if isinstance(sums[5], float):

            if self.r_alpha is None:
                self.del1.append(sums[0])
            else:
                if l == self.Lmin:
                    self.del1true.append(sums[0])
                    self.del1.append(a*sums[0])
                else:
                    self.del1true.append(sums[0])
                    self.del1.append(a*sums[0] - b*self.del1true[l-1])

            if l == 0:
                kurt = 0.0
            else:
                try: kurt =  (sums[3]
                          - 4*sums[2]*sums[0]
                          + 6*sums[1]*sums[0]**2
                          - 3*sums[0]*sums[0]**3)/(sums[1] - sums[0]**2)**2
                except ZeroDivisionError: kurt = 0.0

            self.del2.append(sums[4])
            self.var1.append(sums[1]-sums[0]**2)
            self.var2.append(max(sums[5]-sums[4]**2, 1.0e-15)) # fix for cases with var = 0
            self.kur1.append(kurt)

            if l == self.Lmin:
                check = 0
            else:
                try:
                    check = abs(self.del1[l] + self.del2[l-1] - self.del2[l])
                    if self.r_alpha is None:
                        check = check/(3.0*(sqrt(self.var1[l]) + sqrt(self.var2[l-1]) + sqrt(self.var2[l]))/sqrt(self.N))
                    else:
                        check = check/(3.0*(sqrt(max(0, (a**2-2*a*b)*self.var1[l] + b**2*self.var1[l-1])) + sqrt(self.var2[l-1]) + sqrt(self.var2[l]))/sqrt(self.N))
                    #if self.r_alpha is None:
                    #    check = abs(self.del1[l] + self.del2[l-1] - self.del2[l])
                    #    check = check/(3.0*(sqrt(self.var1[l]) + sqrt(self.var2[l-1]) + sqrt(self.var2[l]))/sqrt(self.N))
                    #else:
                    #    check = abs(a*self.del1[l] - b*self.del1[l-1] + self.del2[l-1] - self.del2[l])
                    #    check = check/(3.0*(sqrt(max(0, (a**2-2*a*b)*self.var1[l] + b**2*self.var1[l-1])) + sqrt(self.var2[l-1]) + sqrt(self.var2[l]))/sqrt(self.N))
                except ZeroDivisionError:
                    check = 0.0

            self.chk1.append(check)

            write(logfile, "%2d   %8.4e  %8.4e  %8.4e  %8.4e  %8.4e  %8.4e \n" % \
                          (l, self.del1[l], self.del2[l], self.var1[l], self.var2[l], self.kur1[l], self.chk1[l]))

            if self.kur1[-1] > 100.0:
                write(logfile, "\n WARNING: kurtosis on finest level = %f \n" % self.kur1[-1]);
                write(logfile, " indicates MLMC correction dominated by a few rare paths; \n");
                write(logfile, " for information on the connection to variance of sample variances,\n");
                write(logfile, " see http://mathworld.wolfram.com/SampleVarianceDistribution.html\n\n");

            if self.chk1[-1] > 1.0:
                write(logfile, "\n WARNING: maximum consistency error = %f \n" % self.chk1[-1])
                write(logfile, " indicates identity E[Pf-Pc] = E[Pf] - E[Pc] not satisfied \n\n")

        else:

            #NOTE: avoid computing kurtosis if the output is not a scalar
            if self.norm is None:
                raise ValueError("No norm specified for non-scalar outputs. Note that non-scalar outputs are not supported for QMC and MLQMC.")

            if self.r_alpha is None:
                self.del1.append(self.norm(sums[0]))
            else:
                if l == self.Lmin:
                    self.del1true.append(sums[0])
                    self.del1.append(a*self.norm(sums[0]))
                else:
                    self.del1true.append(sums[0])
                    self.del1.append(a*self.norm(sums[0]) - b*self.norm(self.del1true[l-1]))

            # NOTE: checking the second moment of the error rather than the variance as computing the variance as E[X^2] - E[X]^2
            #       only works if the norm used is induced by an inner product
            # FIXME: could check whether the norm is induced by an inner product. In that case you can use the trick.
            self.var1.append(sums[1])
            self.del2.append(sums[4])
            if isinstance(sums[5], numpy.ndarray):
                self.var2.append(numpy.maximum(sums[5]-sums[4]**2, 1.0e-15)) # fix for cases with var = 0
            else:
                self.var2.append(sums[5]-sums[4]**2)

            if l == self.Lmin:
                check = 0
            else:
                try:
                    if isinstance(sums[0], numpy.ndarray):
                        if self.r_alpha is None:
                            check = numpy.abs(sums[0] + self.del2[l-1] - self.del2[l])
                            check = abs(check/(3.0*(sqrt(self.var1[l]) + numpy.sqrt(self.var2[l-1]) + numpy.sqrt(self.var2[l]))/sqrt(self.N))).max()
                        else:
                            check = numpy.abs(a*sums[0] - b*self.del1true[l-1] + self.del2[l-1] - self.del2[l])
                            check = abs(check/(3.0*(sqrt(max(0, (a**2-2*a*b)*self.var1[l] + b**2*self.var1[l-1])) + numpy.sqrt(self.var2[l-1]) + numpy.sqrt(self.var2[l]))/sqrt(self.N))).max()
                    else:
                        if self.r_alpha is None:
                            check = self.norm(sums[0] + self.del2[l-1] - self.del2[l])
                            #FIXME: the following check is a bit odd, since the norms are swapped, oh well
                            check = check/(3.0*(sqrt(self.var1[l]) + sqrt(self.norm(self.var2[l-1])) + sqrt(self.norm(self.var2[l])))/sqrt(self.N))
                        else:
                            check = self.norm(a*sums[0] - b*self.del1true[l-1] + self.del2[l-1] - self.del2[l])
                            #FIXME: the following check is a bit odd, since the norms are swapped, oh well
                            check = check/(3.0*(sqrt(max(0, (a**2-2*a*b)*self.var1[l] + b**2*self.var1[l-1])) + sqrt(self.norm(self.var2[l-1])) + sqrt(self.norm(self.var2[l])))/sqrt(self.N))

                except ZeroDivisionError:
                    check = 0.0

            self.chk1.append(check)

            write(logfile, "%2d   %8.4e  %8.4e  %8.4e  %8.4e  %8.4e  %8.4e \n" % \
                          (l, self.del1[l], self.norm(self.del2[l]), self.var1[l], self.norm(self.var2[l]), numpy.nan, self.chk1[l]))

            if self.chk1[-1] > 1.0:
                write(logfile, "\n WARNING: maximum consistency error = %f \n" % self.chk1[-1])
                write(logfile, " indicates identity E[Pf-Pc] = E[Pf] - E[Pc] not satisfied \n\n")

    def print_array_results(self, logfile):
        if not isinstance(self.del2[-1], float):
            for l in range(len(self.del2)):
                write(logfile, "\nLevel %d\n" % l)
                write(logfile, "Expected value: %s\n" % self.del2[l])
                write(logfile, "Variance: %s\n" % self.var2[l])

    def test_parameters(self, cost, refinement_level, logfile):
        init = max(self.Lmin, 1)
        LL = numpy.arange(len(refinement_level))[init:][-3:]
        # NOTE the case in which the cost increases polynomially and the convergence is geometric is currently not supported
        tempdel = numpy.array(numpy.abs(self.del1))[init:]; tempvar = numpy.array(numpy.abs(self.var1))[init:];
        pa = numpy.polyfit(LL, numpy.log2(tempdel[-3:]), 1);                  alpha = -pa[0];
        pb = numpy.polyfit(LL, numpy.log2(tempvar[-3:]), 1);                  beta  = -pb[0];
        pc = numpy.polyfit(LL, numpy.log2(numpy.array(cost)[init:][-3:]), 1); gamma  = pc[0];
                      
        write(logfile, "\n******************************************************\n");
        write(logfile, "*** Linear regression estimates of MLMC parameters ***\n");
        write(logfile, "******************************************************\n");
        write(logfile, "\n alpha = %f  (exponent for MLMC weak convergence)\n" % alpha);
        write(logfile, " beta  = %f  (exponent for MLMC variance) \n" % beta);
        write(logfile, " gamma = %f  (exponent for MLMC cost) \n" % gamma);

        self.mlmc_parameters = (alpha, beta, gamma)

def mlmc_test(mlmc_sampler, N, N0, Eps=None, Lmin = 0, theta=0.5, logfile=None, mc_only=True):
    """
    Multilevel Monte Carlo test routine.

    mlmc_sampler: an FEMLMC MLMCSampler object. Its interface is given by
      sums = mlmc_sampler.sample(N, l=l)
    with inputs
      l = level
      N = number of paths
    and a numpy array of outputs
      sums[0] = sum(Pf-Pc)
      sums[1] = sum((Pf-Pc)**2)
      sums[2] = sum((Pf-Pc)**3)
      sums[3] = sum((Pf-Pc)**4)
      sums[4] = sum(Pf)
      sums[5] = sum(Pf**2)

    N:                     number of samples for convergence tests
    mlmc_sampler.n_levels: number of levels  for convergence tests

    N0:                    initial number of samples for MLMC calculations
    Eps:                   desired accuracy array for MLMC calculations
    Lmin:                  minimum level used for MLMC calculations
    theta:                 statistical error vs bias weight parameter
    mc_only:               terminate test routine soon after the MLMC parameter estimation

    """

    # First, convergence tests

    L                = mlmc_sampler.n_levels - 1
    nout             = mlmc_sampler.output_functional.get_n_outputs()
    norms            = [lambda x : mlmc_sampler.norm(x, i) for i in range(nout)]
    mlmc_outputs = [MLMCOutput(N, Lmin, norms[i], mlmc_sampler.richardson_alpha) for i in range(nout)]
    refinement_level = 1.0/numpy.array(mlmc_sampler.num_dofs)**(1.0/mlmc_sampler.tdim)

    cost      = [0. for i in range(Lmin)]
    sums_list = []

    write(logfile, "\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "*** Convergence tests, kurtosis, telescoping sum check ***\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "\n l   ave(Pf-Pc)    ave(Pf)   var(Pf-Pc)    var(Pf)")
    write(logfile, "    kurtosis     check \n-------------------------")
    write(logfile, "--------------------------------------------------\n")

    for l in range(Lmin, L+1):
        sums, costl = mlmc_sampler.sample(N,l=l)
        cost.append(costl/float(N))
        sums_list.append(sums)

        # compute tests for the first output on-the-go
        # and at the end for the others
        temp_sum = numpy.array([sums[0][i] for i in range(6)])
        mlmc_outputs[0].test_sums(temp_sum, l, logfile)

    mlmc_outputs[0].print_array_results(logfile)
    mlmc_outputs[0].test_parameters(cost, refinement_level, logfile)

    for k in range(1,nout):

        write(logfile, "\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "*** Convergence tests, kurtosis, telescoping sum check ***\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "\n l   ave(Pf-Pc)    ave(Pf)   var(Pf-Pc)    var(Pf)")
        write(logfile, "    kurtosis     check \n-------------------------")
        write(logfile, "--------------------------------------------------\n")

        for l in range(Lmin, L+1):
            temp_sum = numpy.array([sums_list[l-Lmin][k][i] for i in range(6)])
            mlmc_outputs[k].test_sums(temp_sum, l, logfile)

        mlmc_outputs[k].print_array_results(logfile)
        mlmc_outputs[k].test_parameters(cost, refinement_level, logfile)

    if mc_only == True:
        return

    # FIXME, the following code currently only works for one output

    # Second, MLMC complexity tests

    write(logfile, "\n");
    write(logfile, "***************************** \n");
    write(logfile, "*** MLMC complexity tests *** \n");
    write(logfile, "***************************** \n\n");
    write(logfile, "    eps     mlmc_cost   std_cost  savings       N_l \n");
    write(logfile, "--------------------------------------------------- \n");

    prms = numpy.vstack([mlmc_outputs[k].mlmc_parameters for k in range(nout)])
    (alpha, beta, gamma) = (prms[:,0].min(), prms[:,1].min(), prms[:,2].max())

    #FIXME: the next line should be removed, but at the moment we are only considering the first output functional anyways
    (alpha, beta, gamma) = mlmc_outputs[0].mlmc_parameters

    for eps in Eps:
       (P, Nl, Cl) = mlmc(mlmc_sampler, N0, eps, alpha, beta, gamma, theta)
       l = len(Nl) - 1
       mlmc_cost = sum(Nl * Cl)
       Vl = max([norms[k](mlmc_outputs[k].var2[l]) for k in range(nout)])
       #FIXME: the next line should be removed, but at the moment we are only considering the first output functional anyways
       Vl = norms[0](mlmc_outputs[0].var2[l])
       std_cost  = Vl*Cl[l] /((1.0 - theta)*eps**2)

       write(logfile, "%.4e  %.3e  %.3e  %7.2f " % (eps, mlmc_cost, std_cost, std_cost/mlmc_cost))
       write(logfile, " ".join(["%9d" % n for n in Nl]))
       write(logfile, "\n")

    write(logfile, "\n")

def write(logfile, msg):
    """
    Write to both sys.stdout and to a logfile.
    """
    if mpiRank == 0:
        if logfile is not None:
            logfile.write(msg)
            logfile.flush()
        sys.stdout.write(msg)

