#!// vim: set ft=cpp:
#FIXME we work on the unit square/cube so we never risk that an element of meshA is fully outside of meshB.
#      however, what would happen if that was the case? Probably we need to account for that
#
#NOTE: To enable debug mode use -D DEBUG compiler flag

#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

supermesh_code = r'''
#include <iostream>
#include <ctime>

#include "dolfin.h"
#include "libsupermesh-c.h"
#include <math.h>
#include <Eigen/Dense>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/pytypes.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/cast.h>

namespace py = pybind11;

namespace dolfin {

    static std::pair<Eigen::MatrixXd, Eigen::VectorXd> make_affine_mapping(const double* xs, const double* ys, const int dim)
    {
        int row_cur=0, col_start = 0, col_finish=0;

        Eigen::MatrixXd mat = Eigen::MatrixXd::Zero(dim*(dim+1), dim*(dim+1));
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dim*(dim+1));

        for(int i=0;i<dim+1;i++){
            for(int j=0;j<dim;j++){
                row_cur = i*dim + j;
                col_start = dim * j;
                col_finish = col_start + dim;

                for(int k=col_start;k<col_finish;k++)
                    mat(row_cur, k) = xs[i*dim + k - col_start];

                rhs(row_cur) = ys[i*dim + j];
                mat(row_cur, dim*dim + j) = 1.0;
            }
        }

        Eigen::VectorXd sol = mat.colPivHouseholderQr().solve(rhs);
        // NOTE: for some reason you get that A is transposed, hence we transpose it ourselves :)
        Eigen::MatrixXd A = Eigen::Map<Eigen::MatrixXd>(sol.data(), dim, dim).transpose();
        Eigen::VectorXd b = Eigen::VectorXd::Zero(dim);
        for(int i=0;i<dim;i++)
            b(i) = sol(dim*dim + i);
        //Eigen::VectorXd b = Eigen::Map<Eigen::VectorXd>(sol.tail(dim).data(), dim);

        return std::make_pair(A,b);
    }

    static std::vector<double> map_dofs(double* ref_simplex_coor, double* ref_dofs_coor, double* simplex_coor, const int dim, const int n_dofs){

        std::pair<Eigen::MatrixXd, Eigen::VectorXd> pair = make_affine_mapping(ref_simplex_coor, simplex_coor, dim);

        Eigen::MatrixXd A = pair.first;
        Eigen::VectorXd b = pair.second;

        Eigen::MatrixXd ref_nodes = Eigen::MatrixXd::Zero(n_dofs, dim);

        for(int i=0;i<n_dofs;i++)
            for(int j=0;j<dim;j++)
                ref_nodes(i,j) = ref_dofs_coor[i*dim + j];

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> out = (A*ref_nodes.transpose()).transpose();
        out.rowwise() += b.transpose();
        
        std::vector<double> out_vec(out.data(), out.data() + out.rows() * out.cols());

        return out_vec;
    }


class SuperMeshConstructor
{

    public:
    SuperMeshConstructor(std::shared_ptr<const FunctionSpace> Va, std::shared_ptr<const FunctionSpace> Vb, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs);
        Eigen::VectorXd get_full_interp(int i);
        Eigen::VectorXd get_supermesh_element_volumes();
        Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> get_intersection_map();

        std::vector<double> full_interpA;
        std::vector<double> full_interpB;
        std::vector<double> supermesh_element_volumes;
        std::vector<std::size_t> intersection_map;

        #ifdef DEBUG
            Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> get_n_intersection_candidates();
            Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> get_n_actual_intersections();
            Eigen::VectorXd get_duration();

            std::vector<double> duration;
            std::vector<std::size_t> n_intersection_candidates;
            std::vector<std::size_t> n_actual_intersections;
        #endif

    private:

};

SuperMeshConstructor::SuperMeshConstructor(std::shared_ptr<const FunctionSpace> Va, std::shared_ptr<const FunctionSpace> Vb, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs)
{

#ifdef DEBUG
    std::clock_t start, start2;
    start2 = std::clock();
    duration.resize(4);
    std::fill(duration.begin(), duration.end(), 0.0);
#endif

std::shared_ptr<const Mesh> meshA = Va->mesh();
std::shared_ptr<const Mesh> meshB = Vb->mesh();

auto feA = Va->element();
auto feB = Vb->element();

Cell cellA;
Cell cellB;

ufc::cell ufc_cellA;
ufc::cell ufc_cellB;

std::vector<double> coordinate_dofsA;
std::vector<double> coordinate_dofsB;

const unsigned int space_dimA = feA->space_dimension();
const unsigned int space_dimB = feB->space_dimension();

// check that the finite element ranks agree
dolfin_assert(feA->value_rank() == feB->value_rank());

// Number of dofs associated with each fine point
unsigned int data_size = 1;
for (unsigned data_dim = 0; data_dim < feA->value_rank(); data_dim++){
  data_size *= feA->value_dimension(data_dim);
  dolfin_assert(feA->value_dimension(data_dim) == feB->value_dimension(data_dim));
}

std::size_t nelements_a = meshA->num_cells();
std::size_t nelements_b = meshB->num_cells();

std::size_t mA   = space_dimA*data_size;
std::size_t mB   = space_dimB*data_size;
std::size_t mmax = std::max(mA, mB);
//std::size_t mmin = std::min(mA, mB);

// initialise vector where to store the evaluated basis functions
std::vector<double> evaluation_values(mA + mB);
// initialise local interpolation matrices
std::vector<double> local_interpA(mmax*mA, 0.0);
std::vector<double> local_interpB(mmax*mB, 0.0);

// extract mesh vertex coordinates, we need to reshape them into
// a matrix of size geometric_dimension-by-number_of_vertices
std::vector<double> coorA = meshA->coordinates(); // coorA is the flattened version of a [nnodes_a][gdim] array
std::vector<double> coorB = meshB->coordinates(); // coorB is the flattened version of a [nnodes_b][gdim] array

int gdim = meshA->geometry().dim();
int tdim = meshA->topology().dim();
std::size_t nnodes_a = meshA->num_vertices();
std::size_t nnodes_b = meshB->num_vertices();

// see Patrick's paper for the number of elements of the supermesh
int cdim;
if(tdim == 1)
    cdim = 1;
else if(tdim == 2)
    cdim = 4;
else
    cdim = 45;

full_interpA.reserve(cdim*mA*mB*std::max(nelements_a, nelements_b));
full_interpB.reserve(cdim*mA*mB*std::max(nelements_a, nelements_b));

// extract mesh elements/cells to vertices maps, we need to reshape them into
// a matrix of size element_dimension-by-number_of_elements
std::vector<unsigned int> cellsA = meshA->cells(); // cellsA is the flattened version of a [nelements_a][eldim] array
std::vector<unsigned int> cellsB = meshB->cells(); // cellsB is the flattened version of a [nelements_b][eldim] array

// convert to a vector of long (needed for libsupermesh)
std::vector<long> enlist_a(cellsA.begin(), cellsA.end());
std::vector<long> enlist_b(cellsB.begin(), cellsB.end());

// eldim is the number of vertices of every mesh element
int eldim = tdim + 1;

#ifdef DEBUG
    start = std::clock();
#endif

if(tdim == 1)
    libsupermesh_sort_intersection_finder_set_input((long*) &nnodes_a, (int*) &gdim, (long*) &nelements_a, (int*) &eldim, (long*) &nnodes_b, (int*) &gdim, (long*) &nelements_b, (int*) &eldim, (double*) coorA.data(), (long*) enlist_a.data(), (double*) coorB.data(), (long*) enlist_b.data());
else
    libsupermesh_tree_intersection_finder_set_input((long*) &nnodes_a, (int*) &gdim, (long*) &nelements_a, (int*) &eldim, (long*) &nnodes_b, (int*) &gdim, (long*) &nelements_b, (int*) &eldim, (double*) coorA.data(), (long*) enlist_a.data(), (double*) coorB.data(), (long*) enlist_b.data());

std::size_t nindices_ab;

if(tdim == 1)
    libsupermesh_sort_intersection_finder_query_output((long*) &nindices_ab);
else
    libsupermesh_tree_intersection_finder_query_output((long*) &nindices_ab);

// nelements_a are the rows, the columns are given by indices_ab,
// indices_ab[ind_ptr_ab[i]:ind_ptr_ab[i+1]] contains the indices of the elements of mesh B
// which are likely to intersect with the i-th element of mesh A.
std::size_t* indices_ab = new std::size_t[nindices_ab];
std::size_t* ind_ptr_ab = new std::size_t[nelements_a + 1];
if(tdim == 1)
    libsupermesh_sort_intersection_finder_get_output((long*) &nelements_a, (long*) &nindices_ab, (long*) indices_ab, (long*) ind_ptr_ab);
else
    libsupermesh_tree_intersection_finder_get_output((long*) &nelements_a, (long*) &nindices_ab, (long*) indices_ab, (long*) ind_ptr_ab);

#ifdef DEBUG
    duration[0] = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
#endif

// NOTE: the following flattened version is equivalent
//       to the two lines below, but it works even
//       if gdim is not known at compile time
//auto tri_a = new double[eldim][gdim];
//auto tri_b = new double[eldim][gdim];
double* tri_a = new double[eldim*gdim];
double* tri_b = new double[eldim*gdim];

int n_tris_c = 0;

// need to preallocate tris_c
double* tris_c = nullptr;
// flattened version of auto tris_c = new double[TRI_BUF_SIZE][eldim][gdim]
// need to flatten as eldim and gdim are not known at compile time
if(tdim==1)
    tris_c = new double[eldim*gdim];
else if(tdim == 2)
    tris_c = new double[TRI_BUF_SIZE*eldim*gdim];
else
    tris_c = new double[TET_BUF_SIZE*eldim*gdim];

double* tris_flat = new double[eldim*gdim];
double tris_flat_volume = 0.0;
intersection_map.reserve(cdim*2*std::max(nelements_a, nelements_b));
supermesh_element_volumes.reserve(cdim*std::max(nelements_a, nelements_b));
#ifdef DEBUG
    n_intersection_candidates.reserve(nelements_a);
    n_actual_intersections.reserve(nelements_a);
#endif
int how_many_intersect = 0;
std::size_t elemB;

for(std::size_t i = 0; i < nelements_a; i++){

    how_many_intersect = ind_ptr_ab[i+1] - ind_ptr_ab[i];
    #ifdef DEBUG
        n_intersection_candidates.push_back(how_many_intersect);
        n_actual_intersections.push_back(0);
    #endif

    cellA = Cell(*meshA, i);
    cellA.get_coordinate_dofs(coordinate_dofsA);
    cellA.get_cell_data(ufc_cellA);
    
    for (int l = 0; l < eldim; l++){
        for (int k = 0; k < gdim; k++){
            tri_a[l*gdim + k] = coorA[enlist_a[i*eldim + l]*gdim + k];
        }
    }

    for(int j = 0; j < how_many_intersect; j++){
        
        elemB = indices_ab[ind_ptr_ab[i] + j];
        cellB = Cell(*meshB, elemB);
        cellB.get_coordinate_dofs(coordinate_dofsB);
        cellB.get_cell_data(ufc_cellB);

        for (int l = 0; l < eldim; l++){
            for (int k = 0; k < gdim; k++){
                tri_b[l*gdim + k] = coorB[enlist_b[elemB*eldim + l]*gdim + k];
            }
        }

        #ifdef DEBUG
            start = std::clock();
        #endif
        if(tdim == 1)
            libsupermesh_intersect_intervals((double*) tri_a, (double*) tri_b, (double*) tris_c, (int*) &n_tris_c);
        else if(tdim == 2)
            libsupermesh_intersect_tris_real((double*) tri_a, (double*) tri_b, (double*) tris_c, (int*) &n_tris_c);
        else
            libsupermesh_intersect_tets_real((double*) tri_a, (double*) tri_b, (double*) tris_c, (int*) &n_tris_c);

        #ifdef DEBUG
            duration[1] += ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
            if(n_tris_c > 0)
                n_actual_intersections.back() += 1;
        #endif

        for(int kk = 0; kk < n_tris_c; kk++){
            for(int ii = 0; ii<eldim; ii++){
                for(int jj = 0; jj<gdim; jj++){
                    tris_flat[ii*gdim + jj] = tris_c[(kk*eldim + ii)*gdim + jj];
                }
            }

            if(tdim == 1)
                libsupermesh_interval_size(tris_flat, (double*) &tris_flat_volume);
            else if(tdim == 2)
                libsupermesh_triangle_area(tris_flat, (double*) &tris_flat_volume);
            else
                libsupermesh_tetrahedron_volume(tris_flat, (double*) &tris_flat_volume);

            std::fill(local_interpA.begin(), local_interpA.end(), 0.0);
            std::fill(local_interpB.begin(), local_interpB.end(), 0.0);

            #ifdef DEBUG
                start = std::clock();
            #endif

            std::vector<double> tris_c_dofs = map_dofs(reference_simplex_coor.data(), reference_dofs.data(), tris_flat, gdim, mmax);

            for(std::size_t iter = 0; iter < mmax; iter++){

                feA->evaluate_basis_all(evaluation_values.data(), (double*) &tris_c_dofs[gdim*iter], coordinate_dofsA.data(), ufc_cellA.orientation);
                feB->evaluate_basis_all((double*) &evaluation_values[mA], (double*) &tris_c_dofs[gdim*iter], coordinate_dofsB.data(), ufc_cellB.orientation);

                // note that the local interpolation matrices here are "transposed", i.e. they are stored so that their sizes are mA (or mB)-by-mmax and not vice-versa
                for(std::size_t m = 0; m < mA; m++)
                    local_interpA[iter + m*mA] = evaluation_values[m];

                for(std::size_t m = 0; m < mB; m++)
                    local_interpB[iter + m*mB] = evaluation_values[mA + m];
            }

            // add intersection info to the intersection map: the cells of index i and elemB intersect.
            intersection_map.push_back(i);
            intersection_map.push_back(elemB);
            supermesh_element_volumes.push_back(std::sqrt(tris_flat_volume));
            full_interpA.insert(full_interpA.end(), local_interpA.begin(), local_interpA.end());
            full_interpB.insert(full_interpB.end(), local_interpB.begin(), local_interpB.end());
            #ifdef DEBUG
                duration[2] += ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
            #endif
        }
    }
}

// cleaning up the memory
delete [] tri_a;
delete [] tri_b;
delete [] tris_c;
delete [] tris_flat;
delete [] indices_ab;
delete [] ind_ptr_ab;

full_interpA.shrink_to_fit();
full_interpB.shrink_to_fit();
intersection_map.shrink_to_fit();
supermesh_element_volumes.shrink_to_fit();

#ifdef DEBUG
    duration[3] += ( std::clock() - start2 ) / (double) CLOCKS_PER_SEC;
#endif

}

Eigen::VectorXd SuperMeshConstructor::get_full_interp(int i){
        if(i==0)
            return Eigen::Map<Eigen::VectorXd>(full_interpA.data(), full_interpA.size());
        else
            return Eigen::Map<Eigen::VectorXd>(full_interpB.data(), full_interpB.size());
}

Eigen::VectorXd SuperMeshConstructor::get_supermesh_element_volumes(){
        return Eigen::Map<Eigen::VectorXd>(supermesh_element_volumes.data(), supermesh_element_volumes.size());
}

Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> SuperMeshConstructor::get_intersection_map(){
        return Eigen::Map<Eigen::Matrix<std::size_t, Eigen::Dynamic, 1>>(intersection_map.data(), intersection_map.size());
}

#ifdef DEBUG
    Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> SuperMeshConstructor::get_n_intersection_candidates(){
            return Eigen::Map<Eigen::Matrix<std::size_t, Eigen::Dynamic, 1>>(n_intersection_candidates.data(), n_intersection_candidates.size());
    }

    Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> SuperMeshConstructor::get_n_actual_intersections(){
            return Eigen::Map<Eigen::Matrix<std::size_t, Eigen::Dynamic, 1>>(n_actual_intersections.data(), n_actual_intersections.size());
    }

    Eigen::VectorXd SuperMeshConstructor::get_duration(){
            return Eigen::Map<Eigen::VectorXd>(duration.data(), duration.size());
    }
#endif

PYBIND11_MODULE(SIGNATURE, m){
        py::class_<SuperMeshConstructor>
          (m, "SuperMeshConstructor")
          .def(py::init([](py::object Va, py::object Vb, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs)
          {
              auto _Va = Va.attr("_cpp_object").cast<std::shared_ptr<const FunctionSpace>>();
              auto _Vb = Vb.attr("_cpp_object").cast<std::shared_ptr<const FunctionSpace>>();
              return SuperMeshConstructor(_Va, _Vb, reference_simplex_coor, reference_dofs);
          
          }))
          .def("get_full_interp", &SuperMeshConstructor::get_full_interp)
          .def("get_supermesh_element_volumes", &SuperMeshConstructor::get_supermesh_element_volumes)
          #ifdef DEBUG
          .def("get_n_intersection_candidates", &SuperMeshConstructor::get_n_intersection_candidates)
          .def("get_n_actual_intersections", &SuperMeshConstructor::get_n_actual_intersections)
          .def("get_duration", &SuperMeshConstructor::get_duration)
          #endif
          .def("get_intersection_map", &SuperMeshConstructor::get_intersection_map);
}

}

'''

if __name__ == "__main__":

    from dolfin import *
    from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs
    import os

    include_dirs = _libsupermesh_include_dirs
    library_dirs = _libsupermesh_library_dirs
    libraries = "supermesh"

    compiled_code = compile_cpp_code(supermesh_code, cppargs=["-O0", "-g", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

    SuperMeshConstructor = compiled_code.SuperMeshConstructor

    import numpy as np
    import mshr
    #mesh = IntervalMesh(103, 0, 1)
    #mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 50)
    mesh = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 10)
    mesh2 = BoxMesh.create([Point(0,0,0), Point(1,1,1)], [2,2,2], CellType.Type.tetrahedron)
    V = FunctionSpace(mesh, "CG", 2)
    U = FunctionSpace(mesh2, "CG", 1)

    eldim1 = V.element().space_dimension()

    from FIAT import reference_element as re
    import ffc.fiatinterface as ft
    import itertools

    ref_el = np.vstack(re.ufc_simplex(mesh.topology().dim()).get_vertices()).flatten()
    fe = ft.create_element(V.ufl_element())
    reference_nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten()

    constructor = SuperMeshConstructor(V, U, ref_el, reference_nodes)

    full_interp1 = constructor.get_full_interp(0)
    full_interp2 = constructor.get_full_interp(1)

    volumes = constructor.get_supermesh_element_volumes()

    n_supermesh_cells = len(volumes)

    int_map = constructor.get_intersection_map()

    #candidates = constructor.get_n_intersection_candidates()

    #act_int = constructor.get_n_actual_intersections()

    #duration = constructor.get_duration()

    print(volumes.shape[0]/mesh.num_cells())
    #assert (candidates.astype(np.int)-act_int.astype(np.int)).min() >= 0
    assert ((volumes**2).sum() - 1) < 3*DOLFIN_EPS
    assert (int(full_interp1.sum()) - n_supermesh_cells*eldim1) == 0

    print("TEST PASSED!")
