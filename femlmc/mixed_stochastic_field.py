#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import MPI
from petsc4py import PETSc
from .stochastic_field import StochasticField, to_levels
from sys import modules

class MixedStochasticField(StochasticField):
    def __init__(self, stochastic_fields, **kwargs):

        self.RNG = getattr(modules['femlmc'], '__RNG__')
        if self.RNG is None:
            raise RuntimeError("Before using femlmc a random number generator must be set with a call to setRNG")

        self.field_type = "mixed"

        if not isinstance(stochastic_fields, list) or len(stochastic_fields) <= 1:
            raise ValueError("Input must be a list of StochasticFields of length > 1")

        self.stochastic_fields = stochastic_fields

        self.qmc_flag = any([field.qmc_flag for field in stochastic_fields])

        if len(set(stochastic_fields)) != len(stochastic_fields):
            if self.qmc_flag:
                raise ValueError("For QMC and MLQMC the list of input fields cannot contain repetitions of the same random field object!")

        self.n_fields = len(stochastic_fields)

        worldcomm = kwargs.get("worldcomm", MPI.comm_world).Dup()
        kwargs["worldcomm"] = worldcomm
        self.mpiRank = MPI.rank(worldcomm)
        self.mpiSize = MPI.size(worldcomm)
        self.worldComm = worldcomm

        self.params = kwargs

        for field in self.stochastic_fields:
            if isinstance(field, StochasticField):
                self.function_spaces = field.function_spaces
                break

        self._independent = kwargs.get("independent", False)
        self._old_level = kwargs.get("old_level", None)

        self.setup_levels()

        self.setup()

    def sample(self, l):
        xrnge = 2 - (l is 0)
        samples = [[] for i in range(xrnge)]
        for field in self.stochastic_fields:
            sample = field.sample(l)
            [samples[i].append(sample[i]) for i in range(xrnge)]
        return tuple(samples)

    def _initialise_GQRNG(self):
        for field in self.stochastic_fields:
            field._initialise_GQRNG()

    def get_subfield(self,i):
        return self.stochastic_fields[i]

    def get_independent_field(self, l):
        # get an independent random field from level l
        # the following is just so that -1 maps to the largest level
        while l < 0:
            l += self.n_levels
        if l >= self.n_levels:
            raise ValueError('Level specified is larger than the current maximum level')

        indep_fields = []
        for field in self.stochastic_fields:
            indep_fields.append(field.get_independent_field(l))

        return MixedStochasticField(indep_fields, independent = True, old_level = l, **self.get_independent_parameters(l))

    def setup(self):
        pass

    def setup_levels(self):
        for field in self.stochastic_fields:
            if isinstance(field, StochasticField):
                self.n_levels         = field.n_levels
                break
