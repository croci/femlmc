#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import numpy as np
from numpy import sqrt
from dolfin import MPI
import sys

from .qmc import qmc
from .mlqmc import mlqmc
from .mlmc_test import MLMCOutput

comm = MPI.comm_world
mpiRank = MPI.rank(comm)

def mlqmc_test(mlmc_sampler, Eps, Lmin = 0, logfile=None, alpha = None, gamma = None, N = None, theta=0.5, qmc_only = False):
    """
    Multilevel Quasi Monte Carlo test routine.

    mlmc_sampler: an femlmc MLMCSampler object set up for MLQMC.
    Its interface is given by
      sums = mlmc_sampler.sample(N, l=l)
    with inputs
      l = level
      N = number of paths
    and a numpy array of outputs
      sums[0] = sum(Pf-Pc)
      sums[1] = sum((Pf-Pc)**2)
      sums[2] = sum((Pf-Pc)**3)
      sums[3] = sum((Pf-Pc)**4)
      sums[4] = sum(Pf)
      sums[5] = sum(Pf**2)

    Eps:       desired accuracy array for MLMC calculations
    alpha:     weak convergence order parameter
    gamma:     rate of geometrical increase in cost with the level
    N:         number of samples for convergence tests
    theta:     statistical error vs bias weight in (0,1)
    qmc_only:  terminates test routine after the MLQMC parameter estimation
    
    if alpha and/or gamma are not provided, they will be estimated
    by using N samples on each level.


    """

    if N is not None:
        p = int(np.ceil(np.log2(N)))
        if abs(np.log2(N)-int(np.log2(N))) > 1.0e-12:
            N = 2**p
            if mpiRank == 0:
                print("Warning! For MLQMC and QMC the number of samples must be a power of 2. Setting N to the next power of two: N = %d" % N)

    else:
        p = 10
        N = 2**p

    M = mlmc_sampler.M
    n_outputs = mlmc_sampler.n_outputs
    howmanyshifts = mlmc_sampler.howmanyshifts

    if alpha is None or gamma is None:
        # First, convergence tests
        L                = mlmc_sampler.n_levels - 1
        #NOTE: We are passing M instead of N to the MLMCOutput class constructor as that is what needs to be done (see mlmc_sampler.mlqmc_finalise_sums(...) function)
        mlmc_outputs = [MLMCOutput(M, Lmin, None, None) for i in range(n_outputs)]
        refinement_level = 1.0/np.array(mlmc_sampler.num_dofs)**(1.0/mlmc_sampler.tdim)

        cost      = [0. for i in range(Lmin)]
        sums_list = []

        write(logfile, "\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "*** Convergence tests, kurtosis, telescoping sum check ***\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "\n l   ave(Pf-Pc)    ave(Pf)   var(Pf-Pc)    var(Pf)")
        write(logfile, "    kurtosis     check \n-------------------------")
        write(logfile, "--------------------------------------------------\n")

        for l in range(Lmin, L+1):
            out, costl = mlmc_sampler.sample(N,l=l)
            sums = mlmc_sampler.mlqmc_finalise_sums(out, N)
            cost.append(costl/float(N))
            sums_list.append(sums)

            # compute tests for the first output on-the-go
            # and at the end for the others
            temp_sum = np.array([sums[0][i] for i in range(6)])
            mlmc_outputs[0].test_sums(temp_sum, l, logfile)

        #mlmc_outputs[0].print_array_results(logfile)
        mlmc_outputs[0].test_parameters(cost, refinement_level, logfile)

        for k in range(1,n_outputs):

            write(logfile, "\n")
            write(logfile, "**********************************************************\n")
            write(logfile, "*** Convergence tests, kurtosis, telescoping sum check ***\n")
            write(logfile, "**********************************************************\n")
            write(logfile, "\n l   ave(Pf-Pc)    ave(Pf)   var(Pf-Pc)    var(Pf)")
            write(logfile, "    kurtosis     check \n-------------------------")
            write(logfile, "--------------------------------------------------\n")

            for l in range(Lmin, L+1):
                temp_sum = np.array([sums_list[l-Lmin][k][i] for i in range(6)])
                mlmc_outputs[k].test_sums(temp_sum, l, logfile)

            #mlmc_outputs[k].print_array_results(logfile)
            mlmc_outputs[k].test_parameters(cost, refinement_level, logfile)

        if qmc_only == True:
            return

        (alpha, _, tempgamma) = mlmc_outputs[0].mlmc_parameters

        if gamma == None:
            gamma = tempgamma

    # FIXME, the following code currently only works for one output

    # Second, MLMC complexity tests

    write(logfile, "\n");
    write(logfile, "****************************** \n");
    write(logfile, "*** MLQMC complexity tests *** \n");
    write(logfile, "****************************** \n\n");
    write(logfile, "  eps   mlmc_cost   std_cost  savings     N_l \n");
    write(logfile, "----------------------------------------------- \n");

    for eps in Eps:
       (P, Nl, Cl) = mlqmc(mlmc_sampler, eps, alpha = alpha, gamma = gamma, theta=theta)
       l = len(Nl) - 1
       mlqmc_cost = M*sum(Nl * Cl)

       # running standard QMC with the same error tolerance
       qmc_sampler = mlmc_sampler.get_independent_field(l)
       _, _, Nqmc = qmc_sampler.run(Nl[l], eps = sqrt(1 - theta)*eps) 
       std_cost  = M*Nqmc*Cl[l]

       write(logfile, "%.4e  %.3e  %.3e  %7.2f " % (eps, mlqmc_cost, std_cost, std_cost/mlqmc_cost))
       write(logfile, " ".join(["%9d" % n for n in Nl]))
       write(logfile, "\n")

    write(logfile, "\n")

def write(logfile, msg):
    """
    Write to both sys.stdout and to a logfile.
    """
    if mpiRank == 0:
        if logfile is not None:
            logfile.write(msg)
            logfile.flush()
        sys.stdout.write(msg)

