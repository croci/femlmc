#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

from .stochastic_field import StochasticField, to_levels
import numpy as np
from scipy.linalg import solve_triangular
from sys import modules
from scipy.sparse import csr_matrix
from time import time
import os

from .QMC_white_noise_coupling   import QMC_supermesh_code, HaarWhiteNoise
from .MLQMC_white_noise_coupling import MLQMC_supermesh_code, CoupledHaarWhiteNoise
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs
include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
compiled_MLQMC_code = compile_cpp_code(MLQMC_supermesh_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
compiled_QMC_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_QMC_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-Og", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_MLQMC_code = compile_cpp_code(MLQMC_supermesh_code, cppargs=["-Og", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
QMCWhiteNoiseConstructor   = compiled_QMC_code.QMCWhiteNoiseConstructor
MLQMCWhiteNoiseConstructor = compiled_MLQMC_code.MLQMCWhiteNoiseConstructor
        
class QMCWhiteNoiseField(StochasticField):
    def setup(self):

        self.us = [TrialFunction(V) for V in self.function_spaces]
        self.vs = [TestFunction(V)  for V in self.function_spaces]

        self.spaces_dimensions = [V.element().space_dimension() for V in self.function_spaces]

        self.couple_levels = self.params.get("couple_levels", True)

        if self.refinement_type in ["p", "hp"]:
            if self.mpiRank == 0:
                raise NotImplementedError("MLQMC and QMC not implemented with p-refinement.")

        gdim = self.function_spaces[0].mesh().geometry().dim()
        tdim = self.function_spaces[0].mesh().topology().dim()
        vol = np.sum([c.volume() for c in cells(self.function_spaces[0].mesh())])

        # the following is the Haar level with the same Haar mesh size as the finite element mesh size
        max_Lh_needed = np.ceil(np.log2(vol**(1/tdim)/(np.array(self.mesh_sizes)))).astype(np.int)

        wanted_Lh = self.params.get("Lh", None)
        if wanted_Lh is None:
            Lh = max_Lh_needed.copy()
        else:
            Lh = np.maximum(wanted_Lh, 0)

        # NOTE: we always add the remainder term to white noise.
        # If the l-th entry in self.correction_needed is set to False,
        # then the remainder will not be added on level l.
        #self.correction_needed = (max_Lh_needed > Lh)
        self.correction_needed = np.ones_like(max_Lh_needed).astype(np.bool)

        # computing the QMC integrand dimensions
        if gdim == 1:
            self.qmc_dims = 2**Lh
        if gdim == 2:
            self.qmc_dims = (2.**(Lh-1)*(Lh+2)).astype(np.int)
        if gdim == 3:
            self.qmc_dims = (2.**(Lh-3)*(Lh**2 + 7*Lh + 8)).astype(np.int)

        self.n_haar_mesh_cells = 2**(Lh*tdim)
        self.haar_cell_volumes = vol/self.n_haar_mesh_cells

        assert (self.n_haar_mesh_cells - self.qmc_dims).min() >= 0

        if self._independent == False:
            try: 
                QMC_dimensions = getattr(modules['femlmc'], '__QMC_DIMENSIONS__')
            except AttributeError:
                QMC_dimensions = []

            self.input_number = len(QMC_dimensions)
            QMC_dimensions.append([len(QMC_dimensions), self.qmc_dims])
            setattr(modules["femlmc"], '__QMC_DIMENSIONS__', QMC_dimensions)
        else:
            self.input_number = None

        self.GQRNG = None

        self.factors = []
        for l in range(self.n_levels):
            c = Cell(self.function_spaces[l].mesh(),0)
            ref_H = np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume())
            ref_c = assemble_local(self.vs[l]*dx, c)/c.volume()
            self.factors.append((ref_H, ref_c))

        self.interp_matrices   = []
        intersection_maps = []
        self.supermesh_element_volumes = []
        self.sorting_indices = []
        self.haar_white_noises = []

        ref_el = np.vstack(re.ufc_simplex(tdim).get_vertices()).flatten()
        finite_elements = [ft.create_element(V.ufl_element()) for V in self.function_spaces]
        reference_nodes = [np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten() for fe in finite_elements]

        # Supermesh constructions. We are using the affine strategy from the paper.
        from time import time
        for l in range(self.n_levels):
            a = time()
            # if l == 0, only two-way supermesh needed.
            if l == 0 or self.correction_needed[l] == False or self.couple_levels == False:
                constructor = QMCWhiteNoiseConstructor(self.function_spaces[l], Lh[l], ref_el, reference_nodes[l])
                self.interp_matrices.append([constructor.get_full_interp(), None]) # interpolation matrices from the supermesh cells to the parent meshes' cells
                intersection_maps.append([constructor.get_intersection_map(), None]) # intersecting pairs of parent mesh cells
            # otherwise, three-way supermesh.
            else:
                constructor = MLQMCWhiteNoiseConstructor(self.function_spaces[l], self.function_spaces[l-1], Lh[l], ref_el, reference_nodes[l])
                self.interp_matrices.append([constructor.get_full_interp(i) for i in range(2)])  # interpolation matrices from the supermesh cells to the parent meshes' cells
                intersection_maps.append([constructor.get_intersection_map(i) for i in range(2)]) # intersecting pairs of parent mesh cells

            self.supermesh_element_volumes.append(constructor.get_supermesh_element_volumes()) # supermesh cell volumes
            self.sorting_indices.append(np.argsort(self.supermesh_element_volumes[-1])) # sorting the supermesh cell volumes (needed later)

            # creating Haar white noise objects to help with the sampling
            if l == 0 or self.couple_levels == False:
                self.haar_white_noises.append(HaarWhiteNoise(constructor, ref_c, ref_H))
            else:
                self.haar_white_noises.append(CoupledHaarWhiteNoise(constructor, old_constructor, ref_c, ref_H))

            old_constructor = constructor

            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        del constructor
        del old_constructor

        self.n_cells = [len(item) for item in self.supermesh_element_volumes]

        if self.mpiRank == 0:
            print("Cholesky factors computed!")
            print("Generating assembly matrices...")

        # building the sparse boolean supermesh assembly matrices
        self.supermesh_assembly_matrices = []
        dofmaps = [self.function_spaces[l].dofmap() for l in range(self.n_levels)]
        for l in range(self.n_levels):
            a = time()
            if l == 0 or self.couple_levels == False:
                n = self.spaces_dimensions[l]*self.n_cells[l]
                elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                coarse_mat = None

            else:
                if self.correction_needed[l] == True:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l]

                    elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                    elemsB = intersection_maps[l][1][self.sorting_indices[l]]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(elem) for elem in elemsB])

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

                else:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l-1]

                    elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                    elemsB = intersection_maps[l-1][0][self.sorting_indices[l-1]]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(elem) for elem in elemsB])

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

            #self.intersection_maps = intersection_maps
            self.supermesh_assembly_matrices.append((fine_mat,coarse_mat))
            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        self.worldComm.barrier()

        if self.mpiRank == 0:
            print("Assembly matrices computed!")
            print("QMC levels: %s, Max levels needed %s" % (Lh,max_Lh_needed))


    def sample(self, l):

        # NOTE works only in serial (or with mpi_comm_self())
        self._initialise_GQRNG()
        if self.GQRNG is None:
            from .quasi_random_number_generator import GlobalQRNG
            GQRNG = GlobalQRNG(worldcomm = self.worldComm)
            GQRNG._setUp()
            setattr(modules["uqdolfin"], '__GLOBAL_QRNG__', GQRNG)
            self._initialise_GQRNG()


        xrnge = 2 - (l is 0)

        Fs = [Function(V) for V in self.function_levels[l]]
        Fs_arr = [0 for i in range(xrnge)]

        # independent realisations case
        if l == 0 or self.couple_levels == False:

            m = self.spaces_dimensions[l]

            # first sampling a point from the low-discrepancy sequence.
            # we pad it with pseudo-random numbers in case there are more Haar cells than QMC dims.
            # also the sampling is different according to whether the field was created with self.get_independent_field() or not.
            if self._independent == False:
                r_qmc = self.GQRNG.sample(l, self.input_number)
            else:
                r_qmc = self.GQRNG.sample(self._old_level, self.input_number)

            if self.n_haar_mesh_cells[l] > self.qmc_dims[l]:
                r_qmc = np.concatenate([r_qmc, self.RNG.randn(self.n_haar_mesh_cells[l] - self.qmc_dims[l])])

            #assert all(np.isfinite(r_qmc.flatten()))

            # feed the sampled numbers into the Haar white noises to obtain the white noise Haar wavelet series values
            r_qmc = self.haar_white_noises[l].sample(r_qmc)

            if self.correction_needed[l] == True:
                r_mc = self.RNG.randn(m, self.n_cells[l])
                r_mc = self.haar_white_noises[l].sample_correction(r_mc)

                r = r_qmc + r_mc
            else:
                r = r_qmc

            # applying all the supermesh interpolation matrices in block using einsum
            z1  = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],m,m)), r)
            #assert all(np.isfinite(z1.flatten()))

            Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1[:,self.sorting_indices[l]].T.flatten())

        # coupled realisations case
        else:
            mmax = self.spaces_dimensions[l]
            mmin = self.spaces_dimensions[l-1]

            # first sampling a point from the low-discrepancy sequence.
            # we pad it with pseudo-random numbers in case there are more Haar cells than QMC dims.
            # also the sampling is different according to whether the field was created with self.get_independent_field() or not.
            if self._independent == False:
                r_qmc = self.GQRNG.sample(l, self.input_number)
            else:
                r_qmc = self.GQRNG.sample(self._old_level, self.input_number)

            if self.n_haar_mesh_cells[l] > self.qmc_dims[l]:
                r_qmc = np.concatenate([r_qmc, self.RNG.randn(self.n_haar_mesh_cells[l] - self.qmc_dims[l])])

            #try: assert all(np.isfinite(r_qmc.flatten()))
            #except AssertionError:
            #    print((len(r_qmc.flatten()), sum(np.isfinite(r_qmc.flatten()) == False)))

            # delta_W not needed (see below)
            WLf, WLc = self.haar_white_noises[l].sample(r_qmc)

            #assert all(np.isfinite(WLf.flatten()))
            #assert all(np.isfinite(WLc.flatten()))

            if self.correction_needed[l] == True:
                r_mc = self.RNG.randn(mmax, self.n_cells[l])
                WRf = self.haar_white_noises[l].sample_correction(r_mc)
                #assert all(np.isfinite(WRf.flatten()))

                Wf = WLf + WRf

                if self.correction_needed[l-1] == True:
                    # NOTE: if correction is needed we are using a 3-way supermesh
                    #       and Wc as written in the line below is the same as Wf
                    #Wc = WLc + delta_W + WRf # (since WLc + delta_W = WLf and delta_W + WRf = WRc)
                    Wc = Wf.copy()
                else:
                    Wc = WLc

                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                Wf = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), Wf)
                Wc = np.einsum('ikj,ki->ji', self.interp_matrices[l][1].reshape((self.n_cells[l],mmin,mmax)), Wc)

                #assert all(np.isfinite(Wf.flatten()))
                #assert all(np.isfinite(Wc.flatten()))

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(Wf[:,self.sorting_indices[l]].T.flatten())
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(Wc[:,self.sorting_indices[l]].T.flatten())
            
            # no correction needed: just sampling the truncated white noise Haar expansion
            else:

                Wf = WLf
                Wc = WLc

                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                Wf = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), Wf)
                Wc = np.einsum('ikj,ki->ji', self.interp_matrices[l-1][0].reshape((self.n_cells[l-1],mmin,mmin)), Wc)

                #assert all(np.isfinite(Wf.flatten()))
                #assert all(np.isfinite(Wc.flatten()))

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(Wf[:,self.sorting_indices[l]].T.flatten())
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(Wc[:,self.sorting_indices[l-1]].T.flatten())

        Fs[0].vector()[:] = Fs_arr[0]
        if l > 0 and self.couple_levels == True:
            Fs[1].vector()[:] = Fs_arr[1]

        return Fs
