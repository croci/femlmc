#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs, CoupledHaarWhiteNoise, MLQMC_supermesh_code, QMC_supermesh_code
import numpy as np
import os

include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
libraries = "supermesh"

compiled_MLQMC_code = compile_cpp_code(MLQMC_supermesh_code, cppargs=["-Og", "-g", "-Werror", "-Wall", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])
#compiled_MLQMC_code = compile_cpp_code(MLQMC_supermesh_code, cppargs=["-O3", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

compiled_QMC_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

hex_cell_volume = compiled_MLQMC_code.hex_cell_volume
compute_intersection_candidates = compiled_MLQMC_code.compute_intersection_candidates
intersect_vertical_segment = compiled_MLQMC_code.intersect_vertical_segment
MLQMCWhiteNoiseConstructor = compiled_MLQMC_code.MLQMCWhiteNoiseConstructor

QMCWhiteNoiseConstructor = compiled_QMC_code.QMCWhiteNoiseConstructor

from scipy.linalg import solve_triangular
import mshr
dim = 2
if dim == 1:
    mesh  = IntervalMesh(503, 0, 1)
    mesh2 = IntervalMesh(103, 0, 1)
elif dim == 2:
    mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 20)
    mesh2 = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 10)
    #mesh = BoxMesh.create([Point(0,0,0), Point(1,1,1)], [1,1,1], CellType.Type.tetrahedron)
else:
    mesh = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 20)
    mesh2 = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 10)

print("Mesh sizes: (%d, %d)" % (mesh.num_cells(), mesh2.num_cells()))

vol = assemble(Constant(1.0)*dx(domain=mesh))

V = FunctionSpace(mesh, "CG", 1)
V2 = FunctionSpace(mesh2, "CG", 1)

eldim = V.element().space_dimension()

from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

ref_el = np.vstack(re.ufc_simplex(mesh.topology().dim()).get_vertices()).flatten()
fe = ft.create_element(V.ufl_element())
reference_nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten()

c = Cell(mesh,0)
u = TrialFunction(V); v = TestFunction(V)
M_loc = assemble_local(u*v*dx,c)/c.volume()
H_loc = np.linalg.cholesky(M_loc)
c_loc = assemble_local(v*dx,  c)/c.volume()

# [7, 5, 4] are the max Lh in 1, 2 and 3 dimensions
Lh = 3
constructor = MLQMCWhiteNoiseConstructor(V, V2, Lh, ref_el, reference_nodes)
constructor2 = QMCWhiteNoiseConstructor(V2, Lh-1, ref_el, reference_nodes)
haar_noise = CoupledHaarWhiteNoise(constructor, constructor2, c_loc, H_loc)

n_haar_mesh_cells = constructor.get_n_haar_cells()

full_interps = [constructor.get_full_interp(i) for i in range(2)]

volumes = constructor.get_supermesh_element_volumes()

int_map = [constructor.get_intersection_map(i) for i in range(2)]

cells_in_haar_cell = constructor.get_cells_in_haar_cell()

#candidates = [constructor.get_n_intersection_candidates(i) for i in range(2)]

#act_int = [constructor.get_n_actual_intersections(i) for i in range(2)]

#duration = constructor.get_duration()

midpoints = constructor.get_haar_midpoints()

haar_cell_volume = constructor.get_haar_cell_volume()

n_supermesh_cells = volumes.shape[0]

print("N supermesh cells / N finest mesh cells: %f" % (n_supermesh_cells/mesh.num_cells()))
#assert (candidates[0].astype(np.int)-act_int[0].astype(np.int)).min() >= 0
#assert (candidates[1].astype(np.int)-act_int[1].astype(np.int)).min() >= 0
assert abs(full_interps[0].sum() - n_supermesh_cells*len(c_loc)) < 1.0e-10
assert abs(full_interps[1].sum() - n_supermesh_cells*len(c_loc)) < 1.0e-10
print(1 - (volumes**2).sum()/vol)
assert abs((volumes**2).sum()/vol - 1) < 3*DOLFIN_EPS

#print([np.sum(candidates[i]-act_int[i]) for i in range(2)])

#print("\n---------------------------------- Time (s)")
#print("2-way intersection candidates:     %f" % duration[0])
#print("2-way intersection computation:    %f" % duration[1])
#print("Haar intersection candidates:      %f" % duration[2])
#print("3-way intersection computation:    %f" % duration[3])
#print("Interpolation factors computation: %f" % duration[4])
#print("Total supermeshing time:           %f" % duration[5])
#print("-------------------------------------------\n")

#print(duration)

# FIXME: we tested the coupled_haar_noise with 2-way supermeshes. It now needs to be tested with 3-way supermeshes and we need to implement the correction bits (I think)
rng = np.random.RandomState(123456789)
z = rng.randn(n_haar_mesh_cells)
WLf, WLc, delta_W = haar_noise.sample(z, return_delta=True)
print(abs(np.unique((WLf-delta_W).round(decimals=8)) - np.unique(WLc.round(decimals=8))).max())

z = rng.randn(eldim, n_supermesh_cells)
WRf = haar_noise.sample_correction(z)
WRc = delta_W + WRf

outf = WLf + WRf
outc = WLc + WRc

print((np.sum(outf), np.sum(outc)))

print("TEST PASSED!")
