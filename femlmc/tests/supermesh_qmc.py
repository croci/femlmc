#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs, HaarWhiteNoise, QMC_supermesh_code
import numpy as np
import os

include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
libraries = "supermesh"

compiled_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-Og", "-g", "-Wall", "-Werror", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])
#compiled_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-O3", "-ffast-math", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

hex_cell_volume = compiled_code.hex_cell_volume
QMCWhiteNoiseConstructor = compiled_code.QMCWhiteNoiseConstructor

from scipy.linalg import solve_triangular
import mshr
dim = 2
if dim == 1:
    mesh = IntervalMesh(103, 0, 1)
elif dim == 2:
    mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 30)
    #mesh = BoxMesh.create([Point(0,0,0), Point(1,1,1)], [1,1,1], CellType.Type.tetrahedron)
else:
    mesh = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 20)

print("Mesh size: %d" % mesh.num_cells())

vol = assemble(Constant(1.0)*dx(domain=mesh))

V = FunctionSpace(mesh, "CG", 1)

eldim = V.element().space_dimension()

from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

ref_el = np.vstack(re.ufc_simplex(mesh.topology().dim()).get_vertices()).flatten()
fe = ft.create_element(V.ufl_element())
reference_nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten()

c = Cell(mesh,0)
u = TrialFunction(V); v = TestFunction(V)
M_loc = assemble_local(u*v*dx,c)/c.volume()
H_loc = np.linalg.cholesky(M_loc)
c_loc = assemble_local(v*dx,  c)/c.volume()

# [7, 5, 4] are the max Lh in 1, 2 and 3 dimensions
Lh = 3
constructor = QMCWhiteNoiseConstructor(V, Lh, ref_el, reference_nodes)
haar_noise = HaarWhiteNoise(constructor, c_loc, H_loc)

n_haar_mesh_cells = constructor.get_n_haar_cells()

full_interp = constructor.get_full_interp()

volumes = constructor.get_supermesh_element_volumes()

int_map = constructor.get_intersection_map()

#candidates = constructor.get_n_intersection_candidates()

#act_int = constructor.get_n_actual_intersections()

#duration = constructor.get_duration()

midpoints = constructor.get_haar_midpoints()

haar_cell_volume = constructor.get_haar_cell_volume()

n_supermesh_cells = volumes.shape[0]

print("N supermesh cells / N finest mesh cells: %f" % (n_supermesh_cells/mesh.num_cells()))
#print(duration)
#assert (candidates.astype(np.int)-act_int.astype(np.int)).min() >= 0
assert (int(full_interp.sum()) - n_supermesh_cells*len(c_loc)) == 0
print(1 - (volumes**2).sum()/vol)
assert abs((volumes**2).sum()/vol - 1) < 1.0e-14

rng = np.random.RandomState(123456789)

#print("\n---------------------------------- Time (s)")
#print("Haar intersection candidates:      %f" % duration[0])
#print("Haar intersection computation:     %f" % duration[1])
#print("Interpolation factors computation: %f" % duration[2])
#print("Total supermeshing time:           %f" % duration[3])
#print("-------------------------------------------\n")

#print(duration)

# FIXME: we tested the coupled_haar_noise with 2-way supermeshes. It now needs to be tested with 3-way supermeshes and we need to implement the correction bits (I think)
rng = np.random.RandomState(123456789)
z = rng.randn(n_haar_mesh_cells)
out1 = haar_noise.sample(z)
z = rng.randn(eldim, n_supermesh_cells)
out2 = constructor.sample_white_noise_correction(H_loc, c_loc, z)
out3 = haar_noise.sample_correction(z)
print(abs(out2-out3).max())

out  = out1 + out2

print(np.sum(out))

print("TEST PASSED!")
