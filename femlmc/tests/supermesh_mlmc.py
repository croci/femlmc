#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs, supermesh_code
import os

include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
libraries = "supermesh"

compiled_code = compile_cpp_code(supermesh_code, cppargs=["-O0", "-g", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

SuperMeshConstructor = compiled_code.SuperMeshConstructor

import numpy as np
import mshr
#mesh = IntervalMesh(103, 0, 1)
#mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 50)
mesh = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 10)
mesh2 = BoxMesh.create([Point(0,0,0), Point(1,1,1)], [2,2,2], CellType.Type.tetrahedron)
V = FunctionSpace(mesh, "CG", 2)
U = FunctionSpace(mesh2, "CG", 1)

eldim1 = V.element().space_dimension()

from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

ref_el = np.vstack(re.ufc_simplex(mesh.topology().dim()).get_vertices()).flatten()
fe = ft.create_element(V.ufl_element())
reference_nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten()

constructor = SuperMeshConstructor(V, U, ref_el, reference_nodes)

full_interp1 = constructor.get_full_interp(0)
full_interp2 = constructor.get_full_interp(1)

volumes = constructor.get_supermesh_element_volumes()

n_supermesh_cells = len(volumes)

int_map = constructor.get_intersection_map()

#candidates = constructor.get_n_intersection_candidates()

#act_int = constructor.get_n_actual_intersections()

#duration = constructor.get_duration()

print(volumes.shape[0]/mesh.num_cells())
#assert (candidates.astype(np.int)-act_int.astype(np.int)).min() >= 0
assert ((volumes**2).sum() - 1) < 3*DOLFIN_EPS
assert (int(full_interp1.sum()) - n_supermesh_cells*eldim1) == 0

print("TEST PASSED!")
