#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from petsc4py import PETSc
import numpy as np
from scipy.special import gammaln
from scipy.spatial import cKDTree
from .stochastic_field import StochasticField, to_levels
from .white_noise_field import WhiteNoiseField
from .QMC_white_noise_field import QMCWhiteNoiseField
from sys import modules

create_transfer_matrix = PETScDMCollection.create_transfer_matrix

def gammaratio(x,y): # computes the ratio between \Gamma(x) and \Gamma(y)
    return np.exp(gammaln(x) - gammaln(y))

def make_nested_mapping(outer_space, inner_space):
    # Maps the dofs between nested meshes
    outer_dof_coor = outer_space.tabulate_dof_coordinates()
    inner_dof_coor = inner_space.tabulate_dof_coordinates()

    tree = cKDTree(outer_dof_coor)
    _,mapping = tree.query(inner_dof_coor, k=1)
    return mapping


class MaternField(StochasticField):
    def setup(self):
        self.current_level = None
        self.ksp = None

        # Set up and check function spaces
        self.outer_spaces = self.params.get("outer_spaces", None)
        if not(isinstance(self.outer_spaces, (FunctionSpace, list))):
            raise ValueError("A list outer_spaces of the same length of the input function_spaces must be provided to a MaternField") 
        if isinstance(self.outer_spaces, FunctionSpace):
            self.outer_spaces = [self.outer_spaces]
        if len(self.outer_spaces) != len(self.function_spaces):
            raise ValueError("A list outer_spaces of the same length of the input function_spaces must be provided to a MaternField") 

        for i in range(self.n_levels):
            try:
                assert self.function_spaces[i].ufl_element().family() == self.outer_spaces[i].ufl_element().family()
                assert self.function_spaces[i].ufl_element().family() == 'Lagrange'
                assert self.function_spaces[i].ufl_element().degree() == self.outer_spaces[i].ufl_element().degree()
            except AssertionError:
                raise ValueError("function_spaces and outer_spaces must be of Continuous Lagrange type and have the same polynomial degree")

        self.outer_levels = to_levels(self.outer_spaces)

        # define trial and test functions for the outer spaces
        self.u_levels = to_levels([TrialFunction(V) for V in self.outer_spaces])
        self.v_levels = to_levels([TestFunction(V)  for V in self.outer_spaces])

        # Setup Matern field parameters
        self.dim = self.outer_spaces[0].mesh().geometry().dim()
        parameters = self.params.get("parameters", {})
        # default parameters
        self.params["lmbda"] = parameters.get("lmbda", 0.3) # correlation length
        self.params["avg"] = parameters.get("avg", 1.0) # mean
        self.params["sigma"] = parameters.get("sigma", 0.25) # std. deviation
        self.params["nu"] = parameters.get("nu", 2. - self.dim/2.) # smoothness parameter
        self._nu = self.params["nu"]
        self.n_solves = self._nu/2. + self.dim/4.
        # check if we need to solve a fractional diffusion problem
        if abs(self.n_solves - round(self.n_solves)) < 1.0e-2:
            self.n_solves = int(round(self.n_solves))
            self._fractional = False
        else:
            self._fractional = True
            if self.mpiRank == 0:
                print("WARNING! You are trying to solve a fractional diffusion problem. Solver not tested thoroughly")
        # scale mean and std. dev if the field is lognormal
        self.params["lognormal_scaling"] = parameters.get("lognormal_scaling", False)
        self._update_parameters()

        # Check if the hierarchy is nested and act accordingly
        self.nested_hierarchy = self.params.get("nested_hierarchy", False)
        self.hierarchy_mappings = None
        if self.nested_hierarchy == True:
            self.hierarchy_mappings = [None] + [make_nested_mapping(Vfine, Vcoarse) for Vfine, Vcoarse in self.function_levels[1:]]

        # Check if inner subspace is nested within outer subspace and act accordingly
        self.outer_inner_mappings   = None
        self.transfer_matrices = None
        if self.params.get("nested_inner_outer", False) == True:
            self.outer_inner_mappings = [make_nested_mapping(outer_V, V) for outer_V, V in zip(self.outer_spaces, self.function_spaces)]
        else:
            self.transfer_matrices = [create_transfer_matrix(self.outer_spaces[l], self.function_spaces[l]).mat() for l in range(self.n_levels)]

        if self.stochastic_field is None or not isinstance(self.stochastic_field, (WhiteNoiseField, QMCWhiteNoiseField)):
            if not self.qmc_flag:
                self.stochastic_field =    WhiteNoiseField(self.outer_spaces, qmc = self.qmc_flag, independent = self._independent, old_level = self._old_level, filepath = self.params.get("filepath", None), couple_levels = not self.nested_hierarchy)
            else:
                self.stochastic_field = QMCWhiteNoiseField(self.outer_spaces, qmc = self.qmc_flag, independent = self._independent, old_level = self._old_level, filepath = self.params.get("filepath", None), Lh = self.params.get("Lh", None), n_qmc_shifts = self.params.get("n_qmc_shifts", 32), couple_levels = not self.nested_hierarchy)

        self.solver_parameters = self.params.get("solver_parameters", None)
        self.load_petsc_options()

    def _update_parameters(self):

        if self._fractional == True:
            self._n_integer_solves = int(np.floor(self.n_solves))
            self._beta = self.n_solves - self._n_integer_solves
        else:
            self._beta = 0
            self._n_integer_solves = self.n_solves

        # constant in the self-adjoint elliptic equation
        self._const = np.sqrt(8.*self._nu)/self.params['lmbda']
        # this correction gives an output of unit variance
        if self.dim != 2:
            #sigma_correction = np.sqrt((self._const**self.dim)*gammaratio(self._nu, self._nu + self.dim/2)/(4.*DOLFIN_PI)**(self.dim/2.))
            # The factor dependent on nu convergese to 1 as nu goes to infinity
            sigma_correction = np.sqrt(gammaratio(self._nu, self._nu + self.dim/2.)*self._nu**(self.dim/2.))*(2./DOLFIN_PI)**(self.dim/4.)*self.params['lmbda']**(-self.dim/2.)
        else:
            # in 2D the correction is independent from nu
            sigma_correction = np.sqrt(2./DOLFIN_PI)/self.params['lmbda']

        avg   = self.params['avg']
        sigma = self.params['sigma']

        # scale std. dev and mean if lognormal scaling used
        if self.params["lognormal_scaling"] == True:
            sigma_factor = np.sqrt(np.log(1. + (sigma/avg)**2.))
            self._mu = np.log(avg**2./np.sqrt(sigma**2. + avg**2.0))
        else:
            sigma_factor = sigma
            self._mu     = avg

        self._scalings = sigma_factor/sigma_correction

    def copy(self):
        from copy import copy

        field = copy(self)

        if self.qmc_flag:
            try: 
                QMC_dimensions = getattr(modules['femlmc'], '__QMC_DIMENSIONS__')
            except AttributeError:
                QMC_dimensions = []

            field.stochastic_field.input_number = len(QMC_dimensions)
            QMC_dimensions.append([len(QMC_dimensions), field.stochastic_field.qmc_dims])
            setattr(modules["femlmc"], '__QMC_DIMENSIONS__', QMC_dimensions)

        return field

    def solver_options(self):
        prefix = "matern_"
        opts   = {"ksp_type" : "cg",
                  "ksp_atol" : 1.0e-10,
                  "ksp_rtol" : 1.0e-12,
                  "ksp_norm_type" : "unpreconditioned",
                  "ksp_diagonal_scale" : True,
                  "ksp_diagonal_scale_fix" : True,
                  "ksp_reuse_preconditioner" : True,
                  "ksp_max_it" : 1000,
                  "pc_factor_mat_solver_type" : "mumps",

                  #"ksp_converged_reason" : None,
                  #"ksp_monitor_true_residual" : None,
                  #"ksp_view" : None,

                  "pc_type" : "hypre",
                  "pc_hypre_boomeramg_strong_threshold" : [0.25, 0.25, 0.6][self.dim-1],
                  "pc_hypre_type" : "boomeramg",

                  }

        return (prefix, opts)

    def load_petsc_options(self):
        prefix, opts = self.solver_options()

        prefix    = self.params.get("prefix", prefix)
        user_opts = self.params.get("solver_parameters", {})

        petsc_opts = PETSc.Options() 
        #petsc_opts["help"] = None

        for key in opts:
            petsc_opts[prefix + key] = opts[key]
        for key in user_opts:
            petsc_opts[prefix + key] = user_opts[key]

        self.prefix = prefix

    def setup_level(self, l):
        # prepare linear system and solver to sample the Matern field via the SPDE approach by Lindgren
        if self.current_level == l:
            pass
        else:
            xrnge = 2 - (l is 0 or self.nested_hierarchy)

            bcs = [DirichletBC(V, Constant(0.0), "on_boundary") for V in self.outer_levels[l][:xrnge]]
            L = [Constant(self._const**(-2))*inner(grad(u), grad(v))*dx + u*v*dx + Constant(0.0)*v*dx for u,v in zip(self.u_levels[l][:xrnge], self.v_levels[l][:xrnge])]
            L = [as_backend_type(assemble_system(*system(L[i]), bcs=bcs[i])[0]).mat() for i in range(xrnge)]
            self.ksp = [PETSc.KSP().create(self.outer_spaces[l].mesh().mpi_comm()) for i in range(xrnge)]
            [self.ksp[i].setOperators(L[i]) for i in range(xrnge)]
            [self.ksp[i].setOptionsPrefix(self.prefix) for i in range(xrnge)]
            [self.ksp[i].setFromOptions() for i in range(xrnge)]

            if self._fractional == True:
                self._mass  = [as_backend_type(assemble(u*v*dx)).mat() for u,v in zip(self.u_levels[l][:xrnge], self.v_levels[l][:xrnge])]

                self.fsp = [PETSc.KSP().create(self.outer_spaces[l].mesh().mpi_comm()) for i in range(xrnge)]
                [self.fsp[i].setOperators(L[i]) for i in range(xrnge)]
                [self.fsp[i].setOptionsPrefix(self.prefix) for i in range(xrnge)]
                [self.fsp[i].setFromOptions() for i in range(xrnge)]

            self.current_level = l

    def solve_level(self, l, b, out):
        # Solves the SPDE and computes two coupled Matern field samples (ML(Q)MC) or a single one (standard (Q)MC)
        # Given two coupled white noise vectors (or a single one), which are stored in b
        self.setup_level(l)

        xrnge = 2 - (l is 0 or self.nested_hierarchy)

        x = [item.duplicate() for item in b]
        for k in range(self._n_integer_solves):
            [self.ksp[i].solve(b[i], x[i]) for i in range(xrnge)]
            for i in range(xrnge):
                reason = self.ksp[i].getConvergedReason()
                if reason < 0:
                    resnorm = self.ksp[i].getResidualNorm()
                    raise RuntimeError("ERROR in the Matern field sampler! Linear KSP-%d solver did not converge with reason: %d. Residual norm: %f" % (i, reason, resnorm))

            if k < self._n_integer_solves - 1 or self._fractional == True:
                for i in range(xrnge):
                    out[i].vector()[:] = x[i].getArray()
                b = [as_backend_type(assemble(r*v*dx)).vec() for r,v in zip(out[:xrnge], self.v_levels[l][:xrnge])]


        if self._fractional == False:

            for i in range(xrnge):
                out[i].vector()[:] = self._scalings*x[i].getArray() + self._mu

            return out

        else:
            # now "just" need to apply a fractional operator with power self._beta. Using method by Bolin, Kirchner and Kovacs

            #FIXME: the following only works asymptotically: it is unclear how the domain size affects
            #       the convergence of the quadrature error for large mesh sizes. However, large domain size => large mesh size
            # dk is k in the paper by Bolin et al. Maybe one could make dk larger and hopefully the overall error will not be affected
            # Bolin and Kirchner have also used a rational approximation for which the number of solves is half of this one
            dk  = [-DOLFIN_PI**2/(8.*self._beta*np.log(min(V.mesh().hmax(),0.5))) for V in self.outer_levels[l][:xrnge]]
            km  = [int(np.ceil(DOLFIN_PI**2/(4.*self._beta*h**2))) for h in dk]
            kp  = [int(np.ceil(DOLFIN_PI**2/(4.*(1.-self._beta)*h**2))) for h in dk]

            nquadpts = kp[0] + km[0] + 1

            if self.mpiRank == 0 and nquadpts > 50:
                print("WARNING: inverting the fractional elliptic operator will require %d linear solves per sample" % nquadpts)

            ell = [np.arange(-ka,kb+1) for ka,kb in zip(km,kp)]

            for i in range(xrnge):
                L = self.ksp[i].getOperators()[0]
                out[i].vector()[:] = 0.
                for el in ell[i]:
                    U = L + np.exp(-2*dk[i]*el)*self._mass[i]
                    self.fsp[i].setOperators(U)
                    self.fsp[i].solve(b[i], x[i])

                    reason = self.fsp[i].getConvergedReason()
                    if reason < 0:
                        resnorm = self.fsp[i].getResidualNorm()
                        raise RuntimeError("ERROR in the fractional Matern field sampler! Linear KSP-%d solver did not converge with reason: %d. Residual norm: %f" % (i, reason, resnorm))

                    out[i].vector()[:] += np.exp(-2.*(1. - self._beta)*dk[i]*el)*x[i].getArray()

                out[i].vector()[:] *= 2.*dk[i]*np.sin(DOLFIN_PI*self._beta)/DOLFIN_PI 
                out[i].vector()[:]  = self._scalings*out[i].vector()[:] + self._mu

            return out

    def sample(self, l, b = None):
        # samples the required Matern fields by calling the solver and then transferring the sampled fields onto the domain of interest.
        xrnge = 2 - (l is 0 or self.nested_hierarchy)

        if b is None:
            b = self.stochastic_field.sample(l)
        b = [as_backend_type(item.vector()).vec() for item in b[:xrnge]]
        out = [Function(V) for V in self.outer_levels[l][:xrnge]]

        out = self.solve_level(l, b, out)

        uu = [Function(U) for U in self.function_levels[l]]

        if self.outer_inner_mappings is not None:
            for i in range(xrnge):
                uu[i].vector()[:] = out[i].vector()[self.outer_inner_mappings[l-i]]

        else:
            uuu = [as_backend_type(uu[i].vector()).vec() for i in range(xrnge)]

            # FIXME AS IT IS, THE INTERPOLATION OPERATOR ONLY WORKS FOR P-REFINEMENT IF THE MESHES ARE NESTED
            [self.transfer_matrices[l-i].mult(as_backend_type(out[i].vector()).vec(), uuu[i]) for i in range(xrnge)]
            for i in range(xrnge):
                uu[i].vector()[:] = uuu[i].getArray()

            ##NOTE the following works, but I do not like the fact that extrapolation is required
            #[out[i].set_allow_extrapolation(True) for i in range(xrnge)]
            #uu = [interpolate(out[i],uu[i].function_space()) for i in range(xrnge)]

        if self.nested_hierarchy and l > 0:
            uu[1].vector()[:] = uu[0].vector()[self.hierarchy_mappings[l]]

        return uu


