#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import numpy
from dolfin import MPI
from numpy import sqrt
import sys

comm = MPI.comm_world
mpiRank = MPI.rank(comm)

def qmc(mlmc_sampler, N, eps = None, logfile = None):
    """
    Quasi Monte Carlo test routine.

    mlmc_sampler: an mlmc_sampler set up for qmc with M shifts
      qmc_sums = mlmc_sampler.sample(N)
      sums = mlmc_sampler.mlqmc_finalise_sums(sums, N)
    N: number of paths
    eps: tolerance. If eps is unset, then only take N samples.
         if eps is set, then qmc takes N initial samples and doubles them until
         QMC convergence is achieved.

    sums is a list of numpy arrays of outputs
      sums[i][0] = sum(P)
      sums[i][1] = sum(P**2)
      sums[i][2] = sum(P**3)
      sums[i][3] = sum(P**4)

    for i=1,..., mlmc_sampler.n_outputs
    where P_j = 1/N \sum_i p(omega_i^j), j=1,...,M
    is the QMC estimator for a fixed shift j

    """

    M = mlmc_sampler.M
    n_outputs = mlmc_sampler.n_outputs
    howmanyshifts = mlmc_sampler.howmanyshifts
    n_values = 2
    p = int(numpy.ceil(numpy.log2(N)))
    if abs(numpy.log2(N)-int(numpy.log2(N))) > 1.0e-12:
        N = 2**p
        if mpiRank == 0:
            print("Warning! For QMC runs the number of samples must be a power of 2. Setting N to the next power of two: N = %d" % N)

    write(logfile, "\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "***     Convergence, Expected Value, Variance, Cost    ***\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "\n  ave(P)      var(P)      kurt(P)     cost(P)     Nl    \n")
    write(logfile, "----------------------------------------------------------\n")

    avg = []
    var = []
    kurt = []
    cost = []

    sums_list, costl = mlmc_sampler.sample(2**p)
    sums = mlmc_sampler.mlqmc_finalise_sums(sums_list, 2**p)
    for k in range(n_outputs):
        for i in range(len(sums[k])):
            sums[k][i] = sums[k][i]/M

    # NOTE: only the first output functional variance is considered for QMC convergence
    Vl = M/max(M-1,1)*max(0.,sums[0][1] - sums[0][0]**2)/M

    if eps is not None:
        while Vl > eps**2:
            out, time_cost = mlmc_sampler.sample(2**p)
            p += 1
            for k in range(n_outputs):
                for i in range(n_values):
                    for shift_number in range(howmanyshifts):
                        sums_list[shift_number][k][i] += out[shift_number][k][i]

            sums = mlmc_sampler.mlqmc_finalise_sums(sums_list, 2**p)
            for k in range(n_outputs):
                for i in range(len(sums[k])):
                    sums[k][i] = sums[k][i]/M

            # NOTE: only the first output functional variance is considered for QMC convergence
            Vl = M/max(M-1,1)*max(0.,sums[0][1] - sums[0][0]**2)/M

    for k in range(n_outputs):
        cost.append(costl)
        avg.append(sums[k][0])

        if isinstance(avg[-1], float):
            var.append(max(M/max(M-1,1)*abs(sums[k][1]-sums[k][0]**2), 1.0e-16)) 
            kurt.append((sums[k][3] - 4*sums[k][0]*sums[k][2] + 6*sums[k][0]**2*sums[k][1] - 3*sums[k][0]**4)/var[-1]**2.)

            write(logfile, "%8.4e  %8.4e  %8.4e  %8.4e  %8d \n" % (avg[-1], var[-1], kurt[-1], cost[-1], 2**p))
        else:
            var.append(numpy.maximum(M/max(M-1,1)*(sums[k][1]-sums[k][0]**2), 1.0e-16)) # fix for cases with var = 0
            write(logfile, "Expected value: %s\n" % avg[-1])
            write(logfile, "Variance: %s\n" % var[-1])

        write(logfile, "\n")

    if mpiRank == 0:
        [numpy.savetxt(logfile.name[:-4] + '-qmc_output%d.txt' % k, avg[k]) for k in range(n_outputs) if isinstance(avg[k], numpy.ndarray)]

    return avg, var, 2**p

def qmc_convergence_test(mlmc_sampler, N, l = 0, p0 = 0, ismlqmc = False, logfile = None):
    """
    (Quasi) Monte Carlo test routine to estimate the convergence rate with respect to the number of samples.

    mlmc_sampler: an mlmc_sampler set up for qmc
    with inputs
      p0 = we check the error every N0 = 2**p0 samples
      N = max(N, int(np.ceil(N/N0))*N0) will be the number of samples
      ismlqmc = boolean flag. If set to False, then mlmc_sampler.sample(N) should return

        a list of numpy arrays of outputs
          sums[i][0] = sum(P)
          sums[i][1] = sum(P**2)
          sums[i][2] = sum(P**3)
          sums[i][3] = sum(P**4)

      else, it should return,

        a list of numpy arrays of outputs
          sums[i][0] = sum(Pf-Pc)
          sums[i][1] = sum((Pf-Pc)**2)
          sums[i][2] = sum((Pf-Pc)**3)
          sums[i][3] = sum((Pf-Pc)**4)
          sums[i][4] = sum(Pf)
          sums[i][5] = sum(Pf**2)

    N: number of samples for convergence tests
    if n_outputs is the number of output functionals of interest, then i in range(n_outputs)
    """

    # First, convergence tests

    if ismlqmc == False:
        write(logfile, "\n")
        write(logfile, "********************************************************************\n")
        write(logfile, "*** Convergence, Nsamples Nk, Sample Average, Error, Nk*Variance ***\n")
        write(logfile, "********************************************************************\n")
        write(logfile, "\n   Nk      ave(P)       est_sd(P)     Nk*Var(P)                   \n")
        write(logfile,   "-------------------------------------------------\n")
    else:
        write(logfile, "\n")
        write(logfile, "*******************************************************************************\n")
        write(logfile, "*** Convergence, Nsamples Nk, Sample Average, Error, Nk*(Sample Variance)   ***\n")
        write(logfile, "*******************************************************************************\n")
        write(logfile, "\n   Nk    ave(Pf-Pc)   est_sd(Pf-Pc)   Nk*Var(Pf-Pc)    Nk*Var(Pf)            \n")
        write(logfile,   "-------------------------------------------------------------------\n")

    avg = []
    err = []
    var = []
    kurt = []
    cost = []
    avg_list = []
    err_list = []
    conv_order = []

    N0 = 2**p0

    p_max = int(numpy.ceil(numpy.log2(N/N0)))
    NN = N0*2**p_max
    nintervals = p_max + 1

    M = mlmc_sampler.M

    n_outputs = mlmc_sampler.n_outputs
    howmanyshifts = mlmc_sampler.howmanyshifts
    n_values = 2

    sums_list   = [[[0.0 for i in range(n_values)] for k in range(n_outputs)] for shift_number in range(howmanyshifts)]
    cost = [0.0]*n_outputs

    avg_list    = [[[0.0]*2 for kk in range(nintervals)] for k in range(n_outputs)]
    avg_list2   = [[[0.0]*2 for kk in range(nintervals)] for k in range(n_outputs)]
    err_list    = [[0.0 for kk in range(nintervals)] for k in range(n_outputs)]
    err_list2   = [[0.0 for kk in range(nintervals)] for k in range(n_outputs)]
    NVar_list   = [[0.0 for kk in range(nintervals)] for k in range(n_outputs)] # Nk*Var of Pf - Pc
    NVar_list2  = [[0.0 for kk in range(nintervals)] for k in range(n_outputs)] # Nk*Var of Pf

    ## NOTE:
    # Let P = E[p(\omega)], let \tilde{P}_k(\omega) = 1/Nk \sum_{j=1}^{Nk} p(\omega_j). Note that we reuse the \omega_j so the \tilde{P}_k are NOT independent
    # Let \bar{P} = 1/M \sum_m \tilde{P}_k(\omega_m).

    for kk in range(nintervals):

        Nk = N0*2**kk

        sumsl_list, costl = mlmc_sampler.sample(N0*2**(kk-int(kk is not 0)), l=l)

        for k in range(n_outputs):
            cost[k] += costl
            for i in range(n_values):
                for shift_number in range(howmanyshifts):
                    sums_list[shift_number][k][i] += sumsl_list[shift_number][k][i]

        est_sums = mlmc_sampler.mlqmc_finalise_sums(sums_list, Nk)
        for k in range(n_outputs):
            for i in range(6):
                est_sums[k][i] /= M

        for k in range(n_outputs):
            avg_list[k][kk][0] = est_sums[k][0]
            avg_list[k][kk][1] = est_sums[k][1]
            err_list[k][kk]    = numpy.sqrt(M/max(M-1,1)*(est_sums[k][1] - est_sums[k][0]**2)/M) # this is V[\tilde{P}]^{1/2}
            NVar_list[k][kk]   = M/max(M-1,1)*Nk*(est_sums[k][1] - est_sums[k][0]**2)/M
            if ismlqmc == True:
                avg_list2[k][kk][0] = est_sums[k][4]
                avg_list2[k][kk][1] = est_sums[k][5]
                err_list2[k][kk]    = numpy.sqrt(M/max(M-1,1)*(est_sums[k][5] - est_sums[k][4]**2)/M) # this is V[\tilde{P}]^{1/2}
                NVar_list2[k][kk]   = M/max(M-1,1)*Nk*(est_sums[k][5] - est_sums[k][4]**2)/M

    conv_order = [0.0 for k in range(n_outputs)]
    for k in range(n_outputs):

        err_list[k]   = numpy.array(err_list[k])
        NVar_list[k]  = numpy.array(NVar_list[k])
        NVar_list2[k] = numpy.array(NVar_list2[k])
        
        if isinstance(err_list[k][0], float):
            pb = numpy.polyfit(numpy.log2(N0) + numpy.arange(nintervals)[-5:], -numpy.log2(err_list[k])[-5:], 1)
            conv_order[k] = pb[0]
        else:
            conv_order[k] = numpy.nan

    for k in range(n_outputs):
        for kk in range(nintervals):
            if isinstance(avg_list[k][kk][0], float):
                if ismlqmc == False:
                    write(logfile, "%5d    %8.4e    %8.4e    %8.4e  \n" % (N0*2**kk, avg_list[k][kk][0], err_list[k][kk], NVar_list[k][kk]))
                else:
                    write(logfile, "%5d    %8.4e    %8.4e      %8.4e      %8.4e  \n" % (N0*2**kk, avg_list[k][kk][0], err_list[k][kk], NVar_list[k][kk], NVar_list2[k][kk]))
            else:
                write(logfile, "Number of samples                   : %d\n" % (N0*2**kk))
                write(logfile, "Sample average                      : %s\n" % avg_list[k][kk][0])
                write(logfile, "Estimator std. dev.                 : %s\n" % err_list[k][kk])
                if ismlqmc == False:
                    write(logfile, "Nsamples * Estimator var            : %s\n" % NVar_list[k][kk])
                if ismlqmc == True:
                    write(logfile, "Nsamples * Estimator Var for (Pf-Pc): %s\n" % NVar_list[k][kk])
                    write(logfile, "Nsamples * Estimator var for Pf     : %s\n" % NVar_list2[k][kk])

        if isinstance(avg_list[-1][k][0], float):
            if ismlqmc == False:
                write(logfile, "\n")
                write(logfile, "**********************************************************\n")
                write(logfile, "*** Estimated convergence order : %f             ***\n" % conv_order[k])
                write(logfile, "**********************************************************\n")
                write(logfile, "\n")
            else:
                write(logfile, "\n")
                write(logfile, "****************************************************************\n")
                write(logfile, "*** Estimated convergence order : %f                   ***\n" % conv_order[k])
                write(logfile, "****************************************************************\n")
                write(logfile, "\n")


    if ismlqmc == False:
        write(logfile, "\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "***      Expected Value, Variance, Kurtosis, Cost      ***\n")
        write(logfile, "**********************************************************\n")
        write(logfile, "\n  ave(P)      var(P)      kurt(P)     cost(P)           \n")
        write(logfile,   "-----------------------------------------------\n")
    else:
        write(logfile, "\n")
        write(logfile, "****************************************************************\n")
        write(logfile, "***      Expected Value, Variance, Kurtosis, Cost            ***\n")
        write(logfile, "****************************************************************\n")
        write(logfile, "\n  ave(Pf-Pc)    var(Pf-Pc)    kurt(Pf-Pc)   cost(Pf-Pc) \n")
        write(logfile,   "---------------------------------------------------------\n")

    sums = mlmc_sampler.mlqmc_finalise_sums(sums_list, NN)
    for k in range(n_outputs):
        cost[k] /= (NN*M)
        for i in range(len(sums[k])):
            sums[k][i] = sums[k][i]/M

        avg.append(sums[k][0])

        if isinstance(avg[-1], float):
            var.append(max(M/max(M-1,1)*(sums[k][1]-sums[k][0]**2)/M, 1.0e-14))
            kurt.append((sums[k][3] - 4*sums[k][0]*sums[k][2] + 6*sums[k][0]**2*sums[k][1] - 3*sums[k][0]**4)/var[-1]**2.)

            if ismlqmc == False:
                write(logfile, "%8.4e  %8.4e  %8.4e  %8.4e \n" % \
                          (avg[-1], var[-1], kurt[-1], cost[-1]))
            else:
                write(logfile, "  %8.4e    %8.4e    %8.4e    %8.4e \n" % \
                          (avg[-1], var[-1], kurt[-1], cost[-1]))
        else:
            var.append(numpy.maximum(M/max(M-1,1)*(sums[k][1]-sums[k][0]**2)/M, 1.0e-14)) # fix for cases with var = 0
            write(logfile, "Expected value: %s\n" % avg[-1])
            write(logfile, "Variance: %s\n" % var[-1])

        write(logfile, "\n")

    if mpiRank == 0 and logfile is not None:
        [numpy.savetxt(logfile.name[:-4] + '-output%d.txt' % k, avg[k]) for k in range(n_outputs) if isinstance(avg[k], numpy.ndarray)]

    return avg

def write(logfile, msg):
    """
    Write to both sys.stdout and to a logfile.
    """
    if mpiRank == 0:
        if logfile is not None:
            logfile.write(msg)
            logfile.flush()
            sys.stdout.write(msg)


