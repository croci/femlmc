#!// vim: set ft=cpp:
#FIXME we work on the unit square/cube so we never risk that an element of meshA is fully outside of meshH.
#      however, what would happen if that was the case? Probably we need to account for that
#
#NOTE: To enable debug mode use -DDEBUG compiler flag

#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

QMC_supermesh_code = r'''
#include <iostream>
#include <ctime>

#include "dolfin.h"
#include "ufc.h"
#include "libsupermesh-c.h"
#include <math.h>
#include <Eigen/Dense>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/pytypes.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/cast.h>

#define QUAD_TRI_BUF_SIZE 46
#define QUAD_TET_BUF_SIZE 729

#define EPS_TOL 0.0e-16

namespace py = pybind11;

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RowMatrixXd;
typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> VectorXi;

std::vector<std::size_t> compute_intersection_candidates_1D(double* normalised_vertex_coordinates, const std::size_t LH);
std::vector<std::size_t> compute_intersection_candidates_2D(double* normalised_vertex_coordinates, const std::size_t LH);
std::vector<std::size_t> libsupermesh_find_intersection_candidates(double* normalised_vertex_coordinates, const int dim, const std::size_t LH);

std::size_t get_haar_index(const double a, const std::size_t LH){
    std::size_t index = std::min(LH-1, (std::size_t) std::max(a*LH, EPS_TOL));
    return index;
}

std::vector<std::size_t> arange(const std::size_t a, const std::size_t b){
    const std::size_t n_entries = b-a;
    std::vector<std::size_t> out(n_entries,0);
    for(std::size_t i = 0; i<n_entries; i++)
        out[i] = a + i;
        
    return out;
}

std::vector<std::size_t> compute_intersection_candidates(double* normalised_vertex_coordinates, const int dim, const std::size_t LH){
    if(dim == 1)
        return compute_intersection_candidates_1D(normalised_vertex_coordinates, LH);
    else if (dim == 2)
        return compute_intersection_candidates_2D(normalised_vertex_coordinates, LH);
    else
        return libsupermesh_find_intersection_candidates(normalised_vertex_coordinates, dim, LH);
}


std::vector<std::size_t> intersect_vertical_segment(const double x_coor, const double y_min, const double y_max, const double* normalised_vertex_coordinates, const int num_vertices, const std::size_t LH, const bool rotate_coordinates){
    double p0_x, p0_y, p1_x, p1_y; // the coordinates of the points of each segment of the polygon
    double xe_min, xe_max, ye_min, ye_max, t0, y0;
    const double eps = 1.0e-14;
    std::size_t Y0, Y1;
    std::vector<double> out;

    for(int i=0; i<num_vertices; i++){

        p0_x = normalised_vertex_coordinates[2*i];
        p0_y = normalised_vertex_coordinates[2*i + 1];
        p1_x = normalised_vertex_coordinates[2*((i+1)%num_vertices)];
        p1_y = normalised_vertex_coordinates[2*((i+1)%num_vertices) + 1];

        if(rotate_coordinates == true){
            std::swap(p0_x, p0_y);
            std::swap(p1_x, p1_y);
        }

        xe_min = std::min(p0_x, p1_x);
        xe_max = std::max(p0_x, p1_x);
        ye_min = std::min(p0_y, p1_y);
        ye_max = std::max(p0_y, p1_y);

        // if any of the following is satisfied, the segments cannot intersect
        if ((xe_min > x_coor + eps) || (xe_max + eps < x_coor)) // the following are useless as y_min <= ye_min <= ye_max <= y_max always in our application || (ye_min > y_max + eps) || (ye_max + eps < y_min))
            continue;

        // in this case, we have xe_min=xe_max and we know that they are equal to x_coor within tolerance so the segments coincide and we return the intersection
        if (std::fabs(xe_min-xe_max) < eps){
            out.push_back(ye_min);
            out.push_back(ye_max);

        }else{
            t0 = (x_coor - p0_x)/(p1_x - p0_x);
            y0 = p0_y + t0*(p1_y - p0_y);
            out.push_back(y0);
        }
    }

    Y0 = get_haar_index(*std::min_element(out.begin(), out.end()) - EPS_TOL, LH);
    Y1 = get_haar_index(*std::max_element(out.begin(), out.end()) + EPS_TOL, LH);

    std::vector<std::size_t> endpoints(2,0);
    endpoints[0] = Y0;
    endpoints[1] = Y1;

    return endpoints;
}

std::vector<std::size_t> compute_intersection_candidates_1D(double* normalised_vertex_coordinates, const std::size_t LH){
    const double pmin = std::min(normalised_vertex_coordinates[0], normalised_vertex_coordinates[1]);
    const double pmax = std::max(normalised_vertex_coordinates[0], normalised_vertex_coordinates[1]);
    const std::size_t xmin = get_haar_index(pmin - EPS_TOL, LH);
    const std::size_t xmax = get_haar_index(pmax + EPS_TOL, LH);
    return arange(xmin,xmax+1);
}

// NOTE: We decided to not implement the faster intersection candidates routine in 3D.
//       If normalised_vertex_coordinates is a polygon, the vertices must be ordered clockwise or anti-clockwise
std::vector<std::size_t> compute_intersection_candidates_2D(double* normalised_vertex_coordinates, const std::size_t LH){

    const int num_vertices = 3;
    std::vector<std::size_t> out;
    std::vector<std::size_t> vertex_indices_x(num_vertices, 0), vertex_indices_y(num_vertices, 0);

    std::size_t* xmin  = new std::size_t[2];
    std::size_t* xmax  = new std::size_t[2];
    xmin[0] = std::numeric_limits<std::size_t>::max();
    xmin[1] = std::numeric_limits<std::size_t>::max();
    xmax[0] = 0;
    xmax[1] = 0;
    
    for(int i=0; i<num_vertices; i++){
        vertex_indices_x[i] = get_haar_index(normalised_vertex_coordinates[2*i], LH);
        vertex_indices_y[i] = get_haar_index(normalised_vertex_coordinates[2*i+1], LH);
        xmin[0] = std::min(xmin[0], get_haar_index(normalised_vertex_coordinates[2*i] - EPS_TOL, LH));
        xmax[0] = std::max(xmax[0], get_haar_index(normalised_vertex_coordinates[2*i] + EPS_TOL, LH));
        xmin[1] = std::min(xmin[1], get_haar_index(normalised_vertex_coordinates[2*i+1] - EPS_TOL, LH));
        xmax[1] = std::max(xmax[1], get_haar_index(normalised_vertex_coordinates[2*i+1] + EPS_TOL, LH));
    }

    const std::size_t delta_x = xmax[0] - xmin[0];
    const std::size_t delta_y = xmax[1] - xmin[1];

    // everything contained in the same haar cell
    if (delta_x == 0 && delta_y == 0){
        out.push_back(xmax[0] + LH*xmax[1]);
    }else if(delta_x == 0){
        out = arange(xmin[1], xmax[1]+1);
        for(std::size_t i = 0; i < delta_y+1; i++)
            out[i] = xmax[0] + LH*out[i];
    }else if(delta_y == 0){
        out = arange(xmin[0], xmax[0]+1);
        for(std::size_t i = 0; i < delta_x+1; i++)
            out[i] = out[i] + LH*xmax[1];
    }else{
        const double inv_LH = 1.0/((double) LH);
        std::vector<std::size_t> endpoints0;
        std::vector<std::size_t> endpoints1;
        std::vector<std::size_t> temp_coor0;
        std::vector<std::size_t> temp_coor1;
        std::vector<std::vector<std::size_t>> vertex_finder(delta_x + 1);

        if(delta_x <= delta_y){
            double x_coor;
            const double y_min = xmin[1]*inv_LH;
            const double y_max = (xmax[1]+1)*inv_LH;

            for(int j=0; j<num_vertices; j++){
                for(std::size_t i=0; i<delta_x+1; i++){
                    if(vertex_indices_x[j] == xmin[0] + i)
                        vertex_finder[i].push_back(vertex_indices_y[j]);
                }
            }

            for(std::size_t i=0; i<delta_x; i++){
                x_coor = inv_LH*(xmin[0] + i + 1);

                endpoints0 = intersect_vertical_segment(x_coor, y_min, y_max, normalised_vertex_coordinates, num_vertices, LH, false);
                endpoints1 = endpoints0;

                for(std::size_t j=0; j<vertex_finder[i].size(); j++)
                    endpoints0.push_back(vertex_finder[i][j]);

                for(std::size_t j=0; j<vertex_finder[i+1].size(); j++)
                    endpoints1.push_back(vertex_finder[i+1][j]);

                temp_coor0 = arange(*std::min_element(endpoints0.begin(), endpoints0.end()), *std::max_element(endpoints0.begin(), endpoints0.end())+1);
                temp_coor1 = arange(*std::min_element(endpoints1.begin(), endpoints1.end()), *std::max_element(endpoints1.begin(), endpoints1.end())+1);

                for(unsigned int j=0; j<temp_coor0.size(); j++){
                    out.push_back(xmin[0] + i + temp_coor0[j]*LH);
                }

                for(unsigned int j=0; j<temp_coor1.size(); j++){
                    out.push_back(xmin[0] + i + 1 + temp_coor1[j]*LH);
                }
            }

        }else{
            // same as before, but we are swapping the x and y coordinates
            double y_coor;
            const double x_min = xmin[0]*inv_LH;
            const double x_max = (xmax[0]+1)*inv_LH;

            for(int j=0; j<num_vertices; j++){
                for(std::size_t i=0; i<delta_y+1; i++){
                    if(vertex_indices_y[j] == xmin[1] + i)
                        vertex_finder[i].push_back(vertex_indices_x[j]);
                }
            }

            for(std::size_t i=0; i<delta_y; i++){
                y_coor = inv_LH*(xmin[1] + i + 1);

                // NOTE: edges x and y coordinates must be swapped here
                endpoints0 = intersect_vertical_segment(y_coor, x_min, x_max, normalised_vertex_coordinates, num_vertices, LH, true);
                endpoints1 = endpoints0;

                for(std::size_t j=0; j<vertex_finder[i].size(); j++)
                    endpoints0.push_back(vertex_finder[i][j]);

                for(std::size_t j=0; j<vertex_finder[i+1].size(); j++)
                    endpoints1.push_back(vertex_finder[i+1][j]);

                temp_coor0 = arange(*std::min_element(endpoints0.begin(), endpoints0.end()), *std::max_element(endpoints0.begin(), endpoints0.end())+1);
                temp_coor1 = arange(*std::min_element(endpoints1.begin(), endpoints1.end()), *std::max_element(endpoints1.begin(), endpoints1.end())+1);

                for(unsigned int j=0; j<temp_coor0.size(); j++){
                    out.push_back(temp_coor0[j] + (xmin[1] + i)*LH);
                }

                for(unsigned int j=0; j<temp_coor1.size(); j++){
                    out.push_back(temp_coor1[j] + (xmin[1] + i + 1)*LH);
                }
            }

        }
    }

    // NOTE: a unique is needed
    std::sort(out.begin(), out.end());
    auto last = std::unique(out.begin(), out.end());
    out.erase(last, out.end());

    delete [] xmin;
    delete [] xmax;

    return out;
}

template<typename T>
void print_vec(T* a, std::size_t dim){
        for(std::size_t i=0; i<dim;i++)
            std::cout << a[i] << " ";

        std::cout << "\n";

}

template<typename T>
void print_vec(std::vector<T> &a){
        for(std::size_t i=0; i<a.size();i++)
            std::cout << a[i] << " ";

        std::cout << "\n";

}

// this is is slower in 1D and 2D. I do not know how much slower it is in 3D, but finding the candidate intersections in 3D is far
// from being the computational bottleneck so we use this in 3D
std::vector<std::size_t> libsupermesh_find_intersection_candidates(double* normalised_vertex_coordinates, const int dim, const std::size_t LH){

    //FIXME: need to normalise normalised_vertex_coordinates

    const long nnodes_a = dim + 1, nelements_a = 1;
    const int eldim = dim + 1, eldimH =  std::pow(2, dim);
    const double inv_LH = 1.0/((double) LH);

    const std::vector<std::size_t> enlist_a = arange(0, eldim);
    double* norm_tri_a  = new double[dim*eldim];

    std::size_t* xmin  = new std::size_t[dim];
    std::size_t* xmax  = new std::size_t[dim];
    std::size_t* deltas  = new std::size_t[dim];
    for(int j=0; j<dim;j++){
        xmin[j] = std::numeric_limits<std::size_t>::max();
        xmax[j] = 0;
    }
    
    for(int i=0; i<eldim; i++){
        for(int j=0;j<dim;j++){
            xmin[j] = std::min(xmin[j], get_haar_index(normalised_vertex_coordinates[dim*i+j] - EPS_TOL, LH));
            xmax[j] = std::max(xmax[j], get_haar_index(normalised_vertex_coordinates[dim*i+j] + EPS_TOL, LH));
        }
    }

    for(int i=0; i<eldim; i++){
        for(int j=0;j<dim;j++){
            norm_tri_a[dim*i + j] = normalised_vertex_coordinates[dim*i + j] - xmin[j]*inv_LH;
        }
    }

    long nelements_h = 1, nnodes_h = 1;
    for(int j=0; j<dim;j++){
        deltas[j] = xmax[j] - xmin[j];
        nelements_h *= deltas[j] + 1;
        nnodes_h *= deltas[j] + 2;
    }

    // the following is the map from normalised haar element indices to unnormalised haar element indices
    std::vector<std::size_t> haar_element_map(nelements_h, 0);
    std::vector<double> coordinates_b(dim*nnodes_h, 0.0);
    std::vector<std::size_t> enlist_h(eldimH*nelements_h, 0);

    if(dim==1){
        for(std::size_t i=0;i<deltas[0]+1; i++)
            haar_element_map[i] = xmin[0] + i;
        for(std::size_t i=0;i<deltas[0]+2; i++)
            coordinates_b[i] = i*inv_LH;
        for(std::size_t i=0;i<deltas[0]+1; i++){
            enlist_h[2*i] = i;
            enlist_h[2*i+1] = i+1;
        }
    }else if (dim==2){
        for(std::size_t i=0;i<deltas[0]+1; i++)
            for(std::size_t j=0;j<deltas[1]+1; j++)
                haar_element_map[i + (deltas[0]+1)*j] = xmin[0] + i + (xmin[1]+j)*LH;

        // copied from RectangleMesh::build_quad
        std::size_t vertex = 0;
        for(std::size_t i=0; i<deltas[1]+2; i++){
            for(std::size_t j=0; j<deltas[0]+2; j++){
                coordinates_b[2*vertex] = j*inv_LH;
                coordinates_b[2*vertex+1] = i*inv_LH;
                vertex++;
            }
        }

        std::size_t cell = 0;
        for (std::size_t i = 0; i < deltas[1]+1; i++){
            for (std::size_t j = 0; j < deltas[0]+1; j++){
                enlist_h[4*cell  ] = i*(deltas[0] + 1 + 1) + j;
                enlist_h[4*cell+1] = enlist_h[4*cell  ] + 1;
                enlist_h[4*cell+2] = enlist_h[4*cell  ] + (deltas[0] + 1 + 1);
                enlist_h[4*cell+3] = enlist_h[4*cell+1] + (deltas[0] + 1 + 1);
                cell++;
            }
        }

    }else{
        for(std::size_t i=0;i<deltas[0]+1; i++)
            for(std::size_t j=0;j<deltas[1]+1; j++)
                for(std::size_t k=0;k<deltas[2]+1; k++)
                    haar_element_map[i + (deltas[0]+1)*j + (deltas[0]+1)*(deltas[1]+1)*k] = xmin[0] + i + (xmin[1]+j)*LH + (xmin[2] + k)*std::pow(LH,2);

        std::size_t vertex = 0;
        for(std::size_t i=0; i<deltas[2]+2; i++){
            for(std::size_t j=0; j<deltas[1]+2; j++){
                for(std::size_t k=0; k<deltas[0]+2; k++){
                    coordinates_b[3*vertex  ] = k*inv_LH;
                    coordinates_b[3*vertex+1] = j*inv_LH;
                    coordinates_b[3*vertex+2] = i*inv_LH;
                    vertex++;
                }
            }
        }

        // copied from BoxMesh::build_hex
        std::size_t cell = 0;
        for (std::size_t i = 0; i < deltas[2]+1; i++){
            for (std::size_t j = 0; j < deltas[1]+1; j++){
                for (std::size_t k = 0; k < deltas[0]+1; k++){
                    enlist_h[8*cell    ] = (i*(deltas[1]+1 + 1) + j)*(deltas[0]+1 + 1) + k;
                    enlist_h[8*cell + 1] = enlist_h[8*cell    ] + 1;
                    enlist_h[8*cell + 2] = enlist_h[8*cell    ] + (deltas[0]+1 + 1);
                    enlist_h[8*cell + 3] = enlist_h[8*cell + 1] + (deltas[0]+1 + 1);
                    enlist_h[8*cell + 4] = enlist_h[8*cell    ] + (deltas[0]+1 + 1)*(deltas[1]+1 + 1);
                    enlist_h[8*cell + 5] = enlist_h[8*cell + 1] + (deltas[0]+1 + 1)*(deltas[1]+1 + 1);
                    enlist_h[8*cell + 6] = enlist_h[8*cell + 2] + (deltas[0]+1 + 1)*(deltas[1]+1 + 1);
                    enlist_h[8*cell + 7] = enlist_h[8*cell + 3] + (deltas[0]+1 + 1)*(deltas[1]+1 + 1);
                    cell++;
                }
            }
        }

    }

    if(dim == 1)
        libsupermesh_sort_intersection_finder_set_input((long*) &nnodes_a, (int*) &dim, (long*) &nelements_a, (int*) &eldim, (long*) &nnodes_h, (int*) &dim, &nelements_h, (int*) &eldimH, norm_tri_a, (long*) enlist_a.data(), coordinates_b.data(), (long*) enlist_h.data());
    else
        libsupermesh_tree_intersection_finder_set_input((long*) &nnodes_a, (int*) &dim, (long*) &nelements_a, (int*) &eldim, (long*) &nnodes_h, (int*) &dim, &nelements_h, (int*) &eldimH, norm_tri_a, (long*) enlist_a.data(), coordinates_b.data(), (long*) enlist_h.data());

    std::size_t nindices_ab;

    if(dim == 1)
        libsupermesh_sort_intersection_finder_query_output((long*) &nindices_ab);
    else
        libsupermesh_tree_intersection_finder_query_output((long*) &nindices_ab);

    // nelements_a are the rows, the columns are given by indices_ab,
    // indices_ab[ind_ptr_ab[i]:ind_ptr_ab[i+1]] contains the indices of the elements of mesh B
    // which are likely to intersect with the i-th element of mesh A.
    std::size_t* indices_ab = new std::size_t[nindices_ab];
    std::size_t* ind_ptr_ab = new std::size_t[nelements_a + 1];
    if(dim == 1)
        libsupermesh_sort_intersection_finder_get_output((long*) &nelements_a, (long*) &nindices_ab, (long*) indices_ab, (long*) ind_ptr_ab);
    else
        libsupermesh_tree_intersection_finder_get_output((long*) &nelements_a, (long*) &nindices_ab, (long*) indices_ab, (long*) ind_ptr_ab);


    std::size_t how_many_intersect = ind_ptr_ab[1] - ind_ptr_ab[0];
    std::vector<std::size_t> intersecting_elements(how_many_intersect, 0);

    for(std::size_t j=0;j<how_many_intersect;j++)
        intersecting_elements[j] = haar_element_map[indices_ab[ind_ptr_ab[0] + j]];

    delete [] xmin;
    delete [] xmax;
    delete [] norm_tri_a;
    delete [] deltas;
    delete [] indices_ab;
    delete [] ind_ptr_ab;
    
    return intersecting_elements;
}

namespace dolfin {

    double hex_cell_volume(const Cell hexahedron){

        //-------------------------------------------------------------------------
        // Name:
        //   hex_cell_volume(const Cell c):
        //
        // Description:
        //   Compute Volume of an LD Hexahedron cell given the 8 vertices
        //    from paper: 
        //    @book{Grandy_1997, 
        //    title={Efficient computation of volume of hexahedral cells}, 
        //    url={http://www.osti.gov/scitech/servlets/purl/632793}, 
        //    DOI={10.2172/632793}, 
        //    abstractNote={This report describes an efficient method to compute 
        //                  the volume of hexahedral cells used in three-dimensional 
        //                  hydrodynamics simulation. Two common methods for 
        //                  creating the hexahedron using triangular boundaries are 
        //                  considered.}, 
        //    author={Grandy, J.}, year={1997}, month={Oct}}
        //    http://dx.doi.org/10.2172/632793
        //
        //    6*V_LD = 
        // |x6-x0  x3-x0  x2-x7|   |x6-x0  x4-x0  x7-x5|   |x6-x0  x1-x0  x5-x2|
        // |y6-y0  y3-y0  y2-y7| + |y6-y0  y4-y0  y7-y5| + |y6-y0  y1-y0  y5-y2|
        // |z6-z0  z3-z0  z2-z7|   |z6-z0  z4-z0  z7-z5|   |z6-z0  z1-z0  z5-z2|
        //
        // Inputs:
        //   c  -   Hexahedron Cell
        // 
        // Output:
        //   Volume of LD hexahedron cell
        //-------------------------------------------------------------------------


        // Check that we get a hexahedron
        if (hexahedron.dim() != 3)
        {
        dolfin_error("HexahedronCell.cpp",
                     "compute volume of hexahedron cell",
                     "Illegal mesh entity, not a hexahedron");
        }

	// Get mesh geometry
        const MeshGeometry& geometry = hexahedron.mesh().geometry();

        // Only know how to compute the volume when embedded in R^3
        if (geometry.dim() != 3)
        {
        dolfin_error("HexahedronCell.cpp",
                     "compute volume of hexahedron",
                     "Only know how to compute volume when embedded in R^3");
        }

        // Get the coordinates of the eight vertices
        // IMPORTANT: the formula is correct only if the vertices are ordered correctly
        //            (same   order as gmsh and the order used in the Supermesh routines).
        //            If the standard order of hex cell vertices changes in dolfin, the
        //            following order must be changed as well.
        const unsigned int* vertices = hexahedron.entities(0);
        const Point x1 = geometry.point(vertices[0]);
        const Point x2 = geometry.point(vertices[1]);
        const Point x3 = geometry.point(vertices[3]);
        const Point x4 = geometry.point(vertices[2]);
        const Point x5 = geometry.point(vertices[4]);
        const Point x6 = geometry.point(vertices[5]);
        const Point x7 = geometry.point(vertices[7]);
        const Point x8 = geometry.point(vertices[6]);

        const double v = 
            (x7[0]-x1[0])*((x4[1]-x1[1])*(x3[2]-x8[2]) - (x3[1]-x8[1])*(x4[2]-x1[2]))+
            (x4[0]-x1[0])*((x3[1]-x8[1])*(x7[2]-x1[2]) - (x7[1]-x1[1])*(x3[2]-x8[2]))+
            (x3[0]-x8[0])*((x7[1]-x1[1])*(x4[2]-x1[2]) - (x4[1]-x1[1])*(x7[2]-x1[2]))
            +
            (x7[0]-x1[0])*((x5[1]-x1[1])*(x8[2]-x6[2]) - (x8[1]-x6[1])*(x5[2]-x1[2]))+
            (x5[0]-x1[0])*((x8[1]-x6[1])*(x7[2]-x1[2]) - (x7[1]-x1[1])*(x8[2]-x6[2]))+
            (x8[0]-x6[0])*((x7[1]-x1[1])*(x5[2]-x1[2]) - (x5[1]-x1[1])*(x7[2]-x1[2]))
            +
            (x7[0]-x1[0])*((x2[1]-x1[1])*(x6[2]-x3[2]) - (x6[1]-x3[1])*(x2[2]-x1[2]))+
            (x2[0]-x1[0])*((x6[1]-x3[1])*(x7[2]-x1[2]) - (x7[1]-x1[1])*(x6[2]-x3[2]))+
            (x6[0]-x3[0])*((x7[1]-x1[1])*(x2[2]-x1[2]) - (x2[1]-x1[1])*(x7[2]-x1[2]));

        return std::abs(v)/6.0;
    }

    template <typename T>
    std::vector<std::size_t> argsort(const std::vector<T> &v) {

	// initialize original index locations
	std::vector<std::size_t> idx(v.size());
	std::iota(idx.begin(), idx.end(), 0);

	// sort indexes based on comparing values in v
	std::sort(idx.begin(), idx.end(),
	    [&v](std::size_t i1, std::size_t i2) {return v[i1] < v[i2];});

	return idx;
    }

    static std::tuple<Eigen::MatrixXd, Eigen::VectorXd> make_affine_mapping(const double* xs, const double* ys, const int dim)
    {
        int row_cur=0, col_start = 0, col_finish=0;

        Eigen::MatrixXd mat = Eigen::MatrixXd::Zero(dim*(dim+1), dim*(dim+1));
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dim*(dim+1));

        for(int i=0;i<dim+1;i  ++){
            for(int j=0;j<dim;j++){
                row_cur = i*dim + j;
                col_start = dim * j;
                col_finish = col_start + dim;

                for(int k=col_start;k<col_finish;k++)
                    mat(row_cur, k) = xs[i*dim + k - col_start];

                rhs(row_cur) = ys[i*dim + j];
                mat(row_cur, dim*dim + j) = 1.0;
            }
        }

        Eigen::VectorXd sol = mat.colPivHouseholderQr().solve(rhs);

        // NOTE: for some reason you get that A is transposed, hence we transpose it ourselves :)
        Eigen::MatrixXd A = Eigen::Map<Eigen::MatrixXd>(sol.data(), dim, dim).transpose();
        Eigen::VectorXd b = Eigen::VectorXd::Zero(dim);
        for(int i=0;i<dim;i++)
            b(i) = sol(dim*dim + i);
        //Eigen::VectorXd b = Eigen::Map<Eigen::VectorXd>(sol.tail(dim).data(), dim);

        return std::make_tuple(A,b);
    }

    static std::vector<double> map_dofs(double* ref_simplex_coor, double* ref_dofs_coor, double* simplex_coor, const int dim, const int n_dofs){

        Eigen::MatrixXd A;
        Eigen::VectorXd b;

        std::tie(A,b) = make_affine_mapping(ref_simplex_coor, simplex_coor, dim);

        RowMatrixXd ref_nodes = Eigen::Map<RowMatrixXd>(ref_dofs_coor, n_dofs, dim);

        RowMatrixXd out = (A*ref_nodes.transpose()).transpose();
        out.rowwise() += b.transpose();
        
        std::vector<double> out_vec(out.data(), out.data() + out.rows() * out.cols());

        return out_vec;
    }

class CoordinateMapper
{
    public:
        unsigned int dim;
        Eigen::MatrixXd A;
        Eigen::VectorXd b;

        void setUp(double* simplexA_coor, double* simplexB_coor, const unsigned int _dim){
            dim = _dim;
            std::tie(A,b) = make_affine_mapping(simplexA_coor, simplexB_coor, _dim);
        }

        std::vector<double> map_coordinates(double* pointsA_coordinates, const unsigned int n_pointsA) {
            RowMatrixXd coordA = Eigen::Map<RowMatrixXd>(pointsA_coordinates, n_pointsA, dim);

            RowMatrixXd out = (A*coordA.transpose()).transpose();
            out.rowwise() += b.transpose();
            
            std::vector<double> pointsB_coordinates(out.data(), out.data() + out.rows() * out.cols());

            return pointsB_coordinates;
        }
};

class QMCWhiteNoiseConstructor
{

    public:
        QMCWhiteNoiseConstructor(std::shared_ptr<const FunctionSpace> Va, const int Lh, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs);

        RowMatrixXd sample_white_noise_correction(Eigen::Ref<const RowMatrixXd> reference_cholesky, Eigen::Ref<const Eigen::VectorXd> reference_haar_vector, Eigen::Ref<const RowMatrixXd> r);
        Eigen::VectorXd get_full_interp();
        Eigen::VectorXd get_supermesh_element_volumes();
        Eigen::VectorXd get_haar_midpoints();
        VectorXi get_cells_in_haar_cell();
        double get_haar_cell_volume();
        double get_domain_volume();
        std::size_t get_haar_level();
        std::size_t get_n_haar_cells();
        int get_dim();
        int get_n_parent_meshes();
        VectorXi get_intersection_map();

        std::vector<double> normalised_haar_cell_midpoints;
        std::vector<double> full_interpA;
        std::vector<std::size_t> intersection_map;
        std::vector<std::size_t> cells_in_haar_cell;
        Eigen::VectorXd supermesh_element_volumes;

        #ifdef DEBUG
            VectorXi get_n_intersection_candidates();
            VectorXi get_n_actual_intersections();
            Eigen::VectorXd get_duration();

            std::vector<double> duration;
            std::vector<std::size_t> n_intersection_candidates;
            std::vector<std::size_t> n_actual_intersections;
        #endif

        int _tdim;
        double haar_cell_volume;
        std::size_t n_supermesh_cells;
        std::size_t n_haar_cells;
        std::size_t haar_level;
        int n_parent_meshes;

    private:

};

QMCWhiteNoiseConstructor::QMCWhiteNoiseConstructor(std::shared_ptr<const FunctionSpace> Va, const int Lh, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs)
{

#ifdef DEBUG
    std::clock_t start, start2;
    start2 = std::clock();
    duration.resize(4);
    std::fill(duration.begin(), duration.end(), 0.0);
#endif

n_parent_meshes = 2;

const std::shared_ptr<const Mesh> meshA = Va->mesh();

auto feA = Va->element();

auto ufc_feA = feA->ufc_element();

const std::string signA = feA->signature();
const std::string lgrnge("Lagrange");
const bool islagrange = (signA.find(lgrnge) != std::string::npos);


const int gdim = meshA->geometry().dim();
const int tdim = meshA->topology().dim();
_tdim = tdim;

haar_level = Lh;

// extract mesh vertex coordinates, we need to reshape them into
// a matrix of size geometric_dimension-by-number_of_vertices
const std::vector<double> coorA = meshA->coordinates(); // coorA is the flattened version of a [nnodes_a][gdim] array

// Need to get the points from meshA coordinates (meshA is a box domain)
std::vector<double> lower_bnd(gdim), upper_bnd(gdim), interval_size(gdim);
for(int i=0;i<gdim;i++){
    lower_bnd[i] = coorA[i];
    upper_bnd[i] = coorA[i];
}
for(std::size_t i=1;i<coorA.size()/gdim; i++){
    for(int j=0;j<gdim;j++){
        lower_bnd[j] = std::min(lower_bnd[j], coorA[i*gdim + j]);
        upper_bnd[j] = std::max(upper_bnd[j], coorA[i*gdim + j]);
    }
}

for(int i=0; i<gdim; i++)
    interval_size[i] = upper_bnd[i] - lower_bnd[i];

const std::size_t LH = std::pow(2,Lh);

Mesh meshH;

if(gdim==1){
    meshH = IntervalMesh(Va->mesh()->mpi_comm(), LH, lower_bnd[0], upper_bnd[0]);

}else{
    const std::array<Point, 2> p = {Point(gdim, lower_bnd.data()), Point(gdim, upper_bnd.data())};

    if(gdim==2){
        const std::array<std::size_t, 2> n = {LH, LH};
        meshH = RectangleMesh::create(meshA->mpi_comm(), p, n, CellType::Type::quadrilateral);

    }else{
        const std::array<std::size_t, 3> n = {LH, LH, LH};
        meshH = BoxMesh::create(meshA->mpi_comm(), p, n, CellType::Type::hexahedron);
    }
}

if(gdim < 3)
    haar_cell_volume = Cell(meshH, 0).volume();
else
    haar_cell_volume = hex_cell_volume(Cell(meshH, 0));

// extract mesh vertex coordinates, we need to reshape them into
// a matrix of size geometric_dimension-by-number_of_vertices
const std::vector<double> coorH = meshH.coordinates(); // coorH is the flattened version of a [nnodes_b][gdim] array

Cell cellA;

ufc::cell ufc_cellA;

std::vector<double> coordinate_dofsA;

const unsigned int space_dimA = feA->space_dimension();

// Number of dofs associated with each fine point
unsigned int _data_size = 1;
for (unsigned data_dim = 0; data_dim < feA->value_rank(); data_dim++){
  _data_size *= feA->value_dimension(data_dim);
}
const unsigned int data_size = _data_size;

const std::size_t nelements_a = meshA->num_cells();
const std::size_t nelements_h = meshH.num_cells();

const std::size_t mA = space_dimA*data_size;

// initialise vector where to store the evaluated basis functions
std::vector<double> evaluation_values(mA);
// initialise local interpolation matrices
std::vector<double> local_interpA(mA*mA, 0.0);

// see Patrick's paper for the number of elements of the supermesh
// I have no clue of what the values should be for quads and hexes, double check with what happens in practice
int cdim;
if(tdim == 1)
    cdim = 2;
else if(tdim == 2)
    cdim = 4;
else
    cdim = 90;

// extract mesh elements/cells to vertices maps, we need to reshape them into
// a matrix of size element_dimension-by-number_of_elements
const std::vector<unsigned int> cellsA = meshA->cells(); // cellsA is the flattened version of a [nelements_a][eldim] array
const std::vector<unsigned int> cellsH = meshH.cells(); // cellsH is the flattened version of a [nelements_h][eldim] array

// convert to a vector of long (needed for libsupermesh)
const std::vector<long> enlist_a(cellsA.begin(), cellsA.end());
const std::vector<long> enlist_h(cellsH.begin(), cellsH.end());

// eldim is the number of vertices of every mesh element
const unsigned int eldim = tdim + 1;
const unsigned int eldimH = std::pow(2, tdim);

normalised_haar_cell_midpoints.reserve(gdim*nelements_h);
for(CellIterator cell(meshH); !cell.end(); ++cell){
    Point pt = cell->midpoint();
    for(int i=0; i<gdim; i++){
        // map back to the [0,1] interval
        normalised_haar_cell_midpoints.push_back((pt[i] - lower_bnd[i])/interval_size[i]);
    }
}

// NOTE: the following flattened version is equivalent
//       to the two lines below, but it works even
//       if gdim is not known at compile time
//auto tri_a  = new double[eldim][gdim];
//auto quad_h = new double[eldimH][gdim];
double* tri_a  = new double[eldim*gdim];
double* normalised_tri_a  = new double[eldim*gdim];
double* quad_h = new double[eldimH*gdim];
std::vector<double*> quad_h_sorter(eldimH);

std::vector<std::size_t> intersecting_elements;
std::vector<double> tris_c_dofs;
int n_tris_c = 0;

// need to preallocate tris_c
double* tris_c = nullptr;
double* tris_c_alldofs = nullptr;
double* evaluation_valuesA0 = nullptr;
double* evaluation_valuesA1 = nullptr;
// flattened version of auto tris_c = new double[QUAD_TRI_BUF_SIZE][eldim][gdim]
// need to flatten as eldim and gdim are not known at compile time
if(tdim==1){
    tris_c = new double[eldim*gdim];
    tris_c_alldofs = new double[mA*gdim];
    evaluation_valuesA0 = new double[mA*mA];
    evaluation_valuesA1 = new double[mA*mA];
}else if(tdim == 2){
    tris_c = new double[QUAD_TRI_BUF_SIZE*eldim*gdim];
    tris_c_alldofs = new double[QUAD_TRI_BUF_SIZE*mA*gdim];
    evaluation_valuesA0 = new double[QUAD_TRI_BUF_SIZE*mA*mA];
    evaluation_valuesA1 = new double[QUAD_TRI_BUF_SIZE*mA*mA];
}else{
    tris_c = new double[QUAD_TET_BUF_SIZE*eldim*gdim];
    tris_c_alldofs = new double[QUAD_TET_BUF_SIZE*mA*gdim];
    evaluation_valuesA0 = new double[QUAD_TET_BUF_SIZE*mA*mA];
    evaluation_valuesA1 = new double[QUAD_TET_BUF_SIZE*mA*mA];
}

double* tris_flat = new double[eldim*gdim];
double tris_flat_volume = 0.0;

std::vector<std::vector<double>> full_interpA_buf(nelements_h);
std::vector<std::vector<double>> supermesh_element_volumes_buf(nelements_h);
std::vector<std::vector<std::size_t>> intersection_map_buf(nelements_h);

for(std::size_t i=0; i<nelements_h; i++){
    full_interpA_buf[i].reserve(cdim*mA*mA*std::max(nelements_a, nelements_h)/nelements_h);
    intersection_map_buf[i].reserve(cdim*std::max(nelements_a, nelements_h)/nelements_h);
    supermesh_element_volumes_buf[i].reserve(cdim*std::max(nelements_a, nelements_h)/nelements_h);
}

#ifdef DEBUG
    n_intersection_candidates.reserve(nelements_a);
    n_actual_intersections.reserve(nelements_a);
#endif
int how_many_intersect = 0;
std::size_t elemH;

std::vector<double> mapped_tris_cA;
CoordinateMapper cmapperA;

for(std::size_t i = 0; i < nelements_a; i++){

    cellA = Cell(*meshA, i);
    cellA.get_coordinate_dofs(coordinate_dofsA);
    cellA.get_cell_data(ufc_cellA);

    for (unsigned int l = 0; l < eldim; l++){
        for (int k = 0; k < gdim; k++){
            tri_a[l*gdim + k] = coorA[enlist_a[i*eldim + l]*gdim + k];
            normalised_tri_a[l*gdim + k] = (tri_a[l*gdim + k] - lower_bnd[k])/interval_size[k];
        }
    }

    #ifdef DEBUG
        start = std::clock();
    #endif

    intersecting_elements = compute_intersection_candidates(normalised_tri_a, gdim, LH);

    #ifdef DEBUG
        duration[0] += ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    #endif

    how_many_intersect = intersecting_elements.size();

    #ifdef DEBUG
        n_intersection_candidates.push_back(how_many_intersect);
        n_actual_intersections.push_back(0);
    #endif

    if(how_many_intersect > 0)
        cmapperA.setUp(tri_a, reference_simplex_coor.data(), gdim);

    for(int j = 0; j < how_many_intersect; j++){
        
        elemH = intersecting_elements[j];

        // NOTE: all the mess that follows is to reorder the coordinates of quad_h so that they
        //       are in the correct ordering needed by libsupermesh (see libsupermesh documentation). We are sorting the coordinates
        //       in ascending order first by z, then by y, then by x. Then we swap some coordinates as needed.
        for (unsigned int l = 0; l < eldimH; l++)
            quad_h_sorter[l] = (double*) &coorH[enlist_h[elemH*eldimH + l]*gdim];

        if(gdim > 1){
        std::sort(quad_h_sorter.begin(), quad_h_sorter.end(), [gdim](double* a, double* b) { 
            bool out, equal = true;
            double del;
            int kk = gdim-1;
            while(kk>=0 && equal == true){
                del   = *(a+kk) - *(b+kk);
                equal = std::fabs(del) < 3.0*DOLFIN_EPS;
                out   = del < 0;
                kk--;
            }
            return out;   
        });

        std::swap(quad_h_sorter[2], quad_h_sorter[3]);
        if(gdim==3)
            std::swap(quad_h_sorter[6], quad_h_sorter[7]);

        }

        for (unsigned int l = 0; l < eldimH; l++){
            for (int k = 0; k < gdim; k++){
                quad_h[l*gdim + k] = *(quad_h_sorter[l] + k);
            }
        }

        #ifdef DEBUG
            start = std::clock();
        #endif

        if(tdim == 1)
            libsupermesh_intersect_intervals((double*) tri_a, (double*) quad_h, (double*) tris_c, (int*) &n_tris_c);
        else if(tdim == 2)
            libsupermesh_intersect_tri_quad( (double*) tri_a, (double*) quad_h, (double*) tris_c, (int*) &n_tris_c);
        else
            libsupermesh_intersect_tet_hex(  (double*) tri_a, (double*) quad_h, (double*) tris_c, (int*) &n_tris_c);

        #ifdef DEBUG
            duration[1] += ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
            if(n_tris_c > 0)
                n_actual_intersections.back() += 1;
        #endif

        #ifdef DEBUG
            start = std::clock();
        #endif

        for(int ss = 0; ss < n_tris_c; ss++){
            if(tdim == 1)
                libsupermesh_interval_size(&tris_c[ss*eldim*gdim], (double*) &tris_flat_volume);
            else if(tdim == 2)
                libsupermesh_triangle_area(&tris_c[ss*eldim*gdim], (double*) &tris_flat_volume);
            else
                libsupermesh_tetrahedron_volume(&tris_c[ss*eldim*gdim], (double*) &tris_flat_volume);

            supermesh_element_volumes_buf[elemH].push_back(std::sqrt(tris_flat_volume));

            // add intersection info to the intersection map: the cells of indices i, elemB and elemH intersect.
            intersection_map_buf[elemH].push_back(i);
        }

        if(n_tris_c > 0){
            if((islagrange) && (mA != eldim)){
                for(int ss = 0; ss < n_tris_c; ss++){
                    tris_c_dofs = map_dofs(reference_simplex_coor.data(), reference_dofs.data(), &tris_c[ss*eldim*gdim], gdim, mA);
                    for(unsigned int l = 0; l<mA; l++){
                        for(int k = 0; k<gdim; k++){
                            tris_c_alldofs[(ss*mA + l)*gdim + k] = tris_c_dofs[l*gdim + k];
                        }
                    }
                }
                mapped_tris_cA = cmapperA.map_coordinates(tris_c_alldofs, n_tris_c*mA);

            }else{
                mapped_tris_cA = cmapperA.map_coordinates(tris_c, n_tris_c*eldim);
            }

            ufc_feA->evaluate_reference_basis(evaluation_valuesA0, n_tris_c*mA, mapped_tris_cA.data());
            ufc_feA->transform_reference_basis_derivatives(evaluation_valuesA1, 0, n_tris_c*mA, evaluation_valuesA0, mapped_tris_cA.data(), nullptr, nullptr, nullptr, ufc_cellA.orientation);

            full_interpA_buf[elemH].insert(full_interpA_buf[elemH].end(), evaluation_valuesA1, &evaluation_valuesA1[mA*n_tris_c*mA]);
        }

        #ifdef DEBUG
            duration[2] += ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        #endif
    }
}

// cleaning up the memory
delete [] tri_a;
delete [] normalised_tri_a;
delete [] quad_h;
delete [] tris_c;
delete [] tris_c_alldofs;
delete [] tris_flat;
delete [] evaluation_valuesA0;
delete [] evaluation_valuesA1;

n_supermesh_cells = 0;
cells_in_haar_cell.resize(nelements_h);
for(std::size_t i=0;i<nelements_h; i++){
    cells_in_haar_cell[i] = supermesh_element_volumes_buf[i].size();
    n_supermesh_cells += cells_in_haar_cell[i];
}

intersection_map.reserve(n_supermesh_cells);
supermesh_element_volumes = Eigen::VectorXd::Zero(n_supermesh_cells);
full_interpA.reserve(mA*mA*n_supermesh_cells);

std::size_t count = 0;
for(std::size_t i=0;i<nelements_h; i++){
    intersection_map.insert(intersection_map.end(), intersection_map_buf[i].begin(), intersection_map_buf[i].end());
    full_interpA.insert(full_interpA.end(), full_interpA_buf[i].begin(), full_interpA_buf[i].end());
    for(std::size_t j=0;j<cells_in_haar_cell[i]; j++)
        supermesh_element_volumes(count + j) = supermesh_element_volumes_buf[i][j];

    count += cells_in_haar_cell[i];
}

n_haar_cells = nelements_h;

#ifdef DEBUG
    duration[3] += ( std::clock() - start2 ) / (double) CLOCKS_PER_SEC;
#endif

}

RowMatrixXd QMCWhiteNoiseConstructor::sample_white_noise_correction(Eigen::Ref<const RowMatrixXd> reference_cholesky, Eigen::Ref<const Eigen::VectorXd> reference_haar_vector, Eigen::Ref<const RowMatrixXd> r){

    // remember that Eigen vectors are column vectors
    // reference_cholesky must be eldim*eldim and r must be an eldim-by-n_supermesh_cells numpy array of pseudo-random numbers
    const std::size_t eldim = reference_haar_vector.rows();
    dolfin_assert(eldim == (std::size_t) reference_cholesky.cols());
    dolfin_assert(eldim == (std::size_t) reference_haar_vector.rows());

    RowMatrixXd z1 = (reference_cholesky*r)*supermesh_element_volumes.asDiagonal();

    std::vector<double> z0_aggr(n_haar_cells, 0.0);
    std::size_t n_cells_in_haar_cell;
    std::size_t count = 0;

    //FIXME: make sure the following does not need to be sqrt-ed
    const double inv_haar_cell_volume = 1.0/haar_cell_volume;

    for(std::size_t index=0; index<n_haar_cells; index++){

        n_cells_in_haar_cell = cells_in_haar_cell[index];
        z0_aggr[index] = z1.block(0, count, eldim, n_cells_in_haar_cell).sum();
        // squaring the volumes so that only multiplication by the interp matrices is needed later
        z1.block(0, count, eldim, n_cells_in_haar_cell) -= z0_aggr[index]*inv_haar_cell_volume*reference_haar_vector*(supermesh_element_volumes.segment(count, n_cells_in_haar_cell).cwiseProduct(supermesh_element_volumes.segment(count, n_cells_in_haar_cell))).transpose();

        count += n_cells_in_haar_cell;
    }

    return z1;
}

Eigen::VectorXd QMCWhiteNoiseConstructor::get_full_interp(){
        Eigen::VectorXd out = Eigen::Map<Eigen::VectorXd>(full_interpA.data(), full_interpA.size());
        return out;
}

Eigen::VectorXd QMCWhiteNoiseConstructor::get_supermesh_element_volumes(){
        return supermesh_element_volumes;
}

double QMCWhiteNoiseConstructor::get_haar_cell_volume(){
        return haar_cell_volume;
}

std::size_t QMCWhiteNoiseConstructor::get_n_haar_cells(){
        return n_haar_cells;
}

std::size_t QMCWhiteNoiseConstructor::get_haar_level(){
        return haar_level;
}

double QMCWhiteNoiseConstructor::get_domain_volume(){
        return haar_cell_volume*n_haar_cells;
}

int QMCWhiteNoiseConstructor::get_dim(){
        return _tdim;
}

int QMCWhiteNoiseConstructor::get_n_parent_meshes(){
        return n_parent_meshes;
}

Eigen::VectorXd QMCWhiteNoiseConstructor::get_haar_midpoints(){
        Eigen::VectorXd out = Eigen::Map<Eigen::VectorXd>(normalised_haar_cell_midpoints.data(), normalised_haar_cell_midpoints.size());
        return out;
}

VectorXi QMCWhiteNoiseConstructor::get_cells_in_haar_cell(){
        VectorXi out = Eigen::Map<VectorXi>(cells_in_haar_cell.data(), cells_in_haar_cell.size());
        return out;
}

VectorXi QMCWhiteNoiseConstructor::get_intersection_map(){
        VectorXi out = Eigen::Map<VectorXi>(intersection_map.data(), intersection_map.size());
        return out;
}

#ifdef DEBUG
    VectorXi QMCWhiteNoiseConstructor::get_n_intersection_candidates(){
            return Eigen::Map<VectorXi>(n_intersection_candidates.data(), n_intersection_candidates.size());
    }

    VectorXi QMCWhiteNoiseConstructor::get_n_actual_intersections(){
            return Eigen::Map<VectorXi>(n_actual_intersections.data(), n_actual_intersections.size());
    }

    Eigen::VectorXd QMCWhiteNoiseConstructor::get_duration(){
            return Eigen::Map<Eigen::VectorXd>(duration.data(), duration.size());
    }
#endif

PYBIND11_MODULE(SIGNATURE, m){

        //m.def("compute_intersection_candidates", [](std::vector<double> normalised_vertex_coordinates, const int dim, const std::size_t LH){return compute_intersection_candidates(normalised_vertex_coordinates.data(), dim, LH);});
        //m.def("intersect_vertical_segment", [](const double x_coor, const double y_min, const double y_max, const std::vector<double> normalised_vertex_coordinates, const int num_vertices, const std::size_t LH, const bool rotate_coordinates){return intersect_vertical_segment(x_coor, y_min, y_max, normalised_vertex_coordinates.data(), num_vertices, LH, rotate_coordinates);});
        m.def("hex_cell_volume", [](const Cell& hexahedron){return hex_cell_volume(hexahedron);});

        py::class_<QMCWhiteNoiseConstructor>
          (m, "QMCWhiteNoiseConstructor")
          .def(py::init([](py::object Va, const int Lh, std::vector<double> reference_simplex_coor, std::vector<double> reference_dofs)
          {
              auto _Va = Va.attr("_cpp_object").cast<std::shared_ptr<const FunctionSpace>>();
              return QMCWhiteNoiseConstructor(_Va, Lh, reference_simplex_coor, reference_dofs);
          
          }))
          .def("get_full_interp", &QMCWhiteNoiseConstructor::get_full_interp)
          .def("get_supermesh_element_volumes", &QMCWhiteNoiseConstructor::get_supermesh_element_volumes)
          .def("get_haar_midpoints", &QMCWhiteNoiseConstructor::get_haar_midpoints)
          .def("get_haar_cell_volume", &QMCWhiteNoiseConstructor::get_haar_cell_volume)
          .def("get_n_haar_cells", &QMCWhiteNoiseConstructor::get_n_haar_cells)
          .def("get_haar_level", &QMCWhiteNoiseConstructor::get_haar_level)
          .def("get_domain_volume", &QMCWhiteNoiseConstructor::get_domain_volume)
          .def("get_dim", &QMCWhiteNoiseConstructor::get_dim)
          .def("get_n_parent_meshes", &QMCWhiteNoiseConstructor::get_n_parent_meshes)
          .def("get_cells_in_haar_cell", &QMCWhiteNoiseConstructor::get_cells_in_haar_cell)
          #ifdef DEBUG
          .def("get_n_intersection_candidates", &QMCWhiteNoiseConstructor::get_n_intersection_candidates)
          .def("get_n_actual_intersections", &QMCWhiteNoiseConstructor::get_n_actual_intersections)
          .def("get_duration", &QMCWhiteNoiseConstructor::get_duration)
          #endif
          .def("get_intersection_map", &QMCWhiteNoiseConstructor::get_intersection_map)
          .def("sample_white_noise_correction", &QMCWhiteNoiseConstructor::sample_white_noise_correction);
}

}

'''

import numpy as np
from itertools import product

class HaarWhiteNoise(object):
    # Class for computing independent white noise realisations for QMC. This routine samples both truncated white noise W_L and its correction W_R (if required).
    def __init__(self, constructor, reference_haar_vector, reference_cholesky):
        # L is the Haar level, starting from 0, vol is the domain volume, dim is the dimension,
        # normalised_midpoints is an array of the midpoints of the Haar mesh cells, rescaled so that the coordinates are all between 0 and 1

        self.reference_haar_vector = reference_haar_vector
        self.reference_cholesky = reference_cholesky

        L = constructor.get_haar_level()
        self.L = L
        self.dim = constructor.get_dim()
        self.vol = constructor.get_domain_volume()
        self.normalised_midpoints = constructor.get_haar_midpoints().reshape((-1,self.dim))
        self.n_haar_mesh_cells = constructor.get_n_haar_cells()
        self.cells_in_haar_cell = constructor.get_cells_in_haar_cell().astype(np.int)
        self.cum_cells_in_haar_cell = np.concatenate([np.array((0,)), np.cumsum(self.cells_in_haar_cell)])
        self.supermesh_element_volumes = constructor.get_supermesh_element_volumes()
        self.inv_haar_cell_volume = 1.0/constructor.get_haar_cell_volume()

        oneD_haar_levels = np.arange(0, L+1) 
        self.haar_levels = np.vstack(sorted(product(oneD_haar_levels, repeat=self.dim), key = lambda x : sum(x)))
        self.reduced_haar_levels = np.maximum(self.haar_levels-1, 0)
        self.n_haar_levels = self.reduced_haar_levels.shape[0]

        # finds the index in haar_mesh indices such that n is the zero vector. This uses the fact that for each l, there are 2**|l^+|_1 wavelets
        zero_n_indices = np.concatenate([[0],np.cumsum(2**np.sum(self.reduced_haar_levels,1))])[:-1]

        # NOTE: in the following we compute the map between the QMC dimension and the corresponding Haar wavelet.
        # x bits for the ls and y bits for the n, overal (x+y)*dim bits needed
        Lbits = len(format(L, 'b'))

        tot_bits = self.dim*Lbits
        if tot_bits <= 8:
            kind = np.uint8
        elif tot_bits <= 16:
            kind = np.uint16
        elif tot_bits <= 32:
            kind = np.uint32
        elif tot_bits <= 64:
            kind = np.uint64
        else: raise ValueError("The level specified is too high for encoding the haar wavelet indices")

        def encode(l):
            b = l.flatten().astype(kind)
            s = b[0]
            for i in range(1,self.dim):
                s |= b[i] << kind(Lbits*i)
            return s

        inverse_map = dict(zip(map(encode, self.haar_levels), zero_n_indices))

        self.inverted_l = np.array([inverse_map[encode(l)] for l in self.haar_levels], dtype=np.int)

    def sample(self, z):

        assert len(z) == self.n_haar_mesh_cells

        r = np.zeros((self.n_haar_mesh_cells,))

        for i in range(self.n_haar_levels):
            l  = self.haar_levels[i,:]
            ll = self.reduced_haar_levels[i,:]
            # there are 2**l wavelets on level l, so floor(midpoint*2**(l-1)) gives the wavelet number (the l-1 is because here l starts from 0 instead of -1)
            n = (self.normalised_midpoints*(2**ll)).astype(np.int) # casting to int for non-negative numbers is equivalent to np.floor
            # there are 2**(l+1) Haar cells on level l, so floor(midpoint*2**l) gives the haar mesh number (the l is because here l starts from 0 instead of -1).
            # if the cell is even we have a plus sign, if it is odd we have a minus sign.
            nn = (self.normalised_midpoints*(2**l)).astype(np.int) # casting to int for non-negative numbers is equivalent to np.floor
            s = 1 - 2*(np.sum(nn,1) % 2) # equivalent to np.prod(1 - 2*(nn % 2), 1) (we just need to check the parity of the sum)

            # the following is the map between cells and QMC indices 
            # NOTE: the following is equivalent to idxs = np.array([get_index(l,b) for b in n], dtype=np.int)
            if self.dim == 1:
                idxs = self.inverted_l[i] + n[:,0]
            elif self.dim == 2:
                idxs = self.inverted_l[i] + n[:,1] + 2**max(l[1]-1,0) * n[:,0]
            else:
                idxs = self.inverted_l[i] + n[:,2] + 2**ll[2]*n[:,1] + 2**(ll[2] + ll[1]) * n[:,0]

            l_norm = np.sum(ll)
            r += (2**(l_norm/2)/np.sqrt(self.vol))*(s*z[idxs])

        out = np.outer(self.reference_haar_vector, np.repeat(r,self.cells_in_haar_cell)*(self.supermesh_element_volumes**2))

        return out

    def sample_correction(self, z):
        z1 = np.dot(self.reference_cholesky, z)*self.supermesh_element_volumes
        #z0_aggr = np.diff(np.append(np.cumsum(np.sum(z1,0)), np.array((0,)))[self.cum_cells_in_haar_cell-1])
        z0_aggr = np.array([z1[:,self.cum_cells_in_haar_cell[i]:self.cum_cells_in_haar_cell[i+1]].sum() for i in range(self.n_haar_mesh_cells)])
        z1 -= np.outer(self.reference_haar_vector*self.inv_haar_cell_volume, np.repeat(z0_aggr, self.cells_in_haar_cell)*(self.supermesh_element_volumes**2))
        return z1
    
if __name__ == "__main__":

    from dolfin import *
    from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs
    import os

    include_dirs = _libsupermesh_include_dirs
    library_dirs = _libsupermesh_library_dirs
    libraries = "supermesh"

    compiled_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-Og", "-g", "-Wall", "-Werror", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])
    #compiled_code = compile_cpp_code(QMC_supermesh_code, cppargs=["-O3", "-ffast-math", "-D DEBUG", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = [libraries])

    hex_cell_volume = compiled_code.hex_cell_volume
    QMCWhiteNoiseConstructor = compiled_code.QMCWhiteNoiseConstructor

    from scipy.linalg import solve_triangular
    import mshr
    dim = 2
    if dim == 1:
        mesh = IntervalMesh(103, 0, 1)
    elif dim == 2:
        mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 30)
        #mesh = BoxMesh.create([Point(0,0,0), Point(1,1,1)], [1,1,1], CellType.Type.tetrahedron)
    else:
        mesh = mshr.generate_mesh(mshr.Box(Point(0,0,0), Point(1,1,1)), 20)

    print("Mesh size: %d" % mesh.num_cells())

    vol = assemble(Constant(1.0)*dx(domain=mesh))

    V = FunctionSpace(mesh, "CG", 1)

    eldim = V.element().space_dimension()

    from FIAT import reference_element as re
    import ffc.fiatinterface as ft
    import itertools

    ref_el = np.vstack(re.ufc_simplex(mesh.topology().dim()).get_vertices()).flatten()
    fe = ft.create_element(V.ufl_element())
    reference_nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten()

    c = Cell(mesh,0)
    u = TrialFunction(V); v = TestFunction(V)
    M_loc = assemble_local(u*v*dx,c)/c.volume()
    H_loc = np.linalg.cholesky(M_loc)
    c_loc = assemble_local(v*dx,  c)/c.volume()

    # [7, 5, 4] are the max Lh in 1, 2 and 3 dimensions
    Lh = 3
    constructor = QMCWhiteNoiseConstructor(V, Lh, ref_el, reference_nodes)
    haar_noise = HaarWhiteNoise(constructor, c_loc, H_loc)

    n_haar_mesh_cells = constructor.get_n_haar_cells()

    full_interp = constructor.get_full_interp()

    volumes = constructor.get_supermesh_element_volumes()

    int_map = constructor.get_intersection_map()

    #candidates = constructor.get_n_intersection_candidates()

    #act_int = constructor.get_n_actual_intersections()

    #duration = constructor.get_duration()
    
    midpoints = constructor.get_haar_midpoints()

    haar_cell_volume = constructor.get_haar_cell_volume()

    n_supermesh_cells = volumes.shape[0]

    print("N supermesh cells / N finest mesh cells: %f" % (n_supermesh_cells/mesh.num_cells()))
    #print(duration)
    #assert (candidates.astype(np.int)-act_int.astype(np.int)).min() >= 0
    assert (int(full_interp.sum()) - n_supermesh_cells*len(c_loc)) == 0
    print(1 - (volumes**2).sum()/vol)
    assert abs((volumes**2).sum()/vol - 1) < 1.0e-14

    rng = np.random.RandomState(123456789)

    #print("\n---------------------------------- Time (s)")
    #print("Haar intersection candidates:      %f" % duration[0])
    #print("Haar intersection computation:     %f" % duration[1])
    #print("Interpolation factors computation: %f" % duration[2])
    #print("Total supermeshing time:           %f" % duration[3])
    #print("-------------------------------------------\n")

    #print(duration)

    # FIXME: we tested the coupled_haar_noise with 2-way supermeshes. It now needs to be tested with 3-way supermeshes and we need to implement the correction bits (I think)
    rng = np.random.RandomState(123456789)
    z = rng.randn(n_haar_mesh_cells)
    out1 = haar_noise.sample(z)
    z = rng.randn(eldim, n_supermesh_cells)
    out2 = constructor.sample_white_noise_correction(H_loc, c_loc, z)
    out3 = haar_noise.sample_correction(z)
    print(abs(out2-out3).max())

    out  = out1 + out2

    print(np.sum(out))
    
    print("TEST PASSED!")
