#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import MPI, FunctionSpace 
from petsc4py import PETSc
from sys import modules
from numpy import ndarray
import sys

class NullDevice():
    def write(self, s):
        pass

def to_levels(lst):
    # transform a list [a,b,c,d] to [(a,), (b,a), (c,b), (d,c)]
    levels = len(lst)
    return [(lst[0],)] + [(lst[l], lst[l-1]) for l in range(1, levels)]

class StochasticField(object):
    def __init__(self, V, stochastic_field = None, **kwargs):

        self.RNG = getattr(modules['femlmc'], '__RNG__')
        if self.RNG is None:
            raise RuntimeError("Before using femlmc a random number generator must be set with a call to setRNG")

        if not (isinstance(V, list) or isinstance(V, FunctionSpace)):
            raise ValueError("A function space or a list of function spaces must be provided\n")

        if isinstance(V, FunctionSpace):
            V = [V]
        elif not isinstance(V[0], FunctionSpace):
            raise ValueError("A function space or a list of function spaces must be provided\n")

        self.function_spaces = V
        self.stochastic_field = stochastic_field

        self.field_type = 'simple' # as opposed to 'mixed', see mixed_stochastic_field

        worldcomm = kwargs.get("worldcomm", MPI.comm_world).Dup()
        kwargs["worldcomm"] = worldcomm
        self.mpiRank = MPI.rank(worldcomm)
        self.mpiSize = MPI.size(worldcomm)
        self.worldComm = worldcomm

        self.qmc_flag = bool(kwargs.get("qmc", False))

        # private parameters needed for self.get_independent_field()
        self._independent = kwargs.get("independent", False)
        self._old_level = kwargs.get("old_level", None)

        self.params = kwargs

        self.setup_levels()

        self.setup()

    def _initialise_GQRNG(self):
        self.GQRNG = getattr(modules["femlmc"], '__GLOBAL_QRNG__')

    def sample(self, l):
        raise NotImplementedError

    def setup(self):
        pass

    def get_independent_parameters(self, l):
        indep_params = self.params.copy()
        for key in indep_params:
            if isinstance(indep_params[key], (list, tuple, ndarray)):
                if len(indep_params[key]) == self.n_levels:
                    indep_params[key] = [indep_params[key][l]]

        return indep_params

    def get_independent_field(self, l):
        # the following is just so that -1 maps to the largest level
        while l < 0:
            l += self.n_levels
        if l >= self.n_levels:
            raise ValueError('Level specified is larger than the current maximum level')

        # suppress annoying messages
        old_stdout = sys.stdout
        sys.stdout = NullDevice()
        indep_params = self.get_independent_parameters(l)
        indep_params["independent"] = True
        indep_params["old_level"] = l
        if self.stochastic_field is None:
            out = self.__class__(self.function_spaces[l], stochastic_field = None, **indep_params)
        else:
            out = self.__class__(self.function_spaces[l], stochastic_field = self.stochastic_field.get_independent_field(l), **indep_params)

        if out.qmc_flag:
            try: out.input_number = self.input_number
            except AttributeError:
                pass

        sys.stdout = old_stdout
        return out

    def setup_levels(self):
        # get the info needed from the function spaces

        self.n_levels = len(self.function_spaces)

        self.degrees      = []
        self.mesh_sizes   = []
        self.num_dofs     = []
        mesh_dims         = []

        for i in range(self.n_levels):
            #FIXME for mixed function spaces the degrees and dofs are weird
            self.degrees.append(self.function_spaces[i].ufl_element().degree())
            self.mesh_sizes.append(self.function_spaces[i].mesh().hmax())
            self.num_dofs.append(len(self.function_spaces[i].dofmap().dofs()))
            mesh_dims.append(self.function_spaces[i].mesh().num_cells())

        if not all([self.degrees[i] <= self.degrees[i+1] for i in range(self.n_levels-1)]):
            raise ValueError("The function space degree must never decrease with the level")
        if not all([mesh_dims[i] <= mesh_dims[i+1] for i in range(self.n_levels-1)]):
            raise ValueError("The number of mesh elements must never decrease with the level")

        if len(set(self.degrees)) == 1 and len(set(mesh_dims)) == 1:
            self.refinement_type = None
        elif len(set(self.degrees)) == len(self.degrees) and len(set(mesh_dims)) == 1: 
            self.refinement_type = 'p'
        elif len(set(self.degrees)) == 1 and len(set(mesh_dims)) == len(mesh_dims): 
            self.refinement_type = 'h'
        else:
            self.refinement_type = 'hp'

        del mesh_dims

        self.function_levels = to_levels(self.function_spaces)

        self.tdim = self.function_spaces[0].mesh().topology().dim()
        self.gdim = self.function_spaces[0].mesh().geometry().dim()
