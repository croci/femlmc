from dolfin import *
from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

from uqdolfin import _libsupermesh_include_dirs, _libsupermesh_library_dirs, StochasticField, to_levels, setRNG, getRNG
from numpy.random import RandomState
import numpy as np
from scipy.linalg import solve_triangular
from scipy.sparse import csr_matrix
from time import time
import os
from sys import modules

from white_noise_coupling import libsupermesh_code
include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
compiled_libsupermesh_code = compile_cpp_code(libsupermesh_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_libsupermesh_code = compile_cpp_code(libsupermesh_code, cppargs=["-O0", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
SuperMeshConstructor = compiled_libsupermesh_code.SuperMeshConstructor

class WhiteNoiseField(StochasticField):
    def setup(self):

        self.filepath = self.params.get("filepath", "supermesh_factors.p")

        self.us = [TrialFunction(V) for V in self.function_spaces]
        self.vs = [TestFunction(V)  for V in self.function_spaces]

        self.spaces_dimensions = [V.element().space_dimension() for V in self.function_spaces]

        self.couple_levels = self.params.get("couple_levels", True)

        if self.refinement_type in ["p", None] or self.couple_levels == False:

            self.factors = []
            self.n_cells = []
            self.cell_volumes = []
            self.mixed_factors = [None]
            for l in range(self.n_levels):
                mesh = self.function_spaces[l].mesh()
                self.n_cells.append(mesh.num_cells())
                self.cell_volumes.append(np.sqrt(np.array([c.volume() for c in cells(mesh)])))
                c = Cell(mesh, 0)
                self.factors.append(np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume()))

                if l > 0 and self.couple_levels == True:
                    mixed_mass_matrix = assemble_local(self.us[l-1]*self.vs[l]*dx, c)/c.volume()
                    self.mixed_factors.append(solve_triangular(self.factors[l], mixed_mass_matrices[l], lower=True).T)
        else:

            if self.mpiRank == 0:
                print("Generating Cholesky factors...")

            self.factors = []
            for l in range(self.n_levels):
                c = Cell(self.function_spaces[l].mesh(),0)
                self.factors.append(np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume()))

            self.interp_matrices   = [None]
            self.supermesh_element_volumes = [np.sqrt(np.array([c.volume() for c in cells(self.function_spaces[0].mesh())]))]
            self.sorting_indices = [None]
            intersection_maps    = [None]

            ref_el = np.vstack(re.ufc_simplex(self.function_spaces[0].mesh().topology().dim()).get_vertices()).flatten()
            finite_elements = [ft.create_element(V.ufl_element()) for V in self.function_spaces]
            reference_nodes = [np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten() for fe in finite_elements]

            if self.params.get("save_debug_info", False) == True:
                durations                 = [None]
                n_intersection_candidates = [None]
                n_actual_intersections    = [None]

            for l in range(1, self.n_levels):
                a = time()
                constructor = SuperMeshConstructor(self.function_spaces[l], self.function_spaces[l-1], ref_el, reference_nodes[l])
                self.interp_matrices.append((constructor.get_full_interp(0), constructor.get_full_interp(1)))
                intersection_maps.append(constructor.get_intersection_map().reshape((-1,2)))
                self.supermesh_element_volumes.append(constructor.get_supermesh_element_volumes())
                self.sorting_indices.append(np.argsort(self.supermesh_element_volumes[-1]))

                if self.params.get("save_debug_info", False) == True:
                    durations.append(constructor.get_duration())
                    n_intersection_candidates.append(constructor.get_n_intersection_candidates())
                    n_actual_intersections.append(constructor.get_n_actual_intersections())

                del constructor

                if self.mpiRank == 0:
                    print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

            if self.params.get("save_debug_info", False) == True:
                import hickle as hkl
                print("Process %d: Saving debug info..." % self.mpiRank)
                hkl.dump({'intersection_maps':intersection_maps, 'n_cells':self.n_cells, 'supermesh_element_volumes':self.supermesh_element_volumes, 'durations':self.durations, 'n_intersection_candidates' : self.n_intersection_candidates, 'n_actual_intersections':self.n_actual_intersections}, self.filepath, mode='w', compression='gzip', shuffle=True, compression_opts=9)

            self.n_cells = [self.function_spaces[0].mesh().num_cells()] + [len(item) for item in self.supermesh_element_volumes[1:]]

        if self.mpiRank == 0:
            print("Cholesky factors computed!")
            print("Generating assembly matrices...")

        # building the sparse boolean supermesh assembly matrices
        self.supermesh_assembly_matrices = []
        dofmaps = [self.function_spaces[l].dofmap() for l in range(self.n_levels)]
        for l in range(self.n_levels):
            a = time()
            if self.refinement_type in ["p", None] or self.couple_levels == False:
                # final size will be np.zeros((self.function_spaces[l].dim(), self.n_cells[l]*self.spaces_dimensions[l]))
                n = self.spaces_dimensions[l]*self.n_cells[l]
                fine_rows = np.concatenate([dofmaps[l].cell_dofs(i) for i in range(self.n_cells[l])])
                # there is exactly one nonzero entry per column and it is 1
                fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                coarse_mat = None

                if l > 0 and self.couple_levels == True:
                    ncoarse = self.spaces_dimensions[l-1]*self.n_cells[l]
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(i) for i in range(self.n_cells[l])])
                    coarse_mat = sp.csr_matrix((np.ones((ncoarse,),dtype='int8'), (coarse_rows, np.arange(ncoarse))), shape=(self.function_spaces[l-1].dim(), ncoarse), dtype='int8')

            else:
                if l == 0:
                    n = self.spaces_dimensions[l]*self.n_cells[l]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(i) for i in range(self.n_cells[l])])
                    fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                    coarse_mat = None

                else:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l]
                    fine_rows = []
                    coarse_rows = []
                    pairs = intersection_maps[l][self.sorting_indices[l],:]
                    for pair in pairs:
                        fine_rows.append(dofmaps[l].cell_dofs(pair[0]))
                        coarse_rows.append(dofmaps[l-1].cell_dofs(pair[1]))

                    fine_rows = np.concatenate(fine_rows)
                    coarse_rows = np.concatenate(coarse_rows)

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

            self.supermesh_assembly_matrices.append((fine_mat,coarse_mat))

            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        if self.mpiRank == 0:
            print("Assembly matrices computed!")

        self.use_qmc = self.params.get("use_qmc", False)
        if self.use_qmc is True:
            self.qmc_dims = [item for item in self.n_cells]

            if self._independent == False:
                try: 
                    QMC_dimensions = getattr(modules['uqdolfin'], '__QMC_DIMENSIONS__')
                except AttributeError:
                    QMC_dimensions = []

                self.input_number = len(QMC_dimensions)
                QMC_dimensions.append([len(QMC_dimensions), self.qmc_dims])
                setattr(modules["uqdolfin"], '__QMC_DIMENSIONS__', QMC_dimensions)
            else:
                self.input_number = None

            self.GQRNG = None

    #@profile
    def sample(self, l):
        # NOTE works only in serial (or with mpi_comm_self())

        self._initialise_GQRNG()

        xrnge = 2 - (l is 0 or self.couple_levels == False)

        Fs = [Function(V) for V in self.function_levels[l][:xrnge]]
        #Fs_arr = [Fs[i].vector().get_local() for i in range(xrnge)]
        Fs_arr = [0 for i in range(xrnge)]

        #dofmaps = [self.function_levels[l][i].dofmap() for i in range(xrnge)]

        if self.refinement_type in ["p", None] or self.couple_levels == False:

            if self.use_qmc is False:
                r = self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l])
            else:
                if self._independent == False:
                    r = self.GQRNG.sample(l, self.input_number)
                else:
                    r = self.GQRNG.sample(self._old_level, self.input_number)

            z1 = self.factors[l].dot(r)*self.cell_volumes[l]
            if l > 0 and self.couple_levels == True:
                # Apply inverse transpose cholesky factor to all elements in r. r gets overwritten so do not use it afterwards.
                z2 = self.mixed_factors[l].dot(r)*self.cell_volumes[l] # NOTE: if self.couple_levels == True, then the cell_volumes are constant across l

            Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1.T.flatten()) 

            if l > 0 and self.couple_levels == True:
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(z2.T.flatten()) 

            #for i,c in enumerate(cells(self.function_levels[l][0].mesh())):
            #    fine_dofs = dofmaps[0].cell_dofs(c.index())
            #    Fs_arr[0][fine_dofs] += z1[:,i] 

            #    if l > 0 and self.couple_levels == True:
            #        coarse_dofs = dofmaps[1].cell_dofs(c.index())
            #        Fs_arr[1][coarse_dofs] += z2[:,i]

        else:
            if l == 0:
                if self.use_qmc is False:
                    r = self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l])
                else:
                    if self._independent == False:
                        r = self.GQRNG.sample(l, self.input_number)
                    else:
                        r = self.GQRNG.sample(self._old_level, self.input_number)

                z1 = self.factors[l].dot(r)*self.supermesh_element_volumes[l]

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1.T.flatten()) 

                #for i,c in enumerate(cells(self.function_levels[l][0].mesh())):
                #    fine_dofs = dofmaps[0].cell_dofs(c.index())
                #    Fs_arr[0][fine_dofs] += z1[:,i]

            else:
                if self.use_qmc is False:
                    z = self.factors[l].dot(self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l]))*self.supermesh_element_volumes[l]
                else:
                    if self._independent == False:
                        z = self.factors[l].dot(self.GQRNG.sample(l, self.input_number))*self.supermesh_element_volumes[l]
                    else:
                        z = self.factors[l].dot(self.GQRNG.sample(self._old_level, self.input_number))*self.supermesh_element_volumes[l]

                mmax = self.spaces_dimensions[l]
                mmin = self.spaces_dimensions[l-1]
                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                z1 = np.einsum('ijk,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), z)
                z2 = np.einsum('ijk,ki->ji', self.interp_matrices[l][1].reshape((self.n_cells[l],mmin,mmax)), z)
                #assert all(np.isfinite(z1.flatten()))
                #assert all(np.isfinite(z2.flatten()))

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1[:,self.sorting_indices[l]].T.flatten()) 
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(z2[:,self.sorting_indices[l]].T.flatten()) 

                #for i in self.sorting_indices[l]:
                #    pair = self.intersection_maps[l][i]
                #    fine_dofs = dofmaps[0].cell_dofs(pair[0])
                #    coarse_dofs = dofmaps[1].cell_dofs(pair[1])

                #    Fs_arr[0][fine_dofs] += z1[:,i]
                #    Fs_arr[1][coarse_dofs] += z2[:,i]

        Fs[0].vector()[:] = Fs_arr[0]
        if l > 0 and self.couple_levels == True:
            Fs[1].vector()[:] = Fs_arr[1]

        return Fs


if __name__ == "__main__":
    from mkl_sobol import *

    mpiRank = MPI.rank(MPI.comm_world)
    RNG = MKL_RNG(seed=mpiRank)
    #RNG = np.random.RandomState(mpiRank)
    setRNG(RNG)

    nested_hierarchy = False
    k = 1
    dim = 2
    L = [9, 5][dim-2]
    n_levels = L + 1
    buf = 0

    # Matern field parameters
    matern_parameters = {"lmbda"    : 0.25,
                         "avg"      : 0.0,
                         "sigma"    : 1.0,
                         "nu"       : 2*k-dim/2,
                         "lognormal_scaling" : False}

    if nested_hierarchy:
        if dim == 3:
            # 3D
            outer_meshes = [BoxMesh(MPI.comm_self, Point(-1,-1,-1), Point(1,1,1), 2**(l+1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
            inner_meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
        elif dim == 2:
            # 2D
            outer_meshes = [RectangleMesh(MPI.comm_self, Point(-1,-1), Point(1,1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
            inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
    else:

        h5 = HDF5File(MPI.comm_self, "/scratch/croci/white_noise_paper_hierarchies/%dD_hierarchy.h5" % dim, "r")


        inner_meshes = [Mesh(MPI.comm_self) for i in range(L)]
        outer_meshes = [Mesh(MPI.comm_self) for i in range(L)]

        [h5.read(inner_meshes[i], "/inner_mesh%d" % i, False) for i in range(L)]
        [h5.read(outer_meshes[i], "/outer_mesh%d" % i, False) for i in range(L)]

        h5.close()

    inner_spaces = [FunctionSpace(mesh, "CG", 1) for mesh in inner_meshes]
    outer_spaces = [FunctionSpace(mesh, "CG", 1) for mesh in outer_meshes]

    WN = WhiteNoiseField(outer_spaces, qmc=0, couple_levels = not nested_hierarchy)

    WN.sample(L-1)

    #N = 11
    #times = np.zeros((L,))
    #for l in range(L):
    #    for i in range(N):
    #        temp = time()
    #        WN.sample(l)
    #        temp2 = time()
    #        if i > 0: times[l] += temp2 - temp

    #times /= (N-1)
    #print(times)
