#######################################################################
#
# This file is part of UQdolfin
#
# UQdolfin is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from FIAT import reference_element as re
import ufl, ffc
import itertools
import numpy as np

elem_type = "CG"
dim = 3
degree = 3
sdim = dim + 1

#simplex_coor = re.default_simplex(dim).vertices
#simplex_coor = re.ufc_simplex(dim).vertices
simplex_coor = np.random.randn(sdim, dim)

editor = MeshEditor()
mesh = Mesh()
editor.open(mesh, str(ufl.cell.simplex(dim)), dim, dim)
editor.init_vertices(sdim)
editor.init_cells(1)
[editor.add_vertex(i, vertex) for i,vertex in enumerate(simplex_coor)]
editor.add_cell(0, np.arange(sdim).astype(np.uintp))
editor.close()

V = FunctionSpace(mesh, elem_type, degree)
dof_coor  = V.tabulate_dof_coordinates().reshape((-1,dim))
cell_dofs = dof_coor[V.dofmap().cell_dofs(0),:]

# NOTE: currently dolfin uses ufc_simplex rather than FIAT default simplex. The dofs order is different if default_simplex is used.
#ref_el = re.default_simplex(dim)
ref_el = re.ufc_simplex(dim)
A,b = re.make_affine_mapping(ref_el.vertices, simplex_coor)

e = ufl.FiniteElement(elem_type, ufl.cell.simplex(dim), degree)
fe = ffc.fiatinterface.create_element(e)
nodes = np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis()))))

mapped_nodes = A.dot(nodes.T).T + b

assert np.allclose(mapped_nodes, cell_dofs)

import os

from white_noise_coupling import libsupermesh_code
compiled_libsupermesh_code = compile_extension_module(code = libsupermesh_code, cppargs=["-fpermissive", "-O3", "-ffast-math", "-march=native"], include_dirs = [os.path.expanduser("~/install-scripts/dolfin/fenics-dev-20180319/src/supermesh-dolfin/libsupermesh-c")], library_dirs = [os.path.expanduser("~/install-scripts/dolfin/fenics-dev-20180319/src/supermesh-dolfin/libsupermesh-c")], libraries = ["supermesh-c"])
make_affine = compiled_libsupermesh_code.make_affine_mapping
map_dofs = compiled_libsupermesh_code.map_dofs

x = np.vstack(ref_el.vertices).flatten()
y = simplex_coor.flatten()

#make_affine(x,y,dim)
#
#print(" ")
#print(A)
#print(b)

mapped_dofs = map_dofs(x, nodes.flatten(), y, dim, nodes.shape[0])

assert np.allclose(mapped_nodes, mapped_dofs.reshape(mapped_nodes.shape))
