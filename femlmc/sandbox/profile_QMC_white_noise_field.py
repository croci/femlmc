from dolfin import *
from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

from uqdolfin import _libsupermesh_include_dirs, _libsupermesh_library_dirs, StochasticField, to_levels, setRNG, getRNG, setQRNG
import numpy as np
from scipy.linalg import solve_triangular
from sys import modules
from scipy.sparse import csr_matrix
from time import time
import os

from QMC_white_noise_coupling   import QMC_code, HaarWhiteNoise
from MLQMC_white_noise_coupling import MLQMC_code, CoupledHaarWhiteNoise
include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
compiled_MLQMC_code = compile_cpp_code(MLQMC_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
compiled_QMC_code = compile_cpp_code(QMC_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_QMC_code = compile_cpp_code(QMC_code, cppargs=["-Og", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_MLQMC_code = compile_cpp_code(MLQMC_code, cppargs=["-Og", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
QMCWhiteNoiseConstructor   = compiled_QMC_code.QMCWhiteNoiseConstructor
MLQMCWhiteNoiseConstructor = compiled_MLQMC_code.MLQMCWhiteNoiseConstructor
        
class QMCWhiteNoiseField(StochasticField):
    def setup(self):

        self.filepath = self.params.get("filepath", "supermesh_debug_info.hkl")

        self.us = [TrialFunction(V) for V in self.function_spaces]
        self.vs = [TestFunction(V)  for V in self.function_spaces]

        self.spaces_dimensions = [V.element().space_dimension() for V in self.function_spaces]

        self.couple_levels = self.params.get("couple_levels", True)

        if self.refinement_type in ["p", "hp"]:
            if self.mpiRank == 0:
                raise NotImplementedError("MLQMC and QMC not implemented with p-refinement.")

        #FIXME: We need to figure out how to set Lh as a function of the level. This might be determined automatically or a numpy array might be given
        gdim = self.function_spaces[0].mesh().geometry().dim()
        tdim = self.function_spaces[0].mesh().topology().dim()
        vol = np.sum([c.volume() for c in cells(self.function_spaces[0].mesh())])

        desired_Lh = [7, 5, 4][gdim-1] # Desired level according to the dimension so that we never need more than 128 QMC points

        ## the following is the maximum level needed given the finite element mesh size (if the mesh is coarse enough, there is no need for a fine Haar mesh)
        #k = self.params.get("n_solves", None)
        ##FIXME if k is None, then make sure that we correct
        ## if p_factor is chosen to be 1, then the rates should be the same. The value below yields a higher rate wrt to the FEM mesh size
        ## (but in this case the Haar mesh is finer than the FEM mesh and the rate with respect to the Haar mesh size is the usual rate)
        #p_factor = 4*k/(2*min(1,2*k-tdim/2) + tdim) # the dependency wrt to k can be made better if higher degree wavelets are chosen
        #max_Lh_needed = np.ceil(np.log2(vol**(1/tdim)/(np.array(self.mesh_sizes)**p_factor))).astype(np.int)
        max_Lh_needed = np.ceil(np.log2(vol**(1/tdim)/(np.array(self.mesh_sizes)))).astype(np.int)

        wanted_Lh = self.params.get("Lh", None)
        if wanted_Lh is None:
            #Lh = desired_Lh*np.ones_like(max_Lh_needed)
            #Lh = np.minimum(desired_Lh, max(max_Lh_needed)*np.ones_like(max_Lh_needed))
            Lh = max_Lh_needed.copy()
            #Lh = np.arange(self.n_levels)
            #Lh = np.minimum(desired_Lh, np.maximum(max_Lh_needed,0))

        else:
            #Lh = np.minimum(desired_Lh, np.maximum(wanted_Lh, 0))
            Lh = np.maximum(wanted_Lh, 0)

        self.correction_needed = (max_Lh_needed > Lh)
        self.correction_needed = np.ones_like(self.correction_needed).astype(np.bool)
        #self.correction_needed[:2] = False
        #self.correction_needed = np.zeros_like(self.correction_needed).astype(np.bool)

        if gdim == 1:
            self.qmc_dims = 2**Lh
        if gdim == 2:
            self.qmc_dims = (2.**(Lh-1)*(Lh+2)).astype(np.int)
        if gdim == 3:
            self.qmc_dims = (2.**(Lh-3)*(Lh**2 + 7*Lh + 8)).astype(np.int)

        self.n_haar_mesh_cells = 2**(Lh*tdim)
        self.haar_cell_volumes = vol/self.n_haar_mesh_cells

        assert (self.n_haar_mesh_cells - self.qmc_dims).min() >= 0

        if self._independent == False:
            try: 
                QMC_dimensions = getattr(modules['uqdolfin'], '__QMC_DIMENSIONS__')
            except AttributeError:
                QMC_dimensions = []

            self.input_number = len(QMC_dimensions)
            QMC_dimensions.append([len(QMC_dimensions), self.qmc_dims])
            setattr(modules["uqdolfin"], '__QMC_DIMENSIONS__', QMC_dimensions)
        else:
            self.input_number = None

        self.GQRNG = None

        self.factors = []
        for l in range(self.n_levels):
            c = Cell(self.function_spaces[l].mesh(),0)
            ref_H = np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume())
            ref_c = assemble_local(self.vs[l]*dx, c)/c.volume()
            self.factors.append((ref_H, ref_c))

        self.constructors = []

        self.interp_matrices   = []
        intersection_maps = []
        self.supermesh_element_volumes = []
        self.sorting_indices = []
        self.haar_white_noises = []

        ref_el = np.vstack(re.ufc_simplex(tdim).get_vertices()).flatten()
        finite_elements = [ft.create_element(V.ufl_element()) for V in self.function_spaces]
        reference_nodes = [np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten() for fe in finite_elements]

        if self.params.get("save_debug_info", False) == True:
            durations                 = [None]
            n_intersection_candidates = [None]
            n_actual_intersections    = [None]

        for l in range(self.n_levels):
            a = time()
            if l == 0 or self.correction_needed[l] == False or self.couple_levels == False:
                constructor = QMCWhiteNoiseConstructor(self.function_spaces[l], Lh[l], ref_el, reference_nodes[l])
                self.interp_matrices.append([constructor.get_full_interp(), None])
                intersection_maps.append([constructor.get_intersection_map(), None])
            else:
                constructor = MLQMCWhiteNoiseConstructor(self.function_spaces[l], self.function_spaces[l-1], Lh[l], ref_el, reference_nodes[l])
                self.interp_matrices.append([constructor.get_full_interp(i) for i in range(2)])
                intersection_maps.append([constructor.get_intersection_map(i) for i in range(2)])

            self.supermesh_element_volumes.append(constructor.get_supermesh_element_volumes())
            self.sorting_indices.append(np.argsort(self.supermesh_element_volumes[-1]))

            if l == 0 or self.couple_levels == False:
                self.haar_white_noises.append(HaarWhiteNoise(constructor, ref_c, ref_H))
            else:
                self.haar_white_noises.append(CoupledHaarWhiteNoise(constructor, self.constructors[-1], ref_c, ref_H))

            if self.params.get("save_debug_info", False) == True:
                durations.append(constructor.get_duration())
                n_intersection_candidates.append(constructor.get_n_intersection_candidates())
                n_actual_intersections.append(constructor.get_n_actual_intersections())

            self.constructors.append(constructor)

            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        self.n_cells = [len(item) for item in self.supermesh_element_volumes]

        if self.params.get("save_debug_info", False) == True:
            import hickle as hkl
            print("Process %d: Saving debug info..." % self.mpiRank)
            hkl.dump({'intersection_maps':intersection_maps, 'n_cells':self.n_cells, 'supermesh_element_volumes':self.supermesh_element_volumes, 'durations':self.durations, 'n_intersection_candidates' : self.n_intersection_candidates, 'n_actual_intersections':self.n_actual_intersections}, self.filepath, mode='w', compression='gzip', shuffle=True, compression_opts=9)


        if self.mpiRank == 0:
            print("Cholesky factors computed!")
            print("Generating assembly matrices...")

        # building the sparse boolean supermesh assembly matrices
        self.supermesh_assembly_matrices = []
        dofmaps = [self.function_spaces[l].dofmap() for l in range(self.n_levels)]
        for l in range(self.n_levels):
            a = time()
            if l == 0 or self.couple_levels == False:
                n = self.spaces_dimensions[l]*self.n_cells[l]
                elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                coarse_mat = None

            else:
                if self.correction_needed[l] == True:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l]

                    elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                    elemsB = intersection_maps[l][1][self.sorting_indices[l]]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(elem) for elem in elemsB])

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

                else:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l-1]

                    elemsA = intersection_maps[l][0][self.sorting_indices[l]]
                    elemsB = intersection_maps[l-1][0][self.sorting_indices[l-1]]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(elem) for elem in elemsA])
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(elem) for elem in elemsB])

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

            #self.intersection_maps = intersection_maps
            self.supermesh_assembly_matrices.append((fine_mat,coarse_mat))
            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        self.worldComm.barrier()

        if self.mpiRank == 0:
            print("Assembly matrices computed!")
            print("QMC levels: %s, Max levels needed %s" % (Lh,max_Lh_needed))


    #@profile
    def sample(self, l):

        #FIXME: NEED TO RECOVER THE QRNG AFTER IT HAS BEEN SETUP BY THE MLMC_SAMPLER
        # NOTE works only in serial (or with mpi_comm_self())
        self._initialise_GQRNG()
        if self.GQRNG is None:
            from quasi_random_number_generator import GlobalQRNG
            GQRNG = GlobalQRNG(worldcomm = self.worldComm)
            GQRNG._setUp()
            setattr(modules["uqdolfin"], '__GLOBAL_QRNG__', GQRNG)
            self._initialise_GQRNG()


        xrnge = 2 - (l is 0)

        Fs = [Function(V) for V in self.function_levels[l]]
        #Fs_arr = [Fs[i].vector().get_local() for i in range(xrnge)]
        Fs_arr = [0 for i in range(xrnge)]

        #dofmaps = [self.function_levels[l][i].dofmap() for i in range(xrnge)]

        if l == 0 or self.couple_levels == False:

            m = self.spaces_dimensions[l]

            if self._independent == False:
                r_qmc = self.GQRNG.sample(l, self.input_number)
            else:
                r_qmc = self.GQRNG.sample(self._old_level, self.input_number)

            if self.n_haar_mesh_cells[l] > self.qmc_dims[l]:
                r_qmc = np.concatenate([r_qmc, self.RNG.randn(self.n_haar_mesh_cells[l] - self.qmc_dims[l])])

            #assert all(np.isfinite(r_qmc.flatten()))

            r_qmc = self.haar_white_noises[l].sample(r_qmc)

            if self.correction_needed[l] == True:
                r_mc = self.RNG.randn(m, self.n_cells[l])
                r_mc = self.haar_white_noises[l].sample_correction(r_mc)

                r = r_qmc + r_mc
            else:
                r = r_qmc

            z1  = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],m,m)), r)

            #assert all(np.isfinite(z1.flatten()))

            Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1[:,self.sorting_indices[l]].T.flatten())

            ## we add together all the supermesh element contributions
            ## from smaller to larger supermesh element volume for numerical stability
            #for i in self.sorting_indices[l]:
            #    elemA = self.intersection_maps[l][0][i]

            #    fine_dofs = dofmaps[0].cell_dofs(elemA)
            #    Fs_arr[0][fine_dofs] += z1[:,i]

        else:
            mmax = self.spaces_dimensions[l]
            mmin = self.spaces_dimensions[l-1]

            if self._independent == False:
                r_qmc = self.GQRNG.sample(l, self.input_number)
            else:
                r_qmc = self.GQRNG.sample(self._old_level, self.input_number)

            if self.n_haar_mesh_cells[l] > self.qmc_dims[l]:
                r_qmc = np.concatenate([r_qmc, self.RNG.randn(self.n_haar_mesh_cells[l] - self.qmc_dims[l])])

            #try: assert all(np.isfinite(r_qmc.flatten()))
            #except AssertionError:
            #    print((len(r_qmc.flatten()), sum(np.isfinite(r_qmc.flatten()) == False)))

            # delta_W not needed (see below)
            WLf, WLc = self.haar_white_noises[l].sample(r_qmc)

            #assert all(np.isfinite(WLf.flatten()))
            #assert all(np.isfinite(WLc.flatten()))

            if self.correction_needed[l] == True:
                r_mc = self.RNG.randn(mmax, self.n_cells[l])
                WRf = self.haar_white_noises[l].sample_correction(r_mc)
                #assert all(np.isfinite(WRf.flatten()))

                Wf = WLf + WRf

                if self.correction_needed[l-1] == True:
                    # NOTE: if correction is needed we are using a 3-way supermesh
                    #       and Wc as written in the line below is the same as Wf
                    #Wc = WLc + delta_W + WRf # (since WLc + delta_W = WLf and delta_W + WRf = WRc)
                    Wc = Wf.copy()
                else:
                    Wc = WLc

                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                Wf = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), Wf)
                Wc = np.einsum('ikj,ki->ji', self.interp_matrices[l][1].reshape((self.n_cells[l],mmin,mmax)), Wc)

                #assert all(np.isfinite(Wf.flatten()))
                #assert all(np.isfinite(Wc.flatten()))

                ## we add together all the supermesh element contributions
                ## from smaller to larger supermesh element volume for numerical stability
                #for i in self.sorting_indices[l]:
                #    elemA = self.intersection_maps[l][0][i]
                #    elemB = self.intersection_maps[l][1][i]

                #    fine_dofs = dofmaps[0].cell_dofs(elemA)
                #    Fs_arr[0][fine_dofs] += Wf[:,i]

                #    coarse_dofs = dofmaps[1].cell_dofs(elemB)
                #    Fs_arr[1][coarse_dofs] += Wc[:,i]

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(Wf[:,self.sorting_indices[l]].T.flatten())
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(Wc[:,self.sorting_indices[l]].T.flatten())

            else:

                Wf = WLf
                Wc = WLc

                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                Wf = np.einsum('ikj,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), Wf)
                Wc = np.einsum('ikj,ki->ji', self.interp_matrices[l-1][0].reshape((self.n_cells[l-1],mmin,mmin)), Wc)

                #assert all(np.isfinite(Wf.flatten()))
                #assert all(np.isfinite(Wc.flatten()))

                ## we add together all the supermesh element contributions
                ## from smaller to larger supermesh element volume for numerical stability
                #for i in self.sorting_indices[l]:
                #    elemA = self.intersection_maps[l][0][i]

                #    fine_dofs = dofmaps[0].cell_dofs(elemA)
                #    Fs_arr[0][fine_dofs] += Wf[:,i]

                #for i in self.sorting_indices[l-1]:
                #    elemB = self.intersection_maps[l-1][0][i]
                #    coarse_dofs = dofmaps[1].cell_dofs(elemB)
                #    Fs_arr[1][coarse_dofs] += Wc[:,i]

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(Wf[:,self.sorting_indices[l]].T.flatten())
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(Wc[:,self.sorting_indices[l-1]].T.flatten())

        Fs[0].vector()[:] = Fs_arr[0]
        if l > 0 and self.couple_levels == True:
            Fs[1].vector()[:] = Fs_arr[1]

        return Fs

if __name__ == "__main__":
    from mkl_sobol import *

    mpiRank = MPI.rank(MPI.comm_world)
    RNG = MKL_RNG(seed=mpiRank)
    setRNG(RNG)
    setQRNG(MKL_SOBOL_RNG)

    nested_hierarchy = True
    k = 1
    dim = 3
    L = [9, 5][dim-2]
    #L -= 1
    n_levels = L + 1
    buf = 0
    Lh = np.arange(2+buf, L+2+buf)
    Lh = 4*np.ones_like(Lh)

    # Matern field parameters
    matern_parameters = {"lmbda"    : 0.25,
                         "avg"      : 0.0,
                         "sigma"    : 1.0,
                         "nu"       : 2*k-dim/2,
                         "lognormal_scaling" : False}

    h5 = HDF5File(MPI.comm_self, "/scratch/croci/white_noise_paper_hierarchies/%dD_hierarchy.h5" % dim, "r")

    inner_meshes = [Mesh(MPI.comm_self) for i in range(L)]
    outer_meshes = [Mesh(MPI.comm_self) for i in range(L)]

    [h5.read(inner_meshes[i], "/inner_mesh%d" % i, False) for i in range(L)]
    [h5.read(outer_meshes[i], "/outer_mesh%d" % i, False) for i in range(L)]

    h5.close()

    inner_spaces = [FunctionSpace(mesh, "CG", 1) for mesh in inner_meshes]
    outer_spaces = [FunctionSpace(mesh, "CG", 1) for mesh in outer_meshes]

    print([mesh.num_cells() for mesh in outer_meshes])
    import sys; sys.exit(0)

    WN = QMCWhiteNoiseField(outer_spaces, qmc=1, n_solves = k-dim/2. + dim/4., Lh = Lh, couple_levels = not nested_hierarchy)

    #WN.sample(L-1)

    N = 11
    times = np.zeros((L,))
    for l in range(L):
        for i in range(N):
            temp = time()
            WN.sample(l)
            temp2 = time()
            if i > 0: times[l] += temp2 - temp

    times /= (N-1)
    print(times)
