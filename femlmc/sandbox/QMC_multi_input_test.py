#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from petsc4py import PETSc
from femlmc import *
from mkl_sobol import *
import numpy as np
import numpy.random as npr
import sys

mpiRank = MPI.rank(MPI.comm_world)
mpiSize = MPI.size(MPI.comm_world)

class TestFunctional(OutputFunctional):
    def evaluate(self, V, sample = None, rv_sample = None, level_info = (None,None)):

        opts   = {"p_ksp_type" : "cg",
                  "p_ksp_atol" : 1.0e-10,
                  "p_ksp_rtol" : 1.0e-15,
                  "p_ksp_max_it" : 100,
                  "p_ksp_norm_typex" : "unpreconditioned",
                  "p_pc_factor_mat_solver_package" : "mumps",
                  "p_ksp_converged_reasonx" : None,
                  "p_ksp_monitor_true_residualx" : None,
                  "p_ksp_viewx" : None,
                  "p_pc_type" : "hypre",
                  "p_pc_hypre_type" : "boomeramg"}

        petsc_opts = PETSc.Options() 

        for key in opts:
            petsc_opts[key] = opts[key]

        u = TrialFunction(V)
        v = TestFunction(V)

        out = Function(V)

        lhs = inner(exp(sample[0]+sample[1])*grad(u), grad(v))*dx
        rhs = Constant(rv_sample[0]/2+rv_sample[1]/2)*v*dx

        bcs = DirichletBC(u.function_space(), Constant(0.0), 'on_boundary')

        system = assemble_system(lhs,rhs,bcs)
        L = as_backend_type(system[0]).mat()
        b = as_backend_type(system[1]).vec()

        ksp = PETSc.KSP().create(V.mesh().mpi_comm())
        ksp.setOperators(L)
        ksp.setOptionsPrefix("p_")
        ksp.setFromOptions()

        y = b.duplicate()
        ksp.solve(b, y)

        out.vector()[:] = y.getArray()

        E = assemble(out*out*dx) # inner_vol is one here
        #E = assemble(sample*sample*dx) #*2**(V.mesh().topology().dim()) # inner_vol is one here
        #E = assemble((abs(sample)+Constant(1.0))**(-3.5)*dx) # *2**(V.mesh().topology().dim()) # inner_vol is one here

        return [E]

    def get_n_outputs(self):
        return 1

if __name__ == '__main__':

    k   = 1
    dim = 2
    qmc_flag = 1
    coupled = True
    estimate_parameters = False
    options = ["mlqmc_test", "qmc_convergence_test", "mlqmc_run"]
    selected_option = options[1]

    if selected_option == "qmc_convergence_test":
        n_qmc_shifts = 128
    else:
        n_qmc_shifts = 32

    ## Matern field parameters
    #matern_parameters = {"lmbda"    : 0.25,
    #                     "avg"      : 0.0,
    #                     "sigma"    : 1.0,
    #                     "nu"       : 2*k - dim/2,
    #                     "lognormal_scaling" : False}

    # Matern field parameters
    matern_parameters = {"lmbda"    : 0.25,
                         "avg"      : 1.0,
                         "sigma"    : 0.2,
                         "nu"       : 2*k - dim/2,
                         "lognormal_scaling" : True}

    N0 = 10 # initial samples on coarse levels

    try: filename = sys.argv[2]
    except IndexError: filename = "mc_test.txt"
    try: N = int(sys.argv[1])
    except IndexError: N = 1 
    if filename[-4:] != ".txt":
        filename += ".txt"

    assert N > 0

    buf = 2
    counterbuf = buf*int(selected_option == "mlqmc_test")
    n_levels  = 10 - [1, 5, 5][dim-1] - counterbuf
    #RNG       = npr.RandomState(mpiRank)
    RNG = MKL_RNG(seed=mpiRank)
    setRNG(RNG)
    setQRNG(MKL_SOBOL_RNG, M=n_qmc_shifts)

    if dim == 3:
        # 3D
        outer_meshes = [BoxMesh(MPI.comm_self, Point(-1,-1,-1), Point(1,1,1), 2**(l+1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
    elif dim == 2:
        # 2D
        outer_meshes = [RectangleMesh(MPI.comm_self, Point(-1,-1), Point(1,1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
        #outer_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
        #inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.25,-0.25), Point(0.25,0.25), 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
    else:
        # 1D
        outer_meshes = [IntervalMesh(MPI.comm_self, 2**(l+1), -1.0, 1.0) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.5, 0.5) for l in range(buf, n_levels+buf)][1:]
        #outer_meshes = [IntervalMesh(MPI.comm_self, 2**(l+1), -0.5, 0.5) for l in range(buf, n_levels+buf)][1:]
        #inner_meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.25, 0.25) for l in range(buf, n_levels+buf)][1:]

    #outer_meshes = [outer_meshes[0] for l in range(n_levels-1)]
    #inner_meshes = [inner_meshes[0] for l in range(n_levels-1)]

    # h-ref
    outer_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in outer_meshes]
    inner_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in inner_meshes]

    ## p-ref
    #outer_spaces = [FunctionSpace(outer_meshes[2], 'CG', l+1) for l in range(n_levels)]
    #inner_spaces = [FunctionSpace(inner_meshes[2], 'CG', l+1) for l in range(n_levels)]
    
    #matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag)
    #matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag, Lh = [2])
    #matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag, Lh = [3]*(n_levels-1))
    #matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag, n_qmc_shifts = n_qmc_shifts, Lh = np.minimum(15, 2 + buf + np.arange(n_levels-1)))
    matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag, Lh = np.minimum(12, 2 + buf + np.arange(n_levels-1)), nested_inner_outer = True, nested_hierarchy = False)
    matern_field2 = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, qmc = qmc_flag, Lh = np.minimum(12, 2 + buf + np.arange(n_levels-1)), nested_inner_outer = True, nested_hierarchy = False)
    #matern_field2 = matern_field.copy()
    mlmc_sampler = MLMCSampler(inner_spaces, stochastic_field = MixedStochasticField([matern_field,matern_field2]), random_variable = MixedRandomVariable([NormalRandomVariable(qmc=qmc_flag), NormalRandomVariable(qmc=qmc_flag)]), output_functional = TestFunctional())

    Eps = list(5.0e-5/2**np.arange(5))
    Eps = list(5.0e-5/2**np.arange(7))

    logfile = open(filename, 'w')

    if selected_option == "mlqmc_test":
        if estimate_parameters == False:
            mlmc_sampler.run_mlqmc_test(Eps = Eps, logfile = logfile, alpha = 2, gamma = dim, debug = False)
        else:
            mlmc_sampler.run_mlqmc_test(Eps = Eps, logfile = logfile, gamma = dim, N = N, debug = False)

    elif selected_option == "qmc_convergence_test":
        if N >= 32:
            if coupled == False:
                for i in range(len(outer_spaces)):
                    mlmc_sampler.run_qmc_convergence_test(N, l = i, p0=0, logfile = logfile)
            else:
                # still check convergence of level 0 even if it is uncoupled
                for i in range(0,len(outer_spaces)):
                    mlmc_sampler.run_mlqmc_convergence_test(N, l = i, p0=0, logfile = logfile)

    elif selected_option == "mlqmc_run":
        P, Nl, Cl = mlqmc(mlmc_sampler, Eps[-1], alpha = 2, gamma = dim, logfile = logfile)
        if mpiRank == 0:
            print((P, Nl, Cl))
