#######################################################################
#
# This file is part of UQdolfin
#
# UQdolfin is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
import numpy as np
import itertools as itt

a = 0.0
b = 1.0

dim = 2
L = 6

vol = (b-a)**dim

print("creating the mesh...")

if dim == 2:
    mesh = RectangleMesh.create(MPI.comm_world, [Point(a,a), Point(b,b)], [2**(L+1), 2**(L+1)], CellType.Type.quadrilateral)
elif dim == 3:
    mesh = BoxMesh.create(MPI.comm_world, [Point(a,a,a), Point(b,b,b)], [2**(L+1), 2**(L+1), 2**(L+1)], CellType.Type.hexahedron)
else:
    mesh = IntervalMesh(MPI.comm_world, 2**(L+1), a, b)

num_cells = 2**(dim*(L+1))
assert num_cells == mesh.num_cells()

print("creating the haar indices...")

oneD_haar_indices = np.zeros((2**(L+1), 2))
oneD_haar_levels = np.arange(-1, L+1) + 1

count = 0
for l in range(-1, L+1):
    for n in range(0, max(2**l - 1, 0) + 1):
        oneD_haar_indices[count, 0] = l + 1 #NOTE for the l1 norm we need to switch to the L_min = 0 representation (instead of L_min = -1)
        oneD_haar_indices[count, 1] = n
        count += 1

key = lambda x : tuple([sum(x[i][0] for i in range(dim))] + [x[i][0] for i in range(dim)])
haar_mesh_indices = np.array(sorted(itt.product(oneD_haar_indices, repeat=dim), key = key), dtype=np.int)
haar_levels = np.vstack(sorted(itt.product(oneD_haar_levels, repeat=dim), key = lambda x : sum(x)))

# finds the index in haar_mesh indices such that n is the zero vector. This uses the fact that for each l, there are 2**|l^+|_1 wavelets
zero_n_indices = np.concatenate([[0],np.cumsum(2**np.sum(np.maximum(haar_levels-1,0),1))])[:-1]
assert np.sum(haar_mesh_indices[zero_n_indices,:,1]) == 0

# x bits for the ls and y bits for the n, overal (x+y)*dim bits needed
Lbits = len(format(L+1, 'b'))
tot_bits = dim*Lbits
print("num_cells: %d, tot_bits: %d" % (num_cells, tot_bits))

if tot_bits <= 8:
    kind = np.uint8
elif tot_bits <= 16:
    kind = np.uint16
elif tot_bits <= 32:
    kind = np.uint32
elif tot_bits <= 64:
    kind = np.uint64
else: raise ValueError("The level specified is too high for encoding the haar wavelet indices")

def encode(l):
    b = l.flatten().astype(kind)
    s = b[0]
    for i in range(1,dim):
        s |= b[i] << kind(Lbits*i)
    return s

print("inverting the map...")
inverse_map = dict(zip(map(encode, haar_levels), zero_n_indices))

if dim == 1:
    def get_index(l,n):
        return inverse_map[encode(l)] + n[0]
elif dim == 2:
    def get_index(l,n):
        return inverse_map[encode(l)] + n[1] + 2**max(l[1]-1,0) * n[0]
else:
    def get_index(l,n):
        ll = np.maximum(l-1,0)
        return inverse_map[encode(l)] + n[2] + 2**ll[2]*n[1] + 2**(ll[2] + ll[1]) * n[0]

print("checking that everything is correct...")
for i in range(len(haar_mesh_indices)):
    assert i == get_index(haar_mesh_indices[i][:,0], haar_mesh_indices[i][:,1])

print("testing the QMC white noise sampling...")

z = np.arange(num_cells) + 1
out = np.zeros((num_cells,))

normalised_midpoints = np.vstack([c.midpoint().array()[:dim] for c in cells(mesh)])/(b-a)

inversed_l = np.array([inverse_map[encode(l)] for l in haar_levels], dtype=np.int)

for i,l in enumerate(haar_levels):
    ll = np.maximum(l-1,0)
    # there are 2**l wavelets on level l, so floor(midpoint*2**(l-1)) gives the wavelet number (the l-1 is because here l starts from 0 instead of -1)
    n = (normalised_midpoints*(2**ll)).astype(np.int) # casting to int for non-negative numbers is equivalent to np.floor
    # there are 2**(l+1) Haar cells on level l, so floor(midpoint*2**l) gives the haar mesh number (the l is because here l starts from 0 instead of -1).
    # if the cell is even we have a plus sign, if it is odd we have a minus sign.
    nn = (normalised_midpoints*(2**l)).astype(np.int) # casting to int for non-negative numbers is equivalent to np.floor
    s = 1 - 2*(np.sum(nn,1) % 2) # equivalent to np.prod(1 - 2*(nn % 2), 1) (we just need to check the parity of the sum)

    # the following is the map between cells and QMC indices 
    # NOTE: the following is equivalent to idxs = np.array([get_index(l,b) for b in n], dtype=np.int)
    if dim == 1:
        idxs = inversed_l[i] + n[:,0]
    elif dim == 2:
        idxs = inversed_l[i] + n[:,1] + 2**max(l[1]-1,0) * n[:,0]
    else:
        idxs = inversed_l[i] + n[:,2] + 2**ll[2]*n[:,1] + 2**(ll[2] + ll[1]) * n[:,0]

    l_norm = np.sum(ll)
    out += (2**(l_norm/2)/np.sqrt(vol))*(s*z[idxs])

# after this, need to use the supermesh to map the haar mesh cells to the FEM mesh cells
