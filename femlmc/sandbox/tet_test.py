#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
import mshr
import numpy as np
import os
from MLQMC_white_noise_coupling import libsupermesh_code
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs
include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
compiled_code = compile_cpp_code(libsupermesh_code, cppargs=["-O0", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
compute_intersection_candidates_2D = compiled_code.compute_intersection_candidates_2D
intersect_vertical_segment = compiled_code.intersect_vertical_segment
QMCWhiteNoiseConstructor = compiled_code.QMCWhiteNoiseConstructor

################################################################################################################
#
#haar_mesh = BoxMesh.create(MPI.comm_world, [Point(0,0,0), Point(1,1,1)], [8,8,8], CellType.Type.hexahedron)
#
#simplex_coor = np.array([[0.001, 0.001,0.001], [0.999,0.999,0.999], [0.001,0.999,0.999], [0.001,0.001,0.999]])
#editor = MeshEditor()
#mesh = Mesh()
#editor.open(mesh, 'tetrahedron', 3, 3)
#editor.init_vertices(4)
#editor.init_cells(1)
#[editor.add_vertex(i, vertex) for i,vertex in enumerate(simplex_coor)]
#editor.add_cell(0, np.arange(4).astype(np.uintp))
#editor.close()
#
#colors = MeshFunction("size_t", haar_mesh, 3)
#colors.array()[:] = np.arange(haar_mesh.num_cells())
#
#File("colors.pvd") << colors
#File("tet.pvd") << mesh
#
################################################################################################################

Lh = 8
LH = 2**Lh
haar_mesh = RectangleMesh.create(MPI.comm_world, [Point(0,0), Point(1,1)], [LH,LH], CellType.Type.quadrilateral)
mesh = mshr.generate_mesh(mshr.Rectangle(Point(0,0), Point(1,1)), 20)

W = np.loadtxt("int_map_wrong.txt").astype(np.int)
C = np.loadtxt("int_map_correct.txt").astype(np.int)
W_cells = np.loadtxt("cells_in_haar_cell_wrong.txt").astype(np.int)
C_cells = np.loadtxt("cells_in_haar_cell_correct.txt").astype(np.int)

C_cumsum = np.concatenate([[0],np.cumsum(C_cells)])
W_cumsum = np.concatenate([[0],np.cumsum(W_cells)])

## given a position in C, find the haar cell
#def Cwhereisit(index):
#    return np.argmax(C_cumsum > index)-1
#def Wwhereisit(index):
#    return np.argmax(W_cumsum > index)-1
#
#for fem_cell in range(mesh.num_cells()):
#    Ctemp = np.unique(np.where(C == fem_cell)[0])
#    Wtemp = np.unique(np.where(W == fem_cell)[0])
#    cc = np.unique(np.array([Cwhereisit(index) for index in Ctemp]))
#    ww = np.unique(np.array([Wwhereisit(index) for index in Wtemp]))
#    if len(cc) > len(ww):
#        print("FOUND!")
#        break
#
#haar_cell = [item for item in cc if item not in ww][0]

for index in range(len(C_cells)):
    i = C_cumsum[index]
    j = C_cumsum[index+1]
    ii = W_cumsum[index]
    jj = W_cumsum[index+1]
    cc = np.unique(C[i:j])
    ww = np.unique(W[ii:jj])
    if len(cc) > len(ww):
        print("FOUND!")
        break

haar_cell = index
try: fem_cell = [item for item in cc if item not in ww][0]
except IndexError:
    print("cc and ww coincide")

#fem_cell = 101

#rng = np.random.RandomState(123456)
#c = Cell(mesh, rng.randint(0, mesh.num_cells()))
c = Cell(mesh, fem_cell)
inters = compute_intersection_candidates_2D(c.get_vertex_coordinates(), 3, LH)
mf = MeshFunction("size_t", haar_mesh, 2, 0)
mt = MeshFunction("size_t", mesh, 2, 0)
mt[c] = 1
for index in inters:
    mf[index] = 1

mf[haar_cell] = 2

File("mt.pvd") << mt
File("mf.pvd") << mf

eps = 1.0e-14
coor = np.array(c.get_vertex_coordinates()).reshape((-1,2))
vertex_indices = np.minimum(LH-1, np.maximum(coor*LH, eps).astype(np.int))
xmin = np.maximum((np.min(coor,0)*LH - eps).astype(np.int), 0)
xmax = np.minimum((np.max(coor,0)*LH + eps).astype(np.int), LH)

delta_x = xmax[0] - xmin[0]
delta_y = xmax[1] - xmin[1]

inv_LH = 1.0/LH


coord = coor.flatten()
minmax = lambda x : (int(max(min(x)*LH-eps, eps)), int(min(max(x)*LH + eps, LH-1)))
y_min = xmin[1]*inv_LH
y_max = (xmax[1] + 1)*inv_LH
for ii in range(delta_x):
    x_coor = inv_LH*(xmin[0] + ii + 1)
    temp_endpoints = intersect_vertical_segment(x_coor, y_min, y_max, coord, 3, LH, False)

    check_intersections = []
    for i in range(3):
        p0_x = coord[2*i]
        p0_y = coord[2*i + 1]
        p1_x = coord[2*((i+1)%3)]
        p1_y = coord[2*((i+1)%3) + 1]

        xe_min = min(p0_x, p1_x)
        xe_max = max(p0_x, p1_x)
        ye_min = min(p0_y, p1_y)
        ye_max = max(p0_y, p1_y)

        if ((xe_min > x_coor + eps) or (xe_max + eps < x_coor)):
            continue

        if (abs(xe_min-xe_max) < eps):
            check_intersections.append(ye_min)
            check_intersections.append(ye_max)
        else:
            t0 = (x_coor - p0_x)/(p1_x - p0_x)
            y0 = p0_y + t0*(p1_y - p0_y)
            check_intersections.append(y0)

    endpoints = minmax(np.sort(np.array(check_intersections)))

    print((endpoints, temp_endpoints))
