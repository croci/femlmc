#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from petsc4py import PETSc
from femlmc import *
from mkl_sobol import *
import numpy as np
import numpy.random as npr
import sys

mpiRank = MPI.rank(MPI.comm_world)

class TestFunctional(OutputFunctional):
    def evaluate(self, V, sample = None, rv_sample = None, level_info = (None,None)):

        opts   = {"p_ksp_type" : "cg",
                  "p_ksp_atol" : 1.0e-10,
                  "p_ksp_rtol" : 1.0e-15,
                  "p_ksp_max_it" : 100,
                  "p_ksp_norm_typex" : "unpreconditioned",
                  "p_pc_factor_mat_solver_package" : "mumps",
                  "p_ksp_converged_reasonx" : None,
                  "p_ksp_monitor_true_residualx" : None,
                  "p_ksp_viewx" : None,
                  "p_pc_type" : "hypre",
                  "p_pc_hypre_type" : "boomeramg"}

        petsc_opts = PETSc.Options() 

        for key in opts:
            petsc_opts[key] = opts[key]

        u = TrialFunction(V)
        v = TestFunction(V)

        out = Function(V)

        lhs = inner(exp(sample)*grad(u), grad(v))*dx
        rhs = Constant(1.0)*v*dx

        bcs = DirichletBC(u.function_space(), Constant(0.0), 'on_boundary')

        system = assemble_system(lhs,rhs,bcs)
        L = as_backend_type(system[0]).mat()
        b = as_backend_type(system[1]).vec()

        ksp = PETSc.KSP().create(V.mesh().mpi_comm()) # need a .Dup() ?
        ksp.setOperators(L)
        ksp.setOptionsPrefix("p_")
        ksp.setFromOptions()

        y = b.duplicate()
        ksp.solve(b, y)

        out.vector()[:] = y.getArray()

        E = assemble(out*out*dx) # inner_vol is one here
        EE = assemble(out*dx) 
        #E = assemble(sample*sample*dx) # inner_vol is one here
        outlist = [np.array((E,EE)), E, EE]

        return [E]
        #return outlist

    def get_n_outputs(self):
        return 1

if __name__ == '__main__':
    k   = 1
    dim = 2
#
#    # Matern field parameters
#    matern_parameters = {"lmbda"    : 0.25,
#                         "avg"      : 0.0,
#                         "sigma"    : 1.0,
#                         "nu"       : 2*k-dim/2,
#                         "lognormal_scaling" : False}

    # Matern field parameters
    matern_parameters = {"lmbda"    : 0.25,
                         "avg"      : 1.0,
                         "sigma"    : 0.2,
                         "nu"       : 2*k-dim/2,
                         "lognormal_scaling" : True}

    N0 = 10 # initial samples on coarse levels

    try: filename = sys.argv[2]
    except IndexError: filename = "mc_test.txt"
    try: N = int(sys.argv[1])
    except IndexError: N = 1 
    if filename[-4:] != ".txt":
        filename += ".txt"

    assert N > 0

    buf = 0
    counterbuf = buf
    n_levels  = 10 - [2, 3, 5][dim-1] - counterbuf
    #RNG       = npr.RandomState(mpiRank)
    RNG = MKL_RNG(seed=mpiRank)
    setRNG(RNG)

    if dim == 3:
        # 3D
        outer_meshes = [BoxMesh(MPI.comm_self, Point(-1,-1,-1), Point(1,1,1), 2**(l+1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [BoxMesh(MPI.comm_self, Point(-0.5,-0.5,-0.5), Point(0.5,0.5,0.5), 2**l, 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
    elif dim == 2:
        # 2D
        outer_meshes = [RectangleMesh(MPI.comm_self, Point(-1,-1), Point(1,1), 2**(l+1), 2**(l+1)) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [RectangleMesh(MPI.comm_self, Point(-0.5,-0.5), Point(0.5,0.5), 2**l, 2**l) for l in range(buf, n_levels+buf)][1:]
    else:
        # 1D
        outer_meshes = [IntervalMesh(MPI.comm_self, 2**(l+1), -1.0, 1.0) for l in range(buf, n_levels+buf)][1:]
        inner_meshes = [IntervalMesh(MPI.comm_self, 2**l,     -0.5, 0.5) for l in range(buf, n_levels+buf)][1:]

    #outer_meshes = [outer_meshes[0] for l in range(n_levels-1)]
    #inner_meshes = [inner_meshes[0] for l in range(n_levels-1)]

    # h-ref
    outer_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in outer_meshes]
    inner_spaces = [FunctionSpace(mesh, 'CG', 1) for mesh in inner_meshes]

    ## p-ref
    #outer_spaces = [FunctionSpace(outer_meshes[2], 'CG', l+1) for l in range(n_levels)]
    #inner_spaces = [FunctionSpace(inner_meshes[2], 'CG', l+1) for l in range(n_levels)]
    
    matern_field = MaternField(inner_spaces, outer_spaces = outer_spaces, parameters = matern_parameters, nested_inner_outer = True, nested_hierarchy = False)
    mlmc_sampler = MLMCSampler(inner_spaces, stochastic_field = matern_field, output_functional = TestFunctional(), richardson_alpha = None)

    #Eps = [5.0e-6, 2.0e-5, 5.0e-5]
    Eps = list(5.0e-5/2**np.arange(7))
    Eps = list(5.0e-4/2**np.arange(7))

    logfile = open(filename, 'w')

    # the following calls mlmc_test.py
    mlmc_sampler.run_mlmc_test(N, N0, Eps, Lmin = 0, logfile=logfile, debug = False)
    import sys; sys.exit(0)

    # the following calls mlmc.py, or mc_test.py
    mlmc_sampler.run(N0, Eps[-1], alpha = 2, beta = 4, gamma = 2)

    mc_sampler = mlmc_sampler.get_independent_field(-1)
    mc_sampler.run_mlmc_test(N, logfile = logfile)
    mc_sampler.run(N0)

    mc_sampler = MLMCSampler(inner_spaces[-1], stochastic_field = matern_field.get_independent_field(-1), output_functional = TestFunctional())
    mc_sampler.run_mlmc_test(N, logfile = logfile)
    mc_sampler.run(N0)
