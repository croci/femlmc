#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import numpy
from dolfin import MPI
from numpy import sqrt
import sys

comm = MPI.comm_world
mpiRank = MPI.rank(comm)

def mc(mc_fn, N, logfile = None):
    """
    Standard Monte Carlo test routine.

    mlmc_fn: the user low-level routine. Its interface is
      sums = mc_fn(N)
    with inputs
      N = number of paths
    and a list of numpy arrays of outputs
      sums[i][0] = sum(P)
      sums[i][1] = sum(P**2)
      sums[i][2] = sum(P**3)
      sums[i][3] = sum(P**4)

    N: number of samples for convergence tests
    """

    # First, convergence tests

    write(logfile, "\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "***     Convergence, Expected Value, Variance, Cost    ***\n")
    write(logfile, "**********************************************************\n")
    write(logfile, "\n  ave(P)      var(P)      kurt(P)     cost(P) \n")
    write(logfile, "--------------------------------------------------\n")

    avg = []
    var = []
    kurt = []
    cost = []

    sums, costl = mc_fn(N)
    if isinstance(sums, list):
        l = len(sums)
    else:
        l = 1
        sums = [sums]

    for k in range(l):
        cost.append(costl)
        for i in range(len(sums[k])):
            sums[k][i] = sums[k][i]/N

        avg.append(sums[k][0])

        if isinstance(avg[-1], float):
            var.append(max(abs(sums[k][1]-sums[k][0]**2), 1.0e-16)) 
            kurt.append((sums[k][3] - 4*sums[k][0]*sums[k][2] + 6*sums[k][0]**2*sums[k][1] - 3*sums[k][0]**4)/var[-1]**2.)

            write(logfile, "%8.4e  %8.4e  %8.4e  %8.4e \n" % (avg[-1], var[-1], kurt[-1], cost[-1]))
        else:
            var.append(numpy.maximum(sums[k][1]-sums[k][0]**2, 1.0e-10)) # fix for cases with var = 0
            write(logfile, "Expected value: %s\n" % avg[-1])
            write(logfile, "Variance: %s\n" % var[-1])

        write(logfile, "\n")

    if mpiRank == 0 and logfile is not None:
        [numpy.savetxt(logfile.name[:-4] + '-output%d.txt' % k, avg[k]) for k in range(l) if isinstance(avg[k], numpy.ndarray)]

    return avg

def write(logfile, msg):
    """
    Write to both sys.stdout and to a logfile.
    """
    if mpiRank == 0:
        if logfile is not None:
            logfile.write(msg)
            logfile.flush()
        sys.stdout.write(msg)


