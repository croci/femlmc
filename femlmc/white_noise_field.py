#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from dolfin import *
from FIAT import reference_element as re
import ffc.fiatinterface as ft
import itertools

from .stochastic_field import StochasticField, to_levels
from femlmc import _libsupermesh_include_dirs, _libsupermesh_library_dirs
from numpy.random import RandomState
import numpy as np
from scipy.linalg import solve_triangular
from scipy.sparse import csr_matrix
from time import time
import os
from sys import modules

from .white_noise_coupling import supermesh_code
include_dirs = _libsupermesh_include_dirs
library_dirs = _libsupermesh_library_dirs
compiled_libsupermesh_code = compile_cpp_code(supermesh_code, cppargs=["-O3", "-ffast-math", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
#compiled_libsupermesh_code = compile_cpp_code(supermesh_code, cppargs=["-O0", "-g", "-Wl,-rpath=" + library_dirs], include_dirs = [include_dirs], library_dirs = [library_dirs], libraries = ["supermesh"])
SuperMeshConstructor = compiled_libsupermesh_code.SuperMeshConstructor

class WhiteNoiseField(StochasticField):
    def setup(self):

        self.us = [TrialFunction(V) for V in self.function_spaces]
        self.vs = [TestFunction(V)  for V in self.function_spaces]

        self.spaces_dimensions = [V.element().space_dimension() for V in self.function_spaces]

        self.couple_levels = self.params.get("couple_levels", True)

        if self.refinement_type in ["p", None] or self.couple_levels == False:
            # p-refinement or independent levels

            self.factors = []
            self.n_cells = []
            self.cell_volumes = []
            self.mixed_factors = [None]
            for l in range(self.n_levels):
                mesh = self.function_spaces[l].mesh()
                self.n_cells.append(mesh.num_cells())
                self.cell_volumes.append(np.sqrt(np.array([c.volume() for c in cells(mesh)])))
                c = Cell(mesh, 0)
                self.factors.append(np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume()))

                if l > 0 and self.couple_levels == True:
                    mixed_mass_matrix = assemble_local(self.us[l-1]*self.vs[l]*dx, c)/c.volume()
                    self.mixed_factors.append(solve_triangular(self.factors[l], mixed_mass_matrix, lower=True).T)
        else:

            if self.mpiRank == 0:
                print("Generating Cholesky factors...")

            self.factors = []
            for l in range(self.n_levels):
                c = Cell(self.function_spaces[l].mesh(),0)
                self.factors.append(np.linalg.cholesky(assemble_local(self.us[l]*self.vs[l]*dx, c)/c.volume()))

            self.interp_matrices   = [None]
            intersection_maps = [None]
            self.supermesh_element_volumes = [np.sqrt(np.array([c.volume() for c in cells(self.function_spaces[0].mesh())]))]
            self.sorting_indices = [None]

            ref_el = np.vstack(re.ufc_simplex(self.function_spaces[0].mesh().topology().dim()).get_vertices()).flatten()
            finite_elements = [ft.create_element(V.ufl_element()) for V in self.function_spaces]
            reference_nodes = [np.vstack(list(itertools.chain(*(iter(node.get_point_dict().keys()) for node in fe.dual_basis())))).flatten() for fe in finite_elements]

            # Supermesh constructions. We use the affine FEM strategy from the paper
            for l in range(1, self.n_levels):
                a = time()
                constructor = SuperMeshConstructor(self.function_spaces[l], self.function_spaces[l-1], ref_el, reference_nodes[l])
                self.interp_matrices.append((constructor.get_full_interp(0), constructor.get_full_interp(1))) # interpolation matrices from supermesh to parent meshes
                intersection_maps.append(constructor.get_intersection_map().reshape((-1,2))) # intersecting cell pairs
                self.supermesh_element_volumes.append(constructor.get_supermesh_element_volumes()) # volumes of the supermesh cells
                self.sorting_indices.append(np.argsort(self.supermesh_element_volumes[-1])) # needed to sort the supermesh cells later

                del constructor

                if self.mpiRank == 0:
                    print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

            self.n_cells = [self.function_spaces[0].mesh().num_cells()] + [len(item) for item in self.supermesh_element_volumes[1:]]

        if self.mpiRank == 0:
            print("Cholesky factors computed!")
            print("Generating assembly matrices...")

        # building the sparse boolean supermesh assembly matrices
        self.supermesh_assembly_matrices = []
        dofmaps = [self.function_spaces[l].dofmap() for l in range(self.n_levels)]
        for l in range(self.n_levels):
            a = time()
            if self.refinement_type in ["p", None] or self.couple_levels == False:
                # final size will be np.zeros((self.function_spaces[l].dim(), self.n_cells[l]*self.spaces_dimensions[l]))
                n = self.spaces_dimensions[l]*self.n_cells[l]
                fine_rows = np.concatenate([dofmaps[l].cell_dofs(i) for i in range(self.n_cells[l])])
                # there is exactly one nonzero entry per column and it is 1
                fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                coarse_mat = None

                if l > 0 and self.couple_levels == True:
                    ncoarse = self.spaces_dimensions[l-1]*self.n_cells[l]
                    coarse_rows = np.concatenate([dofmaps[l-1].cell_dofs(i) for i in range(self.n_cells[l])])
                    coarse_mat = sp.csr_matrix((np.ones((ncoarse,),dtype='int8'), (coarse_rows, np.arange(ncoarse))), shape=(self.function_spaces[l-1].dim(), ncoarse), dtype='int8')

            else:
                if l == 0:
                    n = self.spaces_dimensions[l]*self.n_cells[l]
                    fine_rows = np.concatenate([dofmaps[l].cell_dofs(i) for i in range(self.n_cells[l])])
                    fine_mat = csr_matrix((np.ones((n,),dtype='int8'), (fine_rows, np.arange(n))), shape = (self.function_spaces[l].dim(), n), dtype='int8')
                    coarse_mat = None

                else:
                    nmax = self.spaces_dimensions[l]*self.n_cells[l]
                    nmin = self.spaces_dimensions[l-1]*self.n_cells[l]
                    fine_rows = []
                    coarse_rows = []
                    pairs = intersection_maps[l][self.sorting_indices[l],:]
                    for pair in pairs:
                        fine_rows.append(dofmaps[l].cell_dofs(pair[0]))
                        coarse_rows.append(dofmaps[l-1].cell_dofs(pair[1]))

                    fine_rows = np.concatenate(fine_rows)
                    coarse_rows = np.concatenate(coarse_rows)

                    fine_mat = csr_matrix((np.ones((nmax,),dtype='int8'), (fine_rows, np.arange(nmax))), shape = (self.function_spaces[l].dim(), nmax), dtype='int8')
                    coarse_mat = csr_matrix((np.ones((nmin,),dtype='int8'), (coarse_rows, np.arange(nmin))), shape = (self.function_spaces[l-1].dim(), nmin), dtype='int8')

            self.supermesh_assembly_matrices.append((fine_mat,coarse_mat))

            if self.mpiRank == 0:
                print("Level %d done, levels left: %d, elapsed time: %f" % (l, self.n_levels - l - 1, time() - a))

        if self.mpiRank == 0:
            print("Assembly matrices computed!")

        self.use_qmc = self.params.get("use_qmc", False)
        if self.use_qmc is True:
            self.qmc_dims = [item for item in self.n_cells]

            if self._independent == False:
                try: 
                    QMC_dimensions = getattr(modules['uqdolfin'], '__QMC_DIMENSIONS__')
                except AttributeError:
                    QMC_dimensions = []

                self.input_number = len(QMC_dimensions)
                QMC_dimensions.append([len(QMC_dimensions), self.qmc_dims])
                setattr(modules["uqdolfin"], '__QMC_DIMENSIONS__', QMC_dimensions)
            else:
                self.input_number = None

            self.GQRNG = None

    def sample(self, l):
        # NOTE we are using the affine strategy from the paper

        xrnge = 2 - (l is 0 or self.couple_levels == False)

        Fs = [Function(V) for V in self.function_levels[l][:xrnge]]
        Fs_arr = [0 for i in range(xrnge)]

        if self.refinement_type in ["p", None] or self.couple_levels == False:

            r = self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l])

            z1 = self.factors[l].dot(r)*self.cell_volumes[l]
            if l > 0 and self.couple_levels == True:
                # Apply inverse transpose cholesky factor to all elements in r
                z2 = self.mixed_factors[l].dot(r)*self.cell_volumes[l] # NOTE: if self.couple_levels == True, then the cell_volumes are constant across l

            Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1.T.flatten()) 

            if l > 0 and self.couple_levels == True:
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(z2.T.flatten()) 

        else:
            if l == 0:
                r = self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l])

                z1 = self.factors[l].dot(r)*self.supermesh_element_volumes[l]

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1.T.flatten()) 

            else:
                z = self.factors[l].dot(self.RNG.randn(self.spaces_dimensions[l], self.n_cells[l]))*self.supermesh_element_volumes[l]

                mmax = self.spaces_dimensions[l]
                mmin = self.spaces_dimensions[l-1]
                # use einsum to apply the interpolation matrices to each local random vector. Output is a matrix of size spaces_dimension-by-n_cells
                z1 = np.einsum('ijk,ki->ji', self.interp_matrices[l][0].reshape((self.n_cells[l],mmax,mmax)), z)
                z2 = np.einsum('ijk,ki->ji', self.interp_matrices[l][1].reshape((self.n_cells[l],mmin,mmax)), z)
                #assert all(np.isfinite(z1.flatten()))
                #assert all(np.isfinite(z2.flatten()))

                Fs_arr[0] = self.supermesh_assembly_matrices[l][0].dot(z1[:,self.sorting_indices[l]].T.flatten()) 
                Fs_arr[1] = self.supermesh_assembly_matrices[l][1].dot(z2[:,self.sorting_indices[l]].T.flatten()) 

        Fs[0].vector()[:] = Fs_arr[0]
        if l > 0 and self.couple_levels == True:
            Fs[1].vector()[:] = Fs_arr[1]

        return Fs
