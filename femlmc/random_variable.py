#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from sys import modules
from scipy.stats import norm

class RandomVariable(object):
    def __init__(self, RNG = None, *args, **kwargs):
        if RNG is None:
            from sys import modules
            self.RNG = getattr(modules['femlmc'], '__RNG__')
            if self.RNG is None:
                raise RuntimeError("Before using femlmc a random number generator must be set with a call to setRNG")

        self.kwparams = kwargs
        self.params   = args

        self.qmc_flag = kwargs.get("qmc", False)
        if self.qmc_flag:
            try: 
                QMC_dimensions = getattr(modules['femlmc'], '__QMC_DIMENSIONS__')
            except AttributeError:
                QMC_dimensions = []

            self.input_number = len(QMC_dimensions)
            QMC_dimensions.append([len(QMC_dimensions), self.dim()])
            setattr(modules["femlmc"], '__QMC_DIMENSIONS__', QMC_dimensions)
            self.GQRNG = None


        self.setup()

    def _initialise_GQRNG(self):
        self.GQRNG = getattr(modules["femlmc"], '__GLOBAL_QRNG__')

    def sample(self, l=0):
        raise NotImplementedError

    def setup(self):
        pass

    def dim(self):
        return 1

class MixedRandomVariable(RandomVariable):
    def __init__(self, random_variables, *args, **kwargs):

        if not isinstance(random_variables, list) or len(random_variables) <= 1:
            raise ValueError("Input must be a list of RandomVariable objects of length > 1")

        self.qmc_flag = any([rv.qmc_flag for rv in random_variables])
        if len(set(random_variables)) != len(random_variables) and self.qmc_flag:
            raise ValueError("For QMC and MLQMC the list of input random variables cannot contain repetitions of the same random variable object!")

        self.random_variables = random_variables
        self.n_rv = len(random_variables)

        self.kwparams = kwargs
        self.params   = args

        self.setup()

    def _initialise_GQRNG(self):
        for rv in self.random_variables:
            rv._initialise_GQRNG()

    def sample(self, l=0):
        samples = [RV.sample(l) for RV in self.random_variables]
        return samples

    def get_sub_random_variable(self,i):
        return self.random_variables[i]

    def setup(self):
        pass

class NormalRandomVariable(RandomVariable):
    def setup(self):
        if len(self.params) > 0:
            if isinstance(self.params[0], tuple):
                self.mu, self.sigma = self.params[0]
            else:
                self.mu    = self.params[0]
                self.sigma = self.params[1]
        else:
            self.mu    = self.kwparams.get("mu", 0.0)
            self.sigma = self.kwparams.get("sigma", 1.0)

    def sample(self,l=0):
        if self.qmc_flag == False:
            return self.sigma*self.RNG.randn() + self.mu
        else:
            return self.sigma*self.GQRNG.sample(l, self.input_number) + self.mu

class UniformRandomVariable(RandomVariable):
    def setup(self):
        if len(self.params) > 0:
            if isinstance(self.params[0], tuple):
                self.a, self.b = self.params[0]
            else:
                self.a = self.params[0]
                self.b = self.params[1]
        else:
            self.a = self.kwparams.get("a", 0.0)
            self.b = self.kwparams.get("b", 1.0)

    def sample(self,l=0):
        if self.qmc_flag == False:
            return (self.b - self.a)*self.RNG.rand() + self.a
        else:
            return (self.b - self.a)*norm.cdf(self.GQRNG.sample(l, self.input_number)) + self.a

class BernoulliRandomVariable(RandomVariable):
    def setup(self):
        if len(self.params) > 0:
            self.p = self.params[0]
        else:
            self.p = self.kwparams.get("p", 0.5)

    def sample(self,l=0):
        if self.qmc_flag == False:
            return self.RNG.rand() > 1.0 - self.p
        else:
            return norm.cdf(self.GQRNG.sample(l, self.input_number)) > 1.0 - self.p
