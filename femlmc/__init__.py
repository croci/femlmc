#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

__author__     = 'Matteo Croci'
__credits__    = ['Matteo Croci', 'Patrick E. Farrell', 'Michael B. Giles', 'Marie E. Rognes']
__license__    = 'GPL-3'
__maintainer__ = 'Matteo Croci'
__email__      = 'matteo.croci@maths.ox.ac.uk'

from sys import modules
from os import environ
_libsupermesh_include_dirs = environ.get('LIBSUPERMESH_DIR', '/usr/local') + "/include"
_libsupermesh_library_dirs = environ.get('LIBSUPERMESH_DIR', '/usr/local') + "/lib"

from .output_functional             import    OutputFunctional
from .stochastic_field              import    StochasticField, to_levels
from .random_variable               import    RandomVariable, MixedRandomVariable, NormalRandomVariable, UniformRandomVariable, BernoulliRandomVariable
from .mixed_stochastic_field        import    MixedStochasticField
from .white_noise_field             import    WhiteNoiseField
from .QMC_white_noise_field         import    QMCWhiteNoiseField
from .matern_field                  import    MaternField, make_nested_mapping
from .mlmc_sampler                  import    MLMCSampler
from .mlmc_test                     import    mlmc_test
from .mlmc                          import    mlmc
from .mc                            import    mc
from .qmc                           import    qmc, qmc_convergence_test
from .mlqmc                         import    mlqmc
from .mlqmc_test                    import    mlqmc_test
from .mlmc_sampler                  import    MLMCSampler
from .quasi_random_number_generator import    GlobalQRNG
from .white_noise_coupling          import    supermesh_code
from .QMC_white_noise_coupling      import    HaarWhiteNoise, QMC_supermesh_code
from .MLQMC_white_noise_coupling    import    CoupledHaarWhiteNoise, MLQMC_supermesh_code

setattr(modules[__name__], '__GLOBAL_QRNG__', None)

def setRNG(RNG):
    setattr(modules[__name__], '__RNG__', RNG)

def getRNG():
    return getattr(modules[__name__], '__RNG__')

def setQRNG(QRNG_CLASS, M=32):
    setattr(modules[__name__], '__QRNG_CLASS__', QRNG_CLASS)
    setattr(modules[__name__], '__QMC_DIMENSIONS__', [])
    setattr(modules[__name__], '__N_QMC_SHIFTS__', M)

def getQRNG():
    return getattr(modules[__name__], '__QRNG_CLASS__')

def get_global_QRNG():
    return getattr(modules[__name__], '__GLOBAL_QRNG__')

def create_global_QRNG(worldcomm=None):
    if worldcomm is not None:
        GQRNG = GlobalQRNG(worldcomm = worldcomm)
    else:
        GQRNG = GlobalQRNG()

    GQRNG._setUp()

    setattr(modules[__name__], '__GLOBAL_QRNG__', GQRNG)
