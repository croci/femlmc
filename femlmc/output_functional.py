#######################################################################
#
# This file is part of FEMLMC
#
# FEMLMC is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from numpy import ndarray

class OutputFunctional(object):
    def __init__(self):
        pass

    def evaluate(self, function_space, sample = None, rv_sample = None, level_info = (None, None), *args, **kwargs):
        raise NotImplementedError

    def get_n_outputs(self):
        return 1

    def norm(self, output, k): # k in range(self.get_n_outputs()) is the output number of output
        if isinstance(output, float):
            return abs(output)
        elif isinstance(output, ndarray):
            return abs(output).max()
        else:
            raise NotImplementedError("Unrecognized output type. By default femlmc only supports scalars and numpy ndarrays. Overload the function norm in the OutputFunctional class with a user-prescribed norm to fix this.")
